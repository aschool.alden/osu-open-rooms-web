extends layout.dt

block title
	title #{lobby.lobby.name} - Manage Lobby - osu! OpenRooms
	link(rel="stylesheet", href="/dragula.js/dist/dragula.min.css")
	- titleBase = "Manage Lobby";

block scripts
	- import vibe.data.json;

	script var botAccounts = #{serializeToJson([botAccount, "BanchoBot"])}; var lobbyId = #{serializeToJson(lobby.bsonID.toString)};
	script loadTabs(); connectAdmin();

block header
	h1= lobby.lobby.name

block subheader
	.subheader.admin
		.container
			.info
				p
					a(href="https://osu.ppy.sh/mp/#{lobby.lobby.apiId}") Match Link
					|  -
					a(href="osu://mp/#{lobby.lobby.gameId}") Join Link
					|  - Current Song: 
					- if (lobby.song.valid)
						- auto map = Map.tryFindById(lobby.song, Map.init);
						span.map: a(href="https://osu.ppy.sh/b/#{map.mapID}") #{map.info.artist} - #{map.info.title} [#{map.info.difficultyName}]
					- else
						span.map none
			- if (error.length)
				p.error Error: #{error}
			.tabcontrol(style="display:none")
				a(href="#chat", data-tab="chat") Chat
				a(href="#mode", data-tab="mode") Playmode
				a(href="#timings", data-tab="timings") Durations
				a(href="#danger", data-tab="danger") Advanced

block content
	include maplists_functions.dt

	.admin
		.tabs
			.chat
				.messages
					p.empty(style=(!chatlog || chatlog.empty) ? "" : "display:none") No chat messages
					- if (chatlog) foreach (message; chatlog.range)
						.message(class=(message.sender == botAccount || message.sender == "BanchoBot") ? "bot" : "")
							- import std.datetime.date : TimeOfDay;
							- import std.datetime.systime : SysTime;
							- import std.datetime.timezone : UTC;
							.time.utc(data-time=SysTime(message.date, UTC()).toISOExtString)= (cast(TimeOfDay)SysTime(message.date, UTC())).toISOExtString
							.sender= message.sender
							.content= message.message.message
				.slots
					- foreach (i; 0 .. 16)
						.slot
			.card.wide.mode
				.modes
					- import master;
					- foreach (other; slaveHandler.availableModes.data)
						- if (other.id == mode.id)
							span #{other.name}
						- else
							a(href="/lobby/#{lobby.bsonID}?mode=#{mode.id}")= mode.name
				form.create.lazerform(action="/lobby/#{lobby.bsonID}/mode", method="POST")
					input(type="hidden", name="token", value=token)
					input(type="hidden", name="mode", value=mode.id)
					h3 #{mode.name} Settings
					- renderMapModeSettings(settings);
					input.lazerbutton(type="submit", value="Update Settings")
			form.timings(action="/lobby/#{lobby.bsonID}/timings", method="POST")
				input(type="hidden", name="token", value=token)
				.labeled
					span Start Game Duration
					input.lazerinput(type="text", name="startGameDuration", value="3 minutes", required, pattern="^\\d+(\\.\\d+)?\\s+(hns|us|ms|s|secs|seconds|m|mins|minutes)$", required)
					p.description The duration to wait after picking a map with not everyone readying up.
				.labeled
					span All-Ready Start Duration
					input.lazerinput(type="text", name="allReadyStartDuration", value="5 seconds", required, pattern="^\\d+(\\.\\d+)?\\s*(hns|us|ms|s|secs|seconds|m|mins|minutes)$", required)
					p.description The duration to wait for starting after everyone readied up.
				.labeled
					span Manual Start Duration
					input.lazerinput(type="text", name="manualStartDuration", value="15 seconds", required, pattern="^\\d+(\\.\\d+)?\\s*(hns|us|ms|s|secs|seconds|m|mins|minutes)$", required)
					p.description The duration to wait when the host or room admin sends the !start command.
				.labeled
					span Map Finish Period
					input.lazerinput(type="text", name="retryEvaluationDuration", value="20 seconds", required, pattern="^\\d+(\\.\\d+)?\\s*(hns|us|ms|s|secs|seconds|m|mins|minutes)$", required)
					p.description The duration how long after a map players have the possibility to vote, retry and bookmark a map.
				.labeled
					span Retry Start Duration
					input.lazerinput(type="text", name="retryGameDuration", value="30 seconds", required, pattern="^\\d+(\\.\\d+)?\\s*(hns|us|ms|s|secs|seconds|m|mins|minutes)$", required)
					p.description The duration to wait before starting after running the !retry command after lobby end.
				input.lazerbutton(type="submit", value="Update")
			.danger
				h2 Advanced Operations
				a.lazerbutton(href="/lobby/#{lobby.bsonID}/close?token=#{token}") Close Room
