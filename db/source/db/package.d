module db;
@safe:

public import db.game;
public import db.user;

public import mongoschema;

import vibe.db.mongo.mongo;

import std.typecons : tuple;

enum WebRoot = "https://openrooms.app";

void registerDB(string host)
{
	registerDB(connectMongoDB(host).getDatabase("osuautohost"));
}

void registerDB(MongoDatabase db)
{
	db["gameusers"].register!GameUser;
	db["users"].register!User;
	db["irccreds"].register!SavedBot;
	db["activelobbies"].register!ActiveLobby;
	db["lobbyhistory"].register!Lobby;
	db["playlists"].register!Playlist;
	db["map"].register!Map;
	db["playlists_likes"].register!PlaylistLike;
	db["map_ratings"].register!MapRating;

	db["playlists_likes"].ensureIndex([tuple("u", 1), tuple("p", 1)]);
	db["map_ratings"].ensureIndex([tuple("u", 1), tuple("p", 1)]);
}
