module util.dates;

import core.time;
import std.conv;
import std.datetime.systime;

string fuzzyDate(SysTime date)
{
	auto now = Clock.currTime;
	auto tm = now - date;
	bool negative = tm < Duration.zero;
	if (negative)
		tm = -tm;
	string unit;
	if (tm < 10.seconds)
		return "now";
	else if (tm < 1.minutes)
		unit = "less than a minute";
	else if (tm < 2.minutes)
		unit = "a minute";
	else if (tm < 1.hours)
		unit = tm.total!"minutes"
			.to!string ~ " minutes";
	else if (tm < 2.hours)
		unit = "an hour";
	else
		unit = tm.total!"hours"
			.to!string ~ " hours";
	return negative ? "in " ~ unit : unit ~ " ago";
}
