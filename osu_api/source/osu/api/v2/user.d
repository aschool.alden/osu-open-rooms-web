/// These functions require the identify oauth scope.
module osu.api.v2.user;
@safe:

import osu.api.v2.base;

import vibe.data.serialization;
import vibe.data.json;

struct User
{
	string username;
	long id;
@optional:

	@name("is_bot")
	bool isBot;

	@name("is_active")
	bool isActive;

	static User getMe(string authorization)
	{
		return requestLazer(HTTPMethod.GET, "/me", authorization).deserializeJson!User;
	}
}
