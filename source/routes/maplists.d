module routes.maplists;

import bancho.irc;

import vibe.vibe;

import db;

import std.algorithm;
import std.array;
import std.ascii;
import std.math;
import std.meta;
import std.string;
import std.traits;
import std.uni;
import std.utf;

import std.string : indexOfAny;

import util.autopp;

void renderMaplists(scope HTTPServerResponse res, User user, string error = null)
{
	res.render!("maplists.dt", user, error, Playlist, User);
}

void renderMaplistsCreate(scope HTTPServerResponse res, User user, string token,
		string recipe = "", string name = "", string description = "", string error = null)
{
	auto ppSources = getPPDataSources();

	res.render!("maplists_create.dt", user, token, error, ppSources, recipe,
			name, description, Playlist);
}

void renderMaplistDetails(scope HTTPServerResponse res, User user, Playlist playlist, string token)
{
	res.render!("maplists_view.dt", user, playlist, token, User, Map);
}

@path("/maplists")
class MaplistsInterface
{
	void index(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!false);

		res.renderMaplists(user);
	}

	void getCreate(scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		mixin(authenticateUser!true);

		string token = makeToken(req, res);

		res.renderMaplistsCreate(user, token);
	}

	void postCreate(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string name, string description, string recipe, bool makePublic, string token)
	{
		mixin(authenticateUser!true);

		auto error = appender!(string);

		if (!validateToken(req, token))
			error.put("Invalid request token (try refreshing the page). ");

		string newToken = makeToken(req, res);

		if (!name.length)
			error.put("The playlist requires a name. ");
		if (name.length > 60)
			error.put("The playlist name is too long. ");

		if (description.length > 2000)
			error.put("The playlist description is too long. ");

		if (Clock.currTime - user.lastMapCreate.toSysTime < 2.minutes)
			error.put("You are trying to create maplists too quickly, cool down a little bit. ");

		RecipeParser parser;
		parser.load();

		auto lines = recipe.lineSplitter;
		foreach (line; lines)
		{
			try
			{
				parser.processLine(line);
			}
			catch (Exception e)
			{
				error.put("The recipe line '");
				error.put(line);
				error.put("' is broken: ");
				error.put(e.msg);
				error.put(". ");
			}
		}

		if (parser.recipe.data.length == 0)
			error.put("The recipe is empty. ");

		if (error.data.length)
		{
			res.renderMaplistsCreate(user, newToken, recipe, name, description, error.data);
			return;
		}

		Playlist playlist;
		playlist.name = name;
		playlist.description = description;
		playlist.public_ = makePublic;
		playlist.author = user.bsonID;
		playlist.recipe = parser.recipe.data;
		try
		{
			playlist.build(user.admin ? Playlist.adminLimit : Playlist.userLimit);
		}
		catch (Exception e)
		{
			logDebug("Exception in playlist.build: %s", e);
			error.put(e.msg);
			error.put(". ");
		}

		if (!playlist.picks.length)
			error.put("The recipe didn't yield any maps. ");

		if (error.data.length)
		{
			res.renderMaplistsCreate(user, newToken, recipe, name, description, error.data);
			return;
		}

		user.createdMap();
		user.save();

		playlist.save();
		res.redirect(WebRoot ~ "/maplists/" ~ playlist.frontendID);
	}

	@path("/:id")
	void getMap(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id)
	{
		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid)
			return;

		_id = ret.bsonID.toString;

		string token;

		if (ret.public_)
		{
			mixin(authenticateUser!false);

			if (user.valid)
				token = makeToken(req, res);

			bool logView = true;
			if (req.cookies.get("viewed") == _id)
				logView = false;
			res.setCookie("viewed", _id);

			if (user.valid && ret.author == user.bsonID)
				logView = false;

			if (logView)
				ret.didView();

			res.renderMaplistDetails(user, ret, token);
		}
		else
		{
			mixin(authenticateUser!true);

			if (ret.author == user.bsonID)
			{
				if (user.valid)
					token = makeToken(req, res);

				res.renderMaplistDetails(user, ret, token);
			}
		}
	}

	@path("/:id/like")
	void getLikeMap(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id,
			string token)
	{
		mixin(authenticateUser!true);

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid)
			return;

		res.redirect(WebRoot ~ "/maplists/" ~ ret.frontendID);

		if (!validateToken(req, token))
			return;

		ret.doLike(user.bsonID, true);
	}

	@path("/:id/unlike")
	void getUnlikeMap(scope HTTPServerRequest req, scope HTTPServerResponse res, string _id,
			string token)
	{
		mixin(authenticateUser!true);

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid)
			return;

		res.redirect(WebRoot ~ "/maplists/" ~ ret.frontendID);

		if (!validateToken(req, token))
			return;

		ret.doLike(user.bsonID, false);
	}

	@path("/:id/delete")
	void getDeleteMap(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token)
	{
		mixin(authenticateUser!true);

		res.redirect(WebRoot ~ "/maplists");

		if (!validateToken(req, token))
			return;

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid || !(ret.author == user.bsonID || user.admin))
			return;

		Playlist.update(["_id": ret.bsonID], ["$set": ["deleted": true]]);
	}

	@path("/:id/undelete")
	void getUndeleteMap(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token)
	{
		mixin(authenticateUser!true);

		res.redirect(WebRoot ~ "/maplists");

		if (!validateToken(req, token))
			return;

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid || !user.admin)
			return;

		Playlist.update(["_id": ret.bsonID], ["$set": ["deleted": false]]);
	}

	@path("/:id/feature")
	void getFeatureMap(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token)
	{
		mixin(authenticateUser!true);

		res.redirect(WebRoot ~ "/maplists");

		if (!validateToken(req, token))
			return;

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid || !user.admin)
			return;

		Playlist.update(["_id": ret.bsonID], ["$set": ["featured": true]]);
	}

	@path("/:id/unfeature")
	void getUnfeatureMap(scope HTTPServerRequest req, scope HTTPServerResponse res,
			string _id, string token)
	{
		mixin(authenticateUser!true);

		res.redirect(WebRoot ~ "/maplists");

		if (!validateToken(req, token))
			return;

		auto ret = Playlist.tryFindById(_id.replace("-", "").toLower, Playlist.init);
		if (!ret.bsonID.valid || !user.admin)
			return;

		Playlist.update(["_id": ret.bsonID], ["$set": ["featured": false]]);
	}
}

struct RecipeParser
{
@safe:
	Appender!(Playlist.Recipe[]) recipe;

	void load()
	{
		recipe = appender!(Playlist.Recipe[]);
	}

	void processLine(const(char)[] line)
	{
		line = line.strip;

		if (line.startsWith("#", "//") || !line.length)
			return;

		if (line.startsWith("ppv1"))
		{
			// ppv1 "<source>" ("<comments generator>") (filters...)
			// where filters is one of:
			// [minPP/maxPP/minLength/maxLength/minKeys/maxKeys/minBpm/maxBpm/minPassCount/maxPassCount/minUpdateHours/maxUpdateHours/langs/genres]=<int>
			// [minDiff/maxDiff/minOverweight/maxOverweight]=<float>
			// [hd/hr/fl]=yes/no/any
			// dt=yes/no/ht/any

			// keep updated with maplists.js
			line = line[4 .. $].stripLeft; // 4 is "ppv1".length

			Playlist.AutoPPPick pp;
			pp.setSource(RecipeParser.parseString(line));
			if (line.startsWith("'", "\""))
				pp.comments = RecipeParser.parseString(line);

			while (line.length)
			{
				auto space = line.indexOf(' ');
				if (space == -1)
					space = line.length;
				auto arg = line[0 .. space];
				line = line[space .. $].stripLeft;

				auto eq = arg.indexOf('=');
				if (eq == -1)
					eq = arg.length;
				auto varname = arg[0 .. eq].toLower;
				auto value = eq == arg.length ? "yes" : arg[eq + 1 .. $];

				const mod = varname.among!("hd", "hr", "fl", "dt");
				switch (mod)
				{
				case 1: // hd
					pp.hd = parseTriState(value, "HD");
					break;
				case 2: // hr
					pp.hr = parseTriState(value, "HR");
					break;
				case 3: // fl
					pp.fl = parseTriState(value, "FL");
					break;
				case 4: // dt
					pp.dt = parseDT(value);
					break;
				default:
					pp.setVariable(varname, value);
					break;
				}
			}

			(() @trusted => recipe.put(Playlist.Recipe(pp)))();
		}
		else
		{
			// <beatmapID>(,<mode>)(+<mods>)(:<comment>)

			// keep updated with maplists.js
			auto end = line.countUntil!(a => !a.isDigit);
			if (end == -1)
				end = line.length;

			if (end == 0 || end >= 16)
				return;

			Mod[] mods;
			string comment;
			GameMode mode = GameMode.osu;

			auto id = line[0 .. end].to!long;
			line = line[end .. $].stripLeft;

			if (line.startsWith(","))
			{
				line = line[1 .. $].stripLeft;
				int index = line.startsWith("osu", "taiko", "ctb", "mania");
				if (index)
				{
					if (index == 1)
						line = line[3 .. $].stripLeft;
					else if (index == 2)
						line = line[5 .. $].stripLeft;
					else if (index == 3)
						line = line[3 .. $].stripLeft;
					else if (index == 4)
						line = line[5 .. $].stripLeft;

					mode = cast(GameMode)(index - 1);
				}
			}

			if (line.startsWith("+"))
			{
				line = line[1 .. $].stripLeft;
				mods = RecipeParser.parseMods(line);
			}

			auto colon = line.indexOf(':');
			if (colon != -1)
				comment = line[colon + 1 .. $].strip.idup;

			(() @trusted => recipe.put(Playlist.Recipe(Playlist.Pick(Map.findOrInsert(id)
					.bsonID, comment, mods, mode))))();
		}
	}

	static Mod[] parseMods(ref const(char)[] data) @safe
	{
		// keep updated with maplists.js
		size_t i;
		Mod[16] mods;
		string shortForm;
		DataLoop: while (data.length)
		{
			static foreach (mod; EnumMembers!Mod)
			{
				shortForm = mod.shortForm;
				if (data.asUpperCase.startsWith(shortForm.asUpperCase))
				{
					mods[i++] = mod;
					data = data[shortForm.length .. $].stripLeft;
					if (i >= mods.length)
						break DataLoop;
					continue DataLoop;
				}
			}
			break;
		}
		if (i == 0)
			return null;

		return mods[0 .. i].dup;
	}

	static string parseString(ref const(char)[] data) @safe
	{
		// keep updated with maplists.js
		if (data.startsWith("\"", "'"))
		{
			const src = data;
			const quote = data[0];
			data.popFront();
			size_t end = 1;
			bool didEscape, escape;
			while (data.length)
			{
				size_t len;
				const c = decodeFront(data, len);
				if (escape)
				{
					didEscape = true;
					escape = false;
				}
				else if (c == quote)
					break;
				else if (c == '\\')
					escape = true;
				end += len;
			}
			const ret = src[1 .. end];
			data = data.stripLeft;
			if (didEscape)
			{
				size_t i;
				char[] copy = new char[ret.length];
				escape = false;
				foreach (c; ret.representation)
				{
					if (escape)
					{
						copy[i++] = cast(char) c;
						escape = false;
					}
					else if (c == '\\')
						escape = true;
					else
						copy[i++] = cast(char) c;
				}
				return (() @trusted => cast(string) copy[0 .. i])();
			}
			else
				return ret.idup;
		}
		else
		{
			auto end = data.indexOfAny(" ,=;:+()");
			if (end == -1)
				end = data.length;
			const ret = data[0 .. end];
			data = data[end .. $].stripLeft;
			return ret.idup;
		}
	}
}

unittest
{
	const(char)[] data = "bar 'my cool string' foo";
	string s = RecipeParser.parseString(data);
	assert(s == "bar", s);
	assert(data == "'my cool string' foo", data);
	s = RecipeParser.parseString(data);
	assert(s == "my cool string", s);
	assert(data == "foo", data);
	s = RecipeParser.parseString(data);
	assert(s == "foo", s);
	assert(data == "", data);
}

unittest
{
	RecipeParser parser;
	parser.load();
	parser.processLine("ppv1 standard-2019-01-16_07-08.json '{{artist}} - {{title}} gives {{pp}}pp with {{dt}}{{ht}}{{hd}}{{hr}}{{fl}} good accuracy. Overweightedness = {{overweight}}. It\\'s great' minPP=280 maxPP=400 minOverweight=40");

	assert(parser.recipe.data.length == 1);
	Playlist.Recipe recipe = parser.recipe.data[0];
	assert(recipe.isType!(Playlist.AutoPPPick));
	Playlist.AutoPPPick pp = recipe.autopp;

	assert(pp.dataSource == "standard-2019-01-16_07-08.json");
	assert(pp.comments == "{{artist}} - {{title}} gives {{pp}}pp with {{dt}}{{ht}}{{hd}}{{hr}}{{fl}} good accuracy. Overweightedness = {{overweight}}. It's great",
			pp.comments);
	assert(pp.minPP == 280);
	assert(pp.maxPP == 400);
	assert(pp.minOverweight == 40);
}
