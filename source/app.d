module app;

import vibe.vibe;

import orw.settings;
import orw.protocol.types;

import std.getopt;

import db;

import mongoschema;
import mongostore;

import services.scheduled;
import services.startup;

import botpool;

import routes.admin;
import routes.api;
import routes.create;
import routes.lobby;
import routes.maplists;
import routes.registration;

import std.functional;

import osu.api.config : banchoApiBase, banchoApiKey;

import botpool : config, oauthClient;

version (unittest)
{
}
else
	int main(string[] args)
{
	config = readConfig();
	oauthClient = readClient();

	auto options = args.getopt("h|host", &config.adminHosts, "P|port",
			&config.adminPort, std.getopt.config.passThrough);

	if (options.helpWanted)
	{
		defaultGetoptPrinter("OsuOpenRoomsWeb web master", options.options);
		return 1;
	}

	bots.config = config;
	(()@trusted => cast()banchoApiBase = config.bancho.apiBase)();
	(()@trusted => cast()banchoApiKey = config.bancho.apiKey)();

	auto db = connectMongoDB("mongodb://127.0.0.1").getDatabase("osuautohost");
	registerDB(db);

	auto settings = new HTTPServerSettings;
	settings.port = 3000;
	settings.bindAddresses = ["::1", "127.0.0.1"];

	settings.sessionStore = new MongoSessionStore(db);

	registerScheduledServices();
	startupServices();

	runTask(&bots.connect);

	auto router = new URLRouter;
	// Serves files out of public folder
	router.get("*", serveStaticFiles("./public/"));
	HTTPFileServerSettings nodeModulesSettings = new HTTPFileServerSettings;
	nodeModulesSettings.options = HTTPFileServerOption.none;
	nodeModulesSettings.preWriteCallback = (scope req, scope res, ref file) @safe {
		if (!file.endsWith(".js", ".css"))
			file = null;
	};
	router.get("*", serveStaticFiles("./node_modules/", nodeModulesSettings));

	// public routes
	router.registerWebInterface(new APIInterface());
	router.registerWebInterface(new LobbyInterface());
	router.registerWebInterface(new MaplistsInterface());
	router.get("/about", &getAbout);

	// auth routes
	router.get("/login", &getLogin);
	router.get("/logout", &getLogout);
	router.get("/auth/callback", &getAuthCallback);

	// user routes
	router.registerWebInterface(new CreateInterface);
	router.registerWebInterface(new AdminInterface);

	settings.errorPageHandler = toDelegate(&errorHandler);

	listenHTTP(settings, router);

	return runEventLoop();
}

void getAbout(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	mixin(authenticateUser!false);

	res.render!("about.dt", user);
}

void errorHandler(HTTPServerRequest req, HTTPServerResponse res, HTTPServerErrorInfo error) @trusted
{
	renderError(req, res, error.code, error.message);
}

void renderError(scope HTTPServerRequest req, scope HTTPServerResponse res, int code, string error)
{
	mixin(authenticateUser!false);

	auto codeMessage = httpStatusText(code);

	res.statusCode = code;
	res.render!("error.dt", user, code, codeMessage, error);
}
