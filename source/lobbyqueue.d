module lobbyqueue;

import bancho.irc;

import core.time;

import vibe.core.log;
import vibe.core.sync;
import vibe.data.bson;

import std.algorithm;
import std.array;
import std.datetime.systime;
import std.datetime.timezone;
import std.range;
import std.string;

import cachetools.containers.lists;

import master;

import db;
import util.caches;

import webconfig;

@formTemplate("%4$s")
struct CreateOptions
{
	@requiredSetting @settingPlaceholder("Bob's Awesome 4* Host Rotate") @settingLength(48, 1) string name;
	@passwordSetting string password;

	@optionsSetting @settingClass("gamemode") GameMode gamemode;

	@settingPlaceholder("One user per line") @settingRows(3) @multilineSetting string extraMatchReferees;

	@settingHTML("<details>")
	@settingHTML("<summary>Mods</summary>")
	@settingHTML(`<div class="modlist">`)
	@settingClass("mapmod mod_hr") bool hardrock;
	@settingClass("mapmod mod_dt") bool doubleTime;
	@settingClass("mapmod mod_fl") bool flashlight;
	@settingClass("mapmod mod_hd") bool hidden;
	@settingClass("mapmod mod_fi") bool fadeIn;
	@settingClass("mapmod mod_free") bool freemod;
	@settingHTML("</div>")
	@settingHTML("</details>")

	@settingHTML("<details>")
	@settingHTML("<summary>Other</summary>")
	TeamMode teammode;
	ScoreMode scoremode;
	bool lockSlots;
	@settingHTML("</details>")

	@settingRange(1, 16) int slots = 16;
}

enum CreateOptions_allCorrect = ~(ulong.max << __traits(allMembers, CreateOptions).length);

struct CreateOptionsEx
{
	SerializableDuration uptime;
	CreateOptions base;
	string mapMode, mapModeName;
}

struct QueuedLobby
{
	BsonObjectID user;
	int userLimit;
	string username;
	Json modeOptions;
	CreateOptionsEx options;
	SysTime queuedAt;
	InterruptibleTaskCondition condition;

	string mode() @property
	{
		return options.mapMode;
	}
}

__gshared InterruptibleTaskMutex lobbyMutex;
__gshared DList!QueuedLobby queuedLobbies;

void queueLobby(BsonObjectID user, int userLimit, string username,
		Json modeOptions, CreateOptionsEx options, bool multiLobbies)
{
	lobbyMutex.lock();
	scope (exit)
		lobbyMutex.unlock();

	if (!multiLobbies)
		foreach (l; queuedLobbies.range)
			if (l.user == user)
				return;

	queuedLobbies.insert_last(QueuedLobby(user, userLimit, username, modeOptions,
			options, Clock.currTime(UTC()), new InterruptibleTaskCondition(cast(Lockable) new TaskMutex())));
}

// run at startup and master reconnects
void cleanupLobbies()
{
	auto slavePids = slaves.map!"a.pid".array;

	foreach (lobby; ActiveLobby.findRange(query!ActiveLobby.pid.notInArray(slavePids)))
		removeActiveLobby(lobby, "Dead startup cleanup");
}

// timer scheduled
void queueChecker()
{
	lobbyMutex.lock();
	scope (exit)
		lobbyMutex.unlock();

	while (true)
	{
		bool found = false;
		auto range = queuedLobbies.range;
		foreach (l; range)
		{
			string mode = l.mode;
			found = false;

			auto lobbies = ActiveLobby.countByCreator(l.user);
			if (lobbies > l.userLimit)
			{
				auto m = l.condition.mutex;
				m.lock();
				scope (exit)
					m.unlock();
				l.condition.notifyAll();
				queuedLobbies.remove(range._current);
				found = true;
				break;
			}

			RoomSettings settings;
			settings.password = l.options.base.password;
			settings.slots = l.options.base.slots;
			settings.locked = l.options.base.lockSlots;
			settings.teammode = l.options.base.teammode;
			settings.scoremode = l.options.base.scoremode;
			settings.gamemode = l.options.base.gamemode;
			if (l.options.base.hardrock)
				settings.mods ~= Mod.HardRock;
			if (l.options.base.doubleTime)
				settings.mods ~= Mod.DoubleTime;
			if (l.options.base.flashlight)
				settings.mods ~= Mod.Flashlight;
			if (l.options.base.hidden)
				settings.mods ~= Mod.Hidden;
			if (l.options.base.fadeIn)
				settings.mods ~= Mod.FadeIn;
			if (l.options.base.freemod)
				settings.mods ~= Mod.FreeMod;
			settings.refs = chain(only(l.username),
					l.options.base.extraMatchReferees.lineSplitter).filter!"a.length".array;
			settings.invites = [l.username];
			settings.ttl = l.options.uptime;
			settings.owners = [l.username];

			OpRoomSubmitResponse res;
			int pid;

			foreach (slaveID, slave; slaves)
			{
				if (slave.pid && slave.availableModes.canFind!(a => a.id == mode))
				{
					pid = slave.pid;
					res = slaveHandler.submitRoom(cast(int) slaveID,
							OpRoomSubmitRequest(l.options.base.name ~ " [OpenRooms]", settings));
					if (res.created)
						slaveHandler.configRoom(cast(int) slaveID, l.mode, l.modeOptions);
					else
					{
						logWarn("Failed creating lobby for user %s on PID %s: %s",
								l.username, slave.pid, res.error);
						continue;
					}
					found = true;
					break;
				}
			}

			if (found)
			{
				if (res.created)
				{
					auto m = l.condition.mutex;
					m.lock();
					scope (exit)
						m.unlock();

					ActiveLobby lobby = ActiveLobby.tryFindOne(query!ActiveLobby.pid(pid), ActiveLobby.init);
					if (lobby.bsonID.valid)
						removeActiveLobby(lobby, "Replaced by new room.");

					lobby = ActiveLobby.init;
					lobby.pid = pid;
					lobby.lobby = Lobby(l.options.base.name, l.user);
					lobby.lobby.ttl = l.options.uptime;
					lobby.lobby.gameId = res.gameId;
					lobby.lobby.apiId = res.apiId;
					lobby.lobby.settingsMode = l.mode;
					lobby.lobby.settings = l.modeOptions;
					lobby.save();

					channelActiveLobbyCache.put(res.apiId, lobby.bsonID);
					activeLobbyChatLogCache.put(lobby.bsonID, new ChatLog());

					l.condition.notifyAll();
					queuedLobbies.remove(range._current);
				}
				break;
			}
		}
		if (!found)
			break;
	}
}

void removeActiveLobby(ActiveLobby lobby, string reason)
{
	{
		channelActiveLobbyCache.remove(lobby.lobby.apiId);
		activeLobbyChatLogCache.remove(lobby.bsonID);

		lobby.lobby.closeReason = reason;
		lobby.lobby.save();
		lobby.remove();
	}
}
