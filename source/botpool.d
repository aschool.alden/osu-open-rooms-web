module botpool;

import bancho.irc;

import core.time;

import vibe.core.core;
import vibe.core.log;
import vibe.core.net;

import orw.settings;

import master;

__gshared Config config;
__gshared OAuthClient oauthClient;

struct BotPool
{
	@disable this(this);

	Config config;

	void connect()
	{
		slaveHandler.load();
		size_t hostIndex = 0;
		while (true)
		{
			TCPConnection conn;
			try
			{
				logDiagnostic("Connecting to %s:%s", config.adminHosts[hostIndex], config.adminPort);
				conn = connectTCP(config.adminHosts[hostIndex], config.adminPort, null, 0, 60.seconds);
			}
			catch (Exception e)
			{
				logError("Failed to connect to host %s:%s", config.adminHosts[hostIndex], config.adminPort);
				hostIndex = (hostIndex + 1) % config.adminHosts.length;
				if (hostIndex == 0)
					sleep(2.seconds);
				continue;
			}
			managerConnected = true;
			scope (exit)
				managerConnected = false;
			scope (exit)
				conn.close();

			auto host = new RelayManagerTCPSlaveConnection(conn);
			slaveHandler.connection = host;
			slaveHandler.reload();
			runTask({ logDiagnostic("Registering on master"); host.pingTask(); });
			logDebug("Starting receive loop");
			host.receiveLoop();
			host.close();
		}
	}

	/// Returns: true if the user could be authenticated.
	bool testAuth(string username, string password)
	{
		auto bot = new BanchoBot(username, password, config.bancho.host, config.bancho.port);

		bot.onAuthenticated ~= &bot.disconnect;

		if (!bot.connect)
			return false;
		return true;
	}
}

__gshared BotPool bots;
