module hostrotate.utils;
@safe:

import bancho.irc;

import core.time;

import std.algorithm;
import std.array;
import std.conv;
import std.format;
import std.math;

import db;

import slave : rootUser;

import vibe.core.core;
import vibe.core.log;
import vibe.core.sync;

import orw.protocol.types;

struct StartTimer
{
	RoomContext context;
	bool starting, countTaskRunning;
	Duration startTime;
	MonoTime startTrigger;
	TimeLeftText lastSentString;

	OsuRoom room()
	{
		return context.room;
	}

	void queueStart(Duration time, bool allowExtend = false)
	{
		if (starting
			|| room.inProgress
			|| (starting && !allowExtend
				&& MonoTime.currTime + time >= startTrigger + startTime))
			return;

		while (!starting && countTaskRunning)
			sleep(10.msecs);

		if (starting) // got multiple waiting for count task at once, first one first serve
			return;

		startTrigger = MonoTime.currTime;
		startTime = time;
		starting = true;
		if (updateTimeLeft(true) && room)
			room.trySendMessage(lastSentString.text);
		runTask({
			countTaskRunning = true;
			scope (exit)
				countTaskRunning = false;

			string lastGenerated;
			while (starting && MonoTime.currTime < startTrigger + startTime)
			{
				sleep(1.seconds);
				if (room)
					room.bancho.ratelimit(room.channel, false);

				if (room.inProgress)
				{
					starting = false;
					return;
				}

				if (updateTimeLeft(false) && room)
					room.trySendMessage(lastSentString.text);
			}

			if (room.inProgress)
				starting = false;

			if (starting)
			{
				if (room)
					room.start();
				starting = false;
			}
		});
	}

	private struct TimeLeftText
	{
		string _text;
		int[2] _lastState;

		bool update(Duration timeLeft, bool exact)
		{
			if (timeLeft < 1.seconds)
				return false;

			if (exact)
			{
				int minutes, seconds;
				timeLeft.split!("minutes", "seconds")(minutes, seconds);

				if (!updateState([minutes, seconds]))
					return false;

				auto ret = appender!string;
				ret.put("Match starting in");
				if (minutes == 727)
					ret.put("WYSI minutes");
				else if (minutes == 1)
					ret.put(" 1 minute");
				else if (minutes > 1)
				{
					ret.put(" ");
					ret.put(minutes.to!string);
					ret.put(" minutes");
				}

				if (seconds == 1)
					ret.put(" 1 second");
				else if (seconds > 1)
				{
					ret.put(" ");
					ret.put(seconds.to!string);
					ret.put(" seconds");
				}

				if (minutes == 0 && seconds == 0)
					ret.put(" any second now.");
				else
					ret.put(".");

				_text = ret.data;
				return true;
			}

			if (timeLeft <= 90.seconds)
			{
				int secs = cast(int)timeLeft.total!"seconds";

				if (secs > 15)
					secs = cast(int)round(secs / 30.0) * 30;
				else if (secs > 10)
					secs = 10;
				else if (secs > 5)
					secs = 5;

				if (!updateState([0, secs]))
					return false;

				if (secs == 1)
					_text = "Good luck have fun!";
				else
					_text = "Match starting in " ~ secs.to!string ~ " seconds";
				return true;
			}
			else
			{
				int mins = cast(int)round(cast(double)timeLeft.total!"minutes" + cast(double)timeLeft.total!"seconds" / 60.0);

				if (mins > 5)
					mins = cast(int)round(mins / 5.0) * 5;
				else if (mins == 4)
					mins = 3;

				if (!updateState([mins, 0]))
					return false;

				if (mins == 1)
					_text = "Match starting in 1 minute";
				else
					_text = "Match starting in " ~ mins.to!string ~ " minutes";
				return true;
			}
		}

		bool updateState(int[2] state)
		{
			if (_lastState == state)
				return false;
			_lastState = state;
			return true;
		}

		string text() const @property
		{
			return _text;
		}

		bool valid() const @property
		{
			return _text.length > 0;
		}
	}

	private bool updateTimeLeft(bool exact)
	{
		if (!starting)
		{
			lastSentString = TimeLeftText.init;
			return false;
		}

		Duration timeLeft = (startTrigger + startTime) - MonoTime.currTime;
		return lastSentString.update(timeLeft, exact);
	}

	void abortStart(bool abortMatch)
	{
		if (starting)
		{
			if (!room.inProgress)
			{
				starting = false;
				room.trySendMessage("Aborted match start.");
			}
			else if (abortMatch)
				room.abortMatch();
		}
	}
}

struct VoteSkip
{
	/// Base percentage of how many in the room need to trigger this vote for it to pass (rounding up to next player if imprecise)
	/// Percentage gets adjusted depending on various factors. See $(DREF targetPercentage) for more information.
	double basePercentage = 0.5;
	bool inMatch;
	string[20] allUsers;
	uint numUsers;
	string[20] skipUsers;
	uint numSkips;
	bool wantSendMessage;
	bool locked;
	OsuRoom room;

	@disable this(this);

	/// Hooks into the onUserJoin, onUserLeave, onMatchEnd and onMatchStart events and loads the current player list.
	this(OsuRoom room) nothrow
	{
		this.room = room;
		(() @trusted {
			room.onUserJoin ~= &onJoin;
			room.onUserLeave = &onLeave ~ room.onUserLeave;
			room.onMatchEnd ~= &finishMap;
			room.onMatchStart ~= &startMap;
		})();

		resync();
	}

	/// Loads the list of users into this vote with no update events into a room.
	this(string[] users) nothrow
	in (users.length <= allUsers.length)
	{
		allUsers[0 .. users.length] = users;
		numUsers = cast(uint) users.length;
		numSkips = 0;
	}

	~this() nothrow
	{
		if (!room)
			return;

		room.onUserJoin = room.onUserJoin.remove!(a => a == &onJoin);
		room.onUserLeave = room.onUserLeave.remove!(a => a == &onLeave);
		room.onMatchEnd = room.onMatchEnd.remove!(a => a == &finishMap);
		room.onMatchStart = room.onMatchStart.remove!(a => a == &startMap);
		room = null;
	}

	private void onJoin(string user, ubyte, Team) nothrow
	{
		join(user);
	}

	private void onLeave(string user) nothrow
	{
		leave(user);
	}

	private void join(string user) nothrow
	{
		if (users.canFind!(a => a == user) || !room)
			return;

		if (numUsers >= allUsers.length)
		{
			numUsers = 0;
			foreach (slot; room.slots)
				if (slot.name.length)
					allUsers[numUsers++] = slot.name;
		}
		else
		{
			allUsers[numUsers++] = user;
		}
		dumpCurrent();
	}

	private void leave(string user) nothrow
	{
		auto i = users.countUntil!(a => a == user);

		if (i == -1)
			return;

		if (!inMatch && numUsers > 0)
			allUsers[i] = allUsers[--numUsers];

		if (numSkips > 0)
		{
			i = skippers.countUntil!(a => a == user);
			if (i != -1)
				skipUsers[i] = skipUsers[--numSkips];
		}

		dumpCurrent();
	}

	private void dumpCurrent(string why = "") nothrow
	{
		try
		{
			logDiagnostic("%s Users is now: %s %s", why, numUsers, users);
		}
		catch (Exception e)
		{
			logException(e, "error dumping users");
		}
	}

	private void startMap() nothrow
	{
		inMatch = true;
	}

	private void finishMap() nothrow
	{
		inMatch = false;
	}

	/// Target percentage is calculated based on the $(DREF basePercentage) and various other inputs. Namely:
	/// - `inMatch` ($(DREF startMap), $(DREF finishMap)) increases the percentage by +20%, no matter the previous percentage, making it impossible to reach the vote if the current percentage is >80%.
	/// Returns: the percentage of players who must be voting for this vote to pass. The number of players will be ceil(userCount * targetPercentage)
	double targetPercentage() const @property nothrow
	{
		return basePercentage + (inMatch ? 0.2 : 0);
	}

	const(string)[] skippers() const @property nothrow return
	{
		return skipUsers[0 .. min(numSkips, $)];
	}

	const(string)[] users() const @property nothrow return
	{
		return allUsers[0 .. min(numUsers, $)];
	}

	void lock()
	{
		locked = true;
	}

	void unlock()
	{
		locked = false;
	}

	deprecated alias reset = resync;

	/// Loads the user list from the current room. Does nothing if there is no room loaded.
	void resync() nothrow
	{
		if (!room)
			return;

		numSkips = 0;

		numUsers = 0;
		foreach (slot; room.slots)
			if (slot.name.length)
				allUsers[numUsers++] = slot.name;

		dumpCurrent("Resynced");
	}

	bool shouldSkip() @property nothrow
	{
		return numSkips > 0 && numSkips >= requiredSkipCount;
	}

	int requiredSkipCount() @property nothrow
	{
		return cast(int) ceil(numUsers * targetPercentage);
	}

	bool voteSkip(string user, bool logDB = true)
	{
		if (locked || numSkips == skipUsers.length || skippers.canFind(user))
			return false;
		if (!room && !users.canFind(user))
			return false;

		bool started = numSkips == 0;
		skipUsers[numSkips++] = user;
		wantSendMessage = true;
		if (logDB)
			runTask({
				import db : GameUser;

				GameUser.findByUsername(user).voteSkip(started);
			}.repeatOnFail(5, 10.seconds, "log voteSkip"));
		return true;
	}
}

struct MapVote
{
	RoomContext context;
	VoteSkip retryVoter;
	Map map;
	string[32] rated;
	uint numRated;

	this(RoomContext context, Map map, string[] finished)
	{
		this.context = context;
		retryVoter = VoteSkip(finished);
		this.map = map;
	}

	bool valid() @property
	{
		return context !is null && context.room !is null && map.bsonID.valid;
	}

	void addFinished(string user)
	{
		retryVoter.join(user);
	}

	bool voteRetry(string user)
	{
		bool started = retryVoter.numSkips == 0;
		if (!retryVoter.voteSkip(user, false))
			return false;
		runTask({
			import db : GameUser;

			GameUser.findByUsername(user).voteRetry(started);
		});
		return true;
	}

	bool shouldRetry() @property
	{
		return retryVoter.shouldSkip;
	}

	bool wantSendMessage() @property
	{
		return retryVoter.wantSendMessage;
	}

	void rate(string user, int rating)
	{
		const shouldScore = numRated < rated.length && rated[0 .. numRated].canFind(user);
		if (shouldScore)
			rated[numRated++] = user;

		auto u = GameUser.findByUsername(user);
		map.doRating(u.bsonID, rating);

		if (!shouldScore)
			return;
		u.didMapRating();
	}

	bool wait(Duration d)
	{
		if (!context || !context.room)
			return false;

		context.room.trySendMessage(format!"How did you enjoy [https://osu.ppy.sh/b/%d %s - %s [%s]]? Rate using [--/-/0/+/++] - vote retry using !retry"(
				map.mapID, map.info.artist, map.info.title, map.info.difficultyName));
		context.room.setTimer(d);

		try
		{
			context.room.waitForTimer(d + 5.seconds);
		}
		catch (Exception e)
		{
		}

		auto rating = Map.findById(map.bsonID).rating;
		if (context && context.room)
			context.room.trySendMessage("This map now has a rating of " ~ rating.to!string);

		return shouldRetry();
	}
}

class RoomContext
{
	OsuRoom room;
	StartTimer startTimer;
	RoomSettings settings;
	void delegate() close;

	this()
	{
		startTimer.context = this;
	}

	BanchoBot bot() @property
	{
		return rootUser;
	}
}

bool isCommand(string message, string command)
{
	import std.uni : normalize, NFKD;
	import std.string : startsWith;
	import std.range : chain;

	if (!message.length)
		return false;

	return message.normalize!NFKD.startsWith(chain("!", command));
}

auto repeatOnFail(CB)(CB cb, int tries, Duration repeatTime, string what)
{
	import std.functional;
	import std.traits;

	return (Parameters!CB params) nothrow {
		foreach (i; 0 .. tries)
		{
			try
			{
				cb(forward!params);
				return;
			}
			catch (Exception e)
			{
				logException(e, "exception while doing " ~ what);
				try
				{
					sleep(repeatTime);
				}
				catch (Exception e2)
				{
					logException(e2, "failed to sleep");
					break;
				}
			}
		}

		try
		{
			logWarn("Gave up retrying to %s", what);
		}
		catch (Exception e)
		{
			logException(e, "gave up log for " ~ what);
		}
	};
}
