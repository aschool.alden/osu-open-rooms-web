module hostrotate.mode.playlist;
@safe:

import hostrotate.mode;

import orw.protocol.types;

import vibe.core.log;

import core.time;

import std.algorithm;
import std.conv;
import std.format;
import std.random;
import std.string;

import tinyevent;

import db;

import bancho.irc;

class PlaylistMode : HostRotateMode
{
	mixin ModeSingleton;

	struct Config
	{
		Playlist.Pick[] maps;
		bool shuffleOnReset;
		bool resetOnEmpty;
		bool applyMods;
		bool sendComments;
	}

	Config config;
	BsonObjectID playlist;
	int nextMapIndex;
	Playlist.Pick prevMap;
	Map osumap;
	Mod[] prevMods;
	VoteSkip voteSkip;
	MapVote* retryVote;
	bool canStart;

	string[20] finished;
	int numFinished;

	override ModeID id() @property
	{
		return ModeID(PlaylistMode.stringof, 1, "Playlist Mode");
	}

	override ModeSettings settings() @property @trusted
	{
		Json base = Json([
				"maps": Json.init,
				"shuffleOnReset": Json(false),
				"resetOnEmpty": Json(false),
				"applyMods": Json(true),
				"sendComments": Json(true)
				]);

		ModeSetting[] settings;
		settings ~= ModeSetting(["maps"], "Maps", "Pick a maplist to play", SettingValue(MapList()));
		settings ~= ModeSetting(["shuffleOnReset"], "Shuffle maps every iteration",
				null, SettingValue(Checkbox()));
		settings ~= ModeSetting(["resetOnEmpty"],
				"Reset maps when everyone leaves", null, SettingValue(Checkbox()));
		settings ~= ModeSetting(["applyMods"],
				"Force mods provided in maplist (DT and HT are applied in any case)",
				null, SettingValue(Checkbox()));
		settings ~= ModeSetting(["sendComments"],
				"Send notes in chat provided for each map in maplist", null, SettingValue(Checkbox()));

		return ModeSettings(base, settings);
	}

	void setup()
	{
		nextMapIndex = 0;
		prevMap = Playlist.Pick.init;
		voteSkip = VoteSkip(room);
		canStart = true;

		everyoneLeft();
	}

	override void load(RoomContext context, Json args)
	{
		config.shuffleOnReset = args["shuffleOnReset"].get!bool;
		config.resetOnEmpty = args["resetOnEmpty"].get!bool;
		config.applyMods = args["applyMods"].get!bool;
		config.sendComments = args["sendComments"].get!bool;
		config.maps = null;

		this.context = context;
		room.clearhost();
		room.onPlayersReady ~= &nothrowOrEndRoom!everyoneReady;
		room.onUserJoin ~= &nothrowOrEndRoom!userJoin;
		room.onUserLeave ~= &nothrowOrEndRoom!userLeave;
		room.onMatchStart ~= &nothrowOrEndRoom!matchStart;
		room.onMatchEnd ~= &nothrowOrEndRoom!matchEnd;
		room.onPlayerFinished ~= &nothrowOrEndRoom!userFinished;

		if (readyForSwitch())
			return;

		playlist = BsonObjectID.fromString(args["maps"].get!string);
		Playlist p = Playlist.tryFindById(playlist, Playlist.init);
		if (!p.bsonID.valid)
		{
			room.trySendMessage("Failed to find playlist.");
			return;
		}
		config.maps = p.picks;
		p.didUse();

		setup();
		skipMap();
	}

	override void unload()
	{
		room.onPlayersReady = room.onPlayersReady.remove!(a => a == &nothrowOrEndRoom!everyoneReady);
		room.onUserJoin = room.onUserJoin.remove!(a => a == &nothrowOrEndRoom!userJoin);
		room.onUserLeave = room.onUserLeave.remove!(a => a == &nothrowOrEndRoom!userLeave);
		room.onMatchEnd = room.onMatchEnd.remove!(a => a == &nothrowOrEndRoom!matchEnd);
		room.onPlayerFinished = room.onPlayerFinished.remove!(a => a == &nothrowOrEndRoom!userFinished);
	}

	mixin NothrowRoomHandler;

	void everyoneLeft()
	{
		if (config.resetOnEmpty)
		{
			nextMapIndex = 0;
			skipMap();
		}
	}

	void everyoneReady()
	{
		unreadyForSwitch();
		if (!canStart)
			return;

		context.startTimer.queueStart(context.settings.durations.allReadyStartDuration);
	}

	void userJoin(string, ubyte, Team)
	{
		int players;
		foreach (player; room.slots)
			if (player != OsuRoom.Settings.Player.init)
				players++;

		if (players == 1)
		{
			if (room.inProgress)
				room.abortMatch();
			canStart = true;
			room.trySendMessage("Ready to start game!");
		}
		else if (players == 2)
		{
			if (!canStart)
				return;
			context.startTimer.queueStart(context.settings.durations.startGameDuration);
		}
	}

	void userLeave(string user)
	{
		if (!voteSkip.wantSendMessage && voteSkip.shouldSkip())
		{
			skipMap();
		}

		foreach (player; room.slots)
			if (player != OsuRoom.Settings.Player.init)
				return;
		context.startTimer.abortStart(true);
		everyoneLeft();
	}

	void matchEnd()
	{
		if (readyForSwitch())
			return;

		if (context.settings.durations.retryEvaluationDuration.total!"seconds" <= 0)
		{
			skipMap();
			return;
		}

		voteSkip.lock();
		scope (exit)
			voteSkip.unlock();

		canStart = false;

		try
		{
			MapVote localVote = MapVote(context, osumap, finished[0 .. numFinished]);
			retryVote = (() @trusted => &localVote)();
			scope (exit)
				retryVote = null;

			if (localVote.wait(context.settings.durations.retryEvaluationDuration))
			{
				queueStart(context.settings.durations.retryGameDuration);
				return;
			}
		}
		catch (Exception e)
		{
			logException(e, "Exception in retry vote");
		}

		skipMap();
	}

	void matchStart()
	{
		if (!canStart)
		{
			room.abortMatch();
			return;
		}

		unreadyForSwitch();

		Playlist.didPlay(playlist);

		numFinished = 0;

		foreach (player; room.slots)
			if (player != OsuRoom.Settings.Player.init)
				return;

		room.abortMatch();
	}

	void userFinished(string user, long score, bool passed)
	{
		if (numFinished >= finished.length || numFinished < 0)
			return;

		string name = user.fixUsername;
		finished[numFinished++] = name;
		if (retryVote !is null)
			retryVote.addFinished(name);
	}

	/// Returns: true if message was processed as relevant.
	override bool onMessage(string room, string sender, string message, bool direct)
	{
		sender = sender.fixUsername;
		if (message.isCommand("queue"))
		{
			context.bot.trySendMessage(room, format!"%s: I've got %d more maps before repeating %d maps."(sender,
					config.maps.length - nextMapIndex, config.maps.length));
			return true;
		}
		else if (message.isCommand("skip"))
		{
			if (direct)
			{
				context.bot.trySendMessage(room, "Skips only work when sent in the room.");
			}
			else
			{
				if (voteSkip.voteSkip(sender))
				{
					context.bot.ratelimit(room, false);
					if (voteSkip.wantSendMessage)
					{
						if (voteSkip.shouldSkip())
							skipMap();
						else
							context.bot.trySendMessage(room, format!"Skip progress: %d/%d"(voteSkip.numSkips,
									voteSkip.requiredSkipCount));
					}
				}
			}
			return true;
		}
		else if (message.isCommand("retry"))
		{
			if (direct)
			{
				context.bot.trySendMessage(room, "Retry only works when sent in the room.");
			}
			else if (retryVote !is null)
			{
				if (retryVote.voteRetry(sender))
				{
					context.bot.ratelimit(room, false);
					if (retryVote is null)
						return true;

					if (retryVote.wantSendMessage)
					{
						if (retryVote.shouldRetry())
							context.bot.trySendMessage(room, "The map will be retried.");
						else
							context.bot.trySendMessage(room, format!"Retry progress: %d/%d"(retryVote.retryVoter.numSkips,
									retryVote.retryVoter.requiredSkipCount));
					}
				}
			}
			return true;
		}
		else if (retryVote !is null)
		{
			auto i = message.strip.among!("--", "-", "0", "+", "++");
			if (i > 0)
			{
				bool hadFinished = finished[0 .. numFinished].canFind(sender);
				if (!hadFinished)
				{
					context.bot.trySendMessage(room,
							sender ~ ": only people who just finished this map may give a rating.");
				}
				else
				{
					i--;
					if (retryVote !is null)
						retryVote.rate(sender, i - 2);

					if (direct)
						context.bot.trySendMessage(room, "vote counted!");
				}
				return true;
			}
		}
		return false;
	}

	void queueStart(Duration d)
	{
		canStart = true;

		int players;
		foreach (player; room.slots)
			if (player != OsuRoom.Settings.Player.init)
				players++;

		// if (players > 1)
			context.startTimer.queueStart(d, true);
		// else if (players == 1)
		// 	context.room.trySendMessage("Ready to start game!");
	}

	override void skipMap()
	{
		canStart = false;
		if (readyForSwitch())
			return;

		voteSkip.lock();
		scope (exit)
			voteSkip.unlock();

		queueStart(context.settings.durations.startGameDuration);

		if (!config.maps.length)
			return;

		if (nextMapIndex == 0)
		{
			if (config.shuffleOnReset)
				config.maps.randomShuffle();
		}

		auto map = config.maps[nextMapIndex % $];
		nextMapIndex = cast(int)((nextMapIndex + 1) % config.maps.length);

		if (map.map != prevMap.map || map.mode != prevMap.mode)
		{
			osumap = Map.findById(map.map);
			room.mapAndMode(osumap.mapID.to!string, map.mode);
		}

		if (map.mods != prevMap.mods)
		{
			if (config.applyMods)
			{
				if (map.mods != prevMods)
					room.mods = map.mods;
				prevMods = map.mods;
			}
			else
			{
				auto wantDT = context.settings.mods.canFind(Mod.DoubleTime);
				auto wantHT = context.settings.mods.canFind(Mod.HalfTime);
				auto wantFreeMod = context.settings.mods.canFind(Mod.FreeMod);

				auto hasDT = map.mods.canFind(Mod.DoubleTime);
				auto hasHT = map.mods.canFind(Mod.HalfTime);

				if (hasDT == wantDT && hasHT == wantHT)
				{
					if (prevMods != context.settings.mods)
						room.mods = context.settings.mods;
					prevMods = context.settings.mods;
				}
				else
				{
					Mod[] pick;
					if (hasDT && !wantDT)
						pick = context.settings.mods ~ Mod.DoubleTime;
					else if (hasHT && !wantHT)
						pick = context.settings.mods ~ Mod.HalfTime;
					else
						pick = context.settings.mods ~ map.mods;

					if (prevMods != pick)
						room.mods = pick;
					prevMods = pick;
				}
			}
		}
		prevMap = map;

		if (map.comment.length && config.sendComments)
		{
			// prepend null-width to make it not run any bancho commands
			room.trySendMessage("\u200b" ~ map.comment);
		}

		voteSkip.reset();
		canStart = true;
	}
}
