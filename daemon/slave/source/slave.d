module slave;
@safe:

import core.time;

import vibe.core.core;
import vibe.core.log;
import vibe.core.net;
import vibe.data.bson;

import orw.protocol.types;
import orw.proto_translate;
import orw.settings;
import orw.tcp;

import std.exception;
import std.string;
import std.traits;

import bancho.irc;

import hostrotate.mode;

import db;
import dm;

shared const Config config;
__gshared BanchoBot _rootUser;
__gshared SlaveState _state = new SlaveState();

BanchoBot rootUser() @trusted nothrow
{
	return cast()_rootUser;
}

SlaveState state() @trusted nothrow
{
	return cast()_state;
}

void connectBancho() @trusted
{
	cast()_rootUser = new BanchoBot(config.bancho.username, config.bancho.password,
			config.bancho.host, config.bancho.port);
	rootUser.doRatelimit = true;
	state.registerEvents();
	// TODO: make ratelimit go through manager
	runTask(() @trusted {
		while (true)
		{
			runTask({ rootUser.waitUntilLoggedIn(); state.onBanchoReconnect(); });
			rootUser.connect();
			logDiagnostic("Got disconnected from bancho...");
			sleep(2.seconds);
		}
	});
}

class ClientTCPSlaveConnection : TCPSlaveConnection
{
@safe:
	Timer pingTimer;
	bool closed;

	this(string host, ushort port, Duration timeout = 20.seconds)
	{
		super(host, port, timeout);
	}

	this(NetworkAddress addr, Duration timeout = 20.seconds)
	{
		super(addr, timeout);
	}

	/// Creates a new TCPSlaveConnection instance and takes control of the passed tcp connection.
	/// The receive loop needs to be started using receiveLoop(). It is run synchroniously.
	this(TCPConnection connection)
	{
		super(connection);
	}

	override void received(string type, Bson data) @trusted
	{
		if (pingTimer)
			pingTimer.rearm(15.seconds, true);
		logDebugV("Got %s message", type);
		handlePacket!(PacketDirection.hostToSlave)(state, type, data);
	}

	void register(string password) @trusted
	{
		state.info.availableModes = AvailableModeIDs;
		sendPacket(OpRegister(password, state.info));
		pingTimer = setTimer(15.seconds, wrapTask(&pingLoop), true);
		state.onConnect();
	}

	void shutdown()
	{
		closed = true;
		if (pingTimer)
			pingTimer.stop();
	}

	private void pingLoop() nothrow
	{
		try
		{
			sendPing();
			runTask({
				if (!waitPong(5.seconds))
				{
					if (closed)
						return;
					close();
					logError("Didn't get pong after 6 seconds");
				}
			});
		}
		catch (Exception e)
		{
			close();
			logError("Failed to ping: %s", e.msg);
		}
	}

	void pingTask()
	{
		pingTimer.rearm(30.seconds, true);
	}

	void close() nothrow
	{
		connection.close();
		if (pingTimer)
			pingTimer.stop();
	}
}

class SlaveState
{
	ClientTCPSlaveConnection connection;
	SlaveInfo info;
	bool handleDMs;
	bool shutdownQueued;
	bool closingRoom;
	RoomContext context;
	Timer roomTimer;
	DMHandler dmhandler;
	bool waitAccept;
	Timer acceptTimer;

	HostRotateMode rotateMode;

	BanchoBot bot() @trusted
	{
		return rootUser;
	}

	OsuRoom room(bool allowNull = false)
	{
		if (!context.room && !allowNull)
			throw new Exception("Room is null!");
		return context.room;
	}

	void registerEvents()
	{
		context = new RoomContext();
		context.close = &genericClose;

		dmhandler.response = (room, message) @safe {
			foreach (line; message.lineSplitter)
			{
				line = line.strip;
				if (line.length)
					(() @trusted => bot.trySendMessage(room, line))();
			}
		};

		bot.onDirectMessage ~= (msg) {
			try
			{
				onMessage(msg.target, msg.sender, msg.message);
			}
			catch (Exception e)
			{
				logException(e, "Failed processing direct message");
			}
		};
	}

	void genericClose()
	{
		if (shutdownQueued)
			closeRoom("A shutdown was queued");
		else
			closeRoom("The room was requested to be closed");
	}

	void loadRoom(OsuRoom room)
	{
		this.context.room = room;
		room.onClosed ~= () nothrow {
			closeRoom("The room has been closed");
			closingRoom = false;
			if (shutdownQueued)
			{
				if (connection)
					connection.close();
				exitEventLoop(true);
			}
		};
		room.onMessage ~= tryDelegate((Message msg) {
			onMessage(msg.target, msg.sender, msg.message);
		});
		room.onMatchStart ~= tryDelegate(&onMatchStart);
		room.onMatchEnd ~= tryDelegate(&onMatchFinish);
		room.onBeatmapPending ~= tryDelegate(&onBeatmapPending);
		room.onBeatmapChanged ~= tryDelegate(&onBeatmapChanged);
		room.onUserJoin ~= tryDelegate(&onUserJoin);
		room.onUserMove ~= tryDelegate(&onUserMove);
		room.onUserLeave ~= tryDelegate(&onUserLeave);
		room.onPlayerFinished ~= tryDelegate(&onUserFinish);
		room.onUserHost ~= tryDelegate(&onUserHost);
		room.onHostCleared ~= tryDelegate(&onHostCleared);
		closingRoom = false;
	}

	void onDisconnect()
	{
	}

	void onConnect()
	{
		handleDMs = false;
	}

	void onMatchStart()
	{
		if (!connection)
			return;

		connection.sendPacket(OpMatchStart());

		foreach (user; room.slots[])
			if (user != OsuRoom.Settings.Player.init)
				GameUser.findByUsername(user.name).didStart();
	}

	void onMatchFinish()
	{
		if (!connection)
			return;

		connection.sendPacket(OpMatchFinish());
	}

	void onBeatmapPending()
	{
		if (!connection)
			return;

		connection.sendPacket(OpBeatmapPending());
	}

	void onBeatmapChanged(BeatmapInfo info)
	{
		if (!connection)
			return;

		logInfo("Beatmap change: %s", info);

		connection.sendPacket(OpBeatmapChanged(info));
	}

	void onUserFinish(string user, long score, bool passed)
	{
		GameUser.findByUsername(user).didFinish();
	}

	void onUserJoin(string user, ubyte slot, Team team)
	{
		if (!connection)
			return;

		OpPlayerChange change = OpPlayerChange(user);
		change.slot = slot;
		if (team != Team.None)
			change.team = team;
		connection.sendPacket(change);

		GameUser.findByUsername(user).didJoin();
	}

	void onUserMove(string user, ubyte slot)
	{
		if (!connection)
			return;

		OpPlayerChange change = OpPlayerChange(user);
		change.slot = slot;
		connection.sendPacket(change);
	}

	void onUserLeave(string user)
	{
		if (!connection)
			return;

		OpPlayerChange change = OpPlayerChange(user);
		change.leave = true;
		connection.sendPacket(change);
	}

	void onUserHost(string user)
	{
		if (!connection)
			return;

		OpPlayerChange change = OpPlayerChange(user);
		change.host = true;
		connection.sendPacket(change);
	}

	void onHostCleared()
	{
		if (!connection)
			return;

		connection.sendPacket(OpHostCleared());
	}

	void onMessage(string room, string user, string message)
	{
		if (!connection)
			return;

		if ((handleDMs && !room.startsWith('#')) || (this.context.room && room == this.room.channel))
			connection.sendPacket(OpPlayerMessage(room, user, message));

		logDebug("Processing message [%s] %s: %s", room, user, message);

		if (!room.startsWith('#'))
		{
			if (this.context.room && this.room.hasPlayer(user))
			{
				if (rotateMode.onMessage(user, user, message, true))
					return;
			}
			else if (handleDMs)
			{
				dmhandler.handle(user, user, message, true);
			}
		}
		else if (context.room && room == this.room.channel)
		{
			if (rotateMode.onMessage(room, user, message, false))
				return;
			dmhandler.handle(room, user, message, false);
		}
	}

	void onBanchoReconnect()
	{
		if (context.room)
		{
			try
			{
				loadRoom(bot.fromUnmanaged(room.channel, room.mpid));
			}
			catch (Exception e)
			{
				closeRoom("Error re-owning after bancho reconnect: " ~ e.msg);
			}
		}
	}

	void closeRoom(string reason) nothrow
	{
		info.available = true;
		info.apiId = null;
		info.gameId = null;
		if (context.room)
			context.room.close();
		context.room = null;
		roomTimer.stop();
		assumeWontThrow(connection.sendPacket(OpClosed(reason)));
	}

	void onPacket(OpManageDMs)
	{
		handleDMs = true;
	}

	void onPacket(OpRoomSubmitRequest room)
	{
		if (info.available)
		{
			info.available = false;
			OsuRoom r;
			try
			{
				r = bot.createRoom(room.name);
			}
			catch (Exception e)
			{
				info.available = true;
				connection.sendPacket(OpRoomSubmitResponse(false, "Failed creating room: " ~ e.msg));
				return;
			}
			loadRoom(r);
			context.settings = room.settings;
			info.apiId = this.room.room;
			info.gameId = this.room.mpid;

			waitAccept = true;
			connection.sendPacket(OpRoomSubmitResponse(true, null, info.gameId, info.apiId));

			if (room.settings.password.set)
				this.room.password = room.settings.password;

			if (room.settings.teammode || room.settings.scoremode)
				this.room.set(room.settings.teammode, room.settings.scoremode,
						cast(ubyte) room.settings.slots);
			else if (room.settings.slots != 8)
				this.room.size = cast(ubyte) room.settings.slots;

			this.room.addRefs(room.settings.refs);
			this.room.mods = room.settings.mods;
			foreach (invite; room.settings.invites)
				this.room.invite(invite);

			if (room.settings.locked)
				this.room.locked = true;

			Duration ttl = room.settings.ttl.value;
			if (ttl > Duration.zero)
			{
				logDebug("Room has TTL of %s", ttl);
				roomTimer = (() @trusted => setTimer(ttl, wrapTask(&timeoutRoom), false))();
			}
			else
			{
				logDebug("Room has no TTL");
			}

			if (waitAccept)
				acceptTimer = (() @trusted => setTimer(30.seconds, wrapTask(&didntAcceptRoom), false))();
		}
		else
		{
			connection.sendPacket(OpRoomSubmitResponse(false,
					"Can't create two rooms", info.gameId, info.apiId, true));
		}
	}

	void onPacket(OpRoomSubmitAccept accepted)
	{
		if (!context.room || !waitAccept)
			return;

		if (accepted.gameId != info.gameId)
		{
			logError("Received invalid ID with OpRoomSubmitAccept: %s (wanted %s)",
					accepted.gameId, info.gameId);

			room.trySendMessage("Room creation had an internal error and will need to be closed");
			sleep(10.seconds);
			closeRoom("Accept packet didn't have expected gameId.");
		}
		else
		{
			waitAccept = false;
			if (acceptTimer)
				acceptTimer.stop();
		}
	}

	void onPacket(OpRoomUpdateDurationsRequest durations)
	{
		context.settings.durations = durations;
	}

	void onPacket(OpModeSettingsRequest settingsRequest)
	{
		foreach (Available; AvailableModes)
			if (Available.info.id == settingsRequest.mode)
			{
				auto inst = Available.getInstance();
				auto settings = inst.settings;
				connection.sendPacket(OpModeSettingsResponse(inst.id, settings));
				return;
			}
		connection.sendPacket(OpModeSettingsResponse.init);
	}

	void onPacket(OpRoomModeSettings playlist)
	{
		if (context.room)
		{
			logInfo("Changing room mode to %s", playlist.mode);
			switchMode(playlist.mode, playlist.config);
		}
	}

	void onPacket(OpConfigureSkip skipSettings)
	{
		if (context.room)
		{
		}
	}

	void onPacket(OpClose close)
	{
		if (context.room)
		{
			if (closingRoom)
				return;

			closingRoom = true;
			if (close.finishMap)
				room.trySendMessage(
						"INFO: This room will be closed after the next map finished. " ~ close.reason);

			if (!close.finishMap || rotateMode.waitForSwitch())
			{
				room.trySendMessage("Closing room now. " ~ close.reason);
				sleep(10.seconds);
				closeRoom(close.reason);
			}
		}
		else
		{
			connection.sendPacket(OpClosed(close.reason));
		}
	}

	void timeoutRoom()
	{
		if (!context.room)
			return;

		room.trySendMessage(
				"This room will be closed after the map finished. The room has reached the time limit.");
		rotateMode.waitForSwitch();
		if (context.room && room.isOpen)
			room.trySendMessage("Closing room now.");
		sleep(10.seconds);
		closeRoom("The room has reached the time limit.");
	}

	void didntAcceptRoom()
	{
		if (!waitAccept || !context.room)
			return;

		room.trySendMessage("An internal error occured while creating the room. It will be shut down");
		sleep(10.seconds);
		closeRoom("No accept packet was sent");
	}

	void onPacket(OpConfigMessage message)
	{
		if (context.room)
		{
			room.trySendMessage(message.message);
		}
	}

	void onPacket(OpSendPrivateMessage message)
	{
		bot.trySendMessage(message.user, message.message);
	}

	void onPacket(OpSimulatePlayerMessage message)
	{
		onMessage(message.room, message.name, message.message);
	}

	void onPacket(QueueShutdown)
	{
		if (context.room)
		{
			shutdownQueued = true;
			onPacket(OpClose.init);
		}
		else
		{
			shutdownQueued = true;
			if (connection)
				connection.close();
			exitEventLoop(true);
		}
	}

	bool switchMode(string id, Json data)
	{
		if (!context.room)
			return false;

		HostRotateMode mode;

		foreach (Available; AvailableModes)
			if (Available.info.id == id)
			{
				mode = Available.getInstance();
				break;
			}

		if (!mode)
			return false;

		if (rotateMode)
			rotateMode.unload();
		rotateMode = mode;
		rotateMode.context = context;
		rotateMode.load(context, data);
		return true;
	}
}

auto tryDelegate(T, size_t line = __LINE__)(T fun) if (isDelegate!T)
{
	return (Parameters!T params) nothrow @safe {
		try
		{
			fun(params);
		}
		catch (Exception e)
		{
			import std.conv : to;

			logException(e, "Error in delegate on line " ~ line.to!string);
		}
	};
}
