module dm;
@safe:

import db;

import hostrotate.utils;

import std.algorithm;
import std.conv;
import std.string;
import std.process;

import core.time;

import cachetools.containers.hashmap;

import app : getUptime;
import slave : config;

struct RanCommand
{
	MonoTime at;
	string what;
}

struct DMHandler
{
	alias Response = @safe void delegate(string room, string message);

	Response response;

	HashMap!(string, RanCommand) userMessages;

	void handle(string room, string username, string message, bool direct)
	{
		if (config.test)
			return;

		auto lastCall = userMessages.get(username, RanCommand.init);
		if (MonoTime.currTime - lastCall.at < 3.seconds)
			return;
		if (MonoTime.currTime - lastCall.at < 10.seconds && message.isCommand(lastCall.what))
			return;

		if (message.isCommand("help") || message.isCommand("info"))
		{
			string what = message.isCommand("info") ? "info" : "help";
			userMessages.put(username, RanCommand(MonoTime.currTime, what));
			if (direct)
				response(room, `
					Open lobby management bot - keeping lobbying as vanilla as possible. Web: https://openrooms.app - Source: https://gitlab.com/WebFreak001/osu-open-rooms-web/
					Commands:
					- !help - show this help response
					- !me - show statistics about your play history
					- !skip - skip the current map
					- !queue - view queue information
				`);
			else
				response(room, "Open lobby management bot - keeping lobbying as vanilla as possible. Web: https://openrooms.app - Source: https://gitlab.com/WebFreak001/osu-open-rooms-web/ - DM me !info for more");
		}
		else if (message.isCommand("me"))
		{
			userMessages.put(username, RanCommand(MonoTime.currTime, "me"));
			auto user = GameUser.tryFindOne(query!GameUser.username(username));
			if (user.isNull)
				response(room, "No play information for user " ~ username ~ " found.");
			else
			{
				response(room,
						username ~ ": You left as host " ~ user.get.hostLeaves.length.to!string ~ " times and got penalized for it "
						~ user.get.numHostLeavePenalties.to!string
						~ " times. You have joined auto host " ~ user.get.joins.to!string ~ " times.");
				response(room,
						username ~ ": So far " ~ user.get.picksGotSkipped.to!string ~ " of your map picks got skipped. You have played "
						~ user.get.playStarts.to!string ~ " times and finished "
						~ user.get.playFinishes.to!string ~ " times out of these.");
			}
		}
		else if (message.isCommand("uptime"))
		{
			userMessages.put(username, RanCommand(MonoTime.currTime, "uptime"));
			response(room, "You are chatting with node " ~ thisProcessID.to!string(
					36) ~ " which has been up for " ~ getUptime().toString);
		}
	}
}
