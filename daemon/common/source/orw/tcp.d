module orw.tcp;

import core.time;

import std.bitmanip;

import vibe.core.core;
import vibe.core.log;
import vibe.core.net;
import vibe.data.bson;

import orw.protocol.serialize;

abstract class TCPSlaveConnection : SlaveConnection
{
@safe:
	TCPConnection connection;
	Task task;

	this(string host, ushort port, Duration timeout = 20.seconds)
	{
		super();
		connection = connectTCP(host, port, null, 0, timeout);

		setup();
	}

	this(NetworkAddress addr, Duration timeout = 20.seconds)
	{
		super();
		connection = connectTCP(addr, anyAddress, timeout);

		setup();
	}

	/// Creates a new TCPSlaveConnection instance and takes control of the passed tcp connection.
	/// The receive loop needs to be started using receiveLoop(). It is run synchroniously.
	this(TCPConnection connection)
	{
		super();
		this.connection = connection;
	}

	override void send(Bson value) nothrow
	{
		try
		{
			connection.write(value.data);
			connection.flush();
		}
		catch (Exception e)
		{
			debug logError("Error writing %s to socket: %s", value, (() @trusted => e.toString)());
			else logError("Error writing %s to socket: %s", value, e.msg);
			connection.close();
			if (task)
				task.interrupt();
		}
	}

	private void setup()
	{
		runTask(&receiveLoop);
	}

	/// Synchroniously processes all incoming messages until connection is terminated.
	/// Already called in a new task by constructors taking in network addresses.
	void receiveLoop()
	{
		scope (exit)
			task = Task.init;

		try
		{
			task = Task.getThis();
			ubyte[4] lenBuffer;
			while (connection.connected)
			{
				if (!connection.waitForData())
					break;
				connection.read(lenBuffer[]);
				auto len = lenBuffer.littleEndianToNative!uint;
				ubyte[] data = new ubyte[len];
				data[0 .. 4] = lenBuffer;
				connection.read(data[4 .. $]);
				runTask(&process, Bson(Bson.Type.object, (() @trusted => cast(immutable(ubyte)[]) data)()));
			}
		}
		catch (InterruptException e)
		{
			debug logError("InterruptException in receiveLoop: %s", (() @trusted => e.toString)());
			else logError("InterruptException in receiveLoop: %s", e.msg);
		}
	}
}
