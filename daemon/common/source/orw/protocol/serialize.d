module orw.protocol.serialize;

import core.time;

import std.conv;
import std.datetime.systime;
import std.exception : assumeWontThrow;

import orw.protocol.types;

import vibe.core.core;
import vibe.core.log;
import vibe.core.sync;
import vibe.data.bson;

@safe:
Bson serializePacket(T)(long seq, T packet)
{
	return Bson([
			"s": Bson(seq),
			"t": Bson(T.stringof),
			"d": serializeToBson(packet)
			]);
}

Bson serializePacket(long seq, string type, Bson data) nothrow
{
	return assumeWontThrow(Bson(["s": Bson(seq), "t": Bson(type), "d": data]));
}

Bson serializePing(long seq, long stdTime) nothrow
{
	return assumeWontThrow(Bson(["s": Bson(seq), "t": Bson("ping"), "d": Bson(stdTime)]));
}

Bson serializePong(long seq, long response) nothrow
{
	return assumeWontThrow(Bson(["s": Bson(seq), "t": Bson("pong"), "d": Bson(response)]));
}

Timer.Callback wrapTask(Timer.Callback fun) @safe nothrow
{
	return () @safe nothrow{ runTask(fun); };
}

void delegate() wrapTask(void delegate() fun) @safe nothrow
{
	return () { runTask(fun); };
}

abstract class SlaveConnection
{
@safe:
	abstract void send(Bson value) nothrow;
	abstract void received(string type, Bson data);

	this()
	{
		pingMutex = new InterruptibleTaskMutex();
		pingCondition = new InterruptibleTaskCondition(cast(Lockable) pingMutex);
	}

	void sendPacket(T)(T value)
	{
		send(serializePacket(++thisSeq, value));
	}

	void sendPacket(string type, Bson data) nothrow
	{
		send(serializePacket(++thisSeq, type, data));
	}

	void sendPing()
	{
		if (!ponged)
			return;

		pingMutex.lock();
		if (!ponged)
			return;
		ponged = false;
		auto seq = pingSeq = ++thisSeq;
		pingMutex.unlock();

		send(serializePing(seq, Clock.currStdTime));
	}

	bool waitPong(Duration timeout)
	{
		if (ponged)
			return true;

		pingMutex.lock();
		scope (exit)
			pingMutex.unlock();

		if (ponged)
			return true;
		if (!pingCondition.wait(timeout))
			return false;
		return true;
	}

	long slaveSeq, thisSeq;
	long pingSeq;
	bool ponged = true;
	InterruptibleTaskMutex pingMutex;
	InterruptibleTaskCondition pingCondition;

	void process(Bson raw)
	{
		auto time = Clock.currStdTime;

		if (raw.type != Bson.Type.object)
			throw new Exception("Malformed packet, expected it to be an object and not: " ~ raw.toString);

		Bson[string] value = raw.get!(Bson[string]);

		if ("s" !in value || "t" !in value)
			throw new Exception(
					"Malformed packet, expected 's' (sequence) and 't' (type) to be in packet: "
					~ raw.toString);

		auto seq = value["s"].toJson.to!long;
		auto type = value["t"].toJson.to!string;
		auto data = "d" in value;

		int sleptMs;
		while (seq < slaveSeq + 1)
		{
			if (sleptMs > 400)
				throw new Exception("Packet order timeout, expected packet " ~ (slaveSeq + 1)
						.to!string ~ " to arrive first, but got " ~ seq.to!string ~ "!");
			sleep(1.msecs);
			sleptMs++;
		}

		if (type == "ping")
		{
			if (data)
			{
				long sentTime = data.toJson.to!long;
				auto ping = (time - sentTime).hnsecs;
				logDebugV("master-slave ping is %s", ping);
			}

			send(serializePong(++thisSeq, seq));
		}
		else if (type == "pong")
		{
			if (!data)
				throw new Exception("Malformed pong packet, needs data");

			long id = data.toJson.to!long;
			if (id != pingSeq)
			{
				logDebug("Dropping unknown pong seq %s", id);
				return;
			}

			pingMutex.lock();
			scope (exit)
				pingMutex.unlock();

			ponged = true;
			pingCondition.notifyAll();
		}
		else
		{
			if (data)
				received(type, *data);
			else
				received(type, Bson.init);
		}
	}
}
