module orw.settings;
@safe:

import standardpaths;

import std.path;

import vibe.core.file;
import vibe.core.log;
import vibe.data.json;

struct Config
{
	struct Bancho
	{
		string username;
		string password;
		@optional string host = "irc.ppy.sh";
		@optional ushort port = 6667;
		@optional string apiBase = "https://osu.ppy.sh/api";
		@optional string apiKey;
	}

@optional:
	string password;
	string[] hosts = ["127.0.0.1", "::1"];
	ushort port = 19124;
	string[] adminHosts = ["127.0.0.1", "::1"];
	ushort adminPort = 19125;
	bool limitedMode;
	Bancho bancho;
	bool test;
}

Config readConfig()
{
	foreach (configDir; standardPaths(StandardPath.config, "osu-open-rooms"))
	{
		auto config = buildPath(configDir, "config.json");
		logDiagnostic("Testing %s for config", config);
		if (existsFile(config))
			return deserializeJson!Config(parseJsonString(readFileUTF8(config), config));
	}

	return Config.init;
}

struct OAuthClient
{
	string secret;
	string redirect;
	int id;
}

OAuthClient readClient()
{
	foreach (configDir; standardPaths(StandardPath.config, "osu-open-rooms"))
	{
		auto client = buildPath(configDir, "client.json");
		logDiagnostic("Testing %s for client", client);
		if (existsFile(client))
			return deserializeJson!OAuthClient(parseJsonString(readFileUTF8(client), client));
	}

	return OAuthClient.init;
}
