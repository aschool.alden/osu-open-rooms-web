function convertTimezones() {
	var locals = document.getElementsByClassName("utc");
	for (var i = 0; i < locals.length; i++) {
		var t = locals[i];
		var time = new Date(t.getAttribute("data-time"));
		if (!(time instanceof Date) || isNaN(time))
			continue;

		if (t.classList.contains("time")) {
			t.textContent = time.toLocaleTimeString();
		}
	}
}

function convertValues() {
	var locals = document.getElementsByClassName("localize");

	for (var i = 0; i < locals.length; i++) {
		var v = locals[i];
		if (v.classList.contains("number")) {
			v.textContent = parseFloat(v.textContent).toLocaleString();
		}
	}
}

convertTimezones();
convertValues();
