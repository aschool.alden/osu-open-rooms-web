///@ts-check

var dragula = require("../../node_modules/dragula/dist/dragula");

// maplists_create

/**
 * @typedef {"osu" | "taiko" | "ctb" | "mania"} GameMode
 */

/**
 * @typedef {"yes" | "no" | "any"} TriState
 */

/**
 * @typedef {"yes" | "no" | "any" | "ht"} DT
 */

/**
 * @typedef {{element: HTMLInputElement, getTransformedValue: () => number, setTransformedValue: (v: number) => void}} InverseSlider
 */

/**
 * @typedef {object} AutoPPPick
 * @property {string} dataSource
 * @property {GameMode} [mode]
 * @property {string} [comments]
 * @property {number} [minPP]
 * @property {number} [maxPP]
 * @property {number} [minLength]
 * @property {number} [maxLength]
 * @property {number} [minKeys]
 * @property {number} [maxKeys]
 * @property {number} [minBpm]
 * @property {number} [maxBpm]
 * @property {number} [minDiff]
 * @property {number} [maxDiff]
 * @property {number} [minOverweight]
 * @property {number} [maxOverweight]
 * @property {number} [minPassCount]
 * @property {number} [maxPassCount]
 * @property {number} [minUpdateHours]
 * @property {number} [maxUpdateHours]
 * @property {number} [langs]
 * @property {number} [genres]
 * @property {DT} [dt]
 * @property {TriState} [hd]
 * @property {TriState} [hr]
 * @property {TriState} [fl]
 */

/**
 * @typedef {object} ManualPick
 * @property {string} map
 * @property {string} comment
 * @property {string[]} mods
 * @property {GameMode} mode
 */

/**
 * @typedef {DetailedModInfo | string | string[] | "break"} ModInfo
 */

/**
 * @typedef {object} DetailedModInfo
 * @property {ModInfo} mod
 * @property {GameMode[]} [modes]
 */

/**
 * @typedef {object} NormalizedModInfo
 * @property {string | "break"} mod
 * @property {GameMode[]} modes
 */

/**
 * @typedef {({"type":"autopp", "value":AutoPPPick}|{"type":"manual", "value":ManualPick})} AnyPick
 */

var parsable = [
	"minPP", "maxPP", "minLength", "maxLength", "minKeys", "maxKeys",
	"minBpm", "maxBpm", "minPassCount", "maxPassCount", "minUpdateHours",
	"maxUpdateHours", "langs", "genres", "minDiff", "maxDiff", "minOverweight",
	"maxOverweight"
];
var floats = ["minDiff", "maxDiff", "minOverweight", "maxOverweight"];

/**
 * @type {ModInfo[]}
 */
var allMods = [
	"EZ", "NF", "HT", { mod: "SO", modes: ["osu"] }, "break",
	"HR", "SD", ["DT", "NC"], "HD", { mod: ["FI"], modes: ["mania"] }, "FL", "break",
	{ mod: "RX", modes: ["osu", "taiko", "ctb"] }, "AP", { mod: "break", modes: ["mania"] },
	{ mod: ["K4", "K5", "K6", "K7", "K8", "K9", "K1", "K2", "K3"], modes: ["mania"] }, { mod: "COOP", modes: ["mania"] }, { mod: "RN", modes: ["mania"] }
];
/**
 * @param {ModInfo} mod
 * @param {GameMode[]} [allModes]
 * @returns {NormalizedModInfo[]}
 */
function normalizeMod(mod, allModes) {
	if (!allModes) allModes = ["ctb", "mania", "osu", "taiko"];
	/**
	 * @type {NormalizedModInfo[]}
	 */
	var ret = [];
	if (typeof mod == "string")
		ret.push({
			mod: mod,
			modes: allModes
		});
	else if (Array.isArray(mod)) {
		for (var j = 0; j < mod.length; j++) {
			var modname = mod[j];
			ret.push({
				mod: modname,
				modes: allModes
			});
		}
	}
	else if (typeof mod == "object") {
		if (!mod.modes)
			mod.modes = allModes;
		if (Array.isArray(mod.mod)) {
			for (var j = 0; j < mod.mod.length; j++) {
				ret.push.apply(ret, normalizeMod(mod.mod[j], mod.modes));
			}
		}
		else {
			ret = normalizeMod(mod.mod, mod.modes);
		}
	}
	return ret;
}
/**
 * @param {ModInfo} mod
 * @returns {string[]}
 */
function modNames(mod) {
	/**
	 * @type {string[]}
	 */
	var ret = [];
	if (mod == "break") { }
	else if (typeof mod == "string")
		ret.push(mod);
	else if (Array.isArray(mod)) {
		for (var index = 0; index < mod.length; index++) {
			var element = mod[index];
			ret.push.apply(ret, modNames(element));
		}
	}
	else if (typeof mod == "object") {
		ret.push.apply(ret, modNames(mod.mod));
	}
	else console.error("Unknown mod name for", mod);
	return ret;
}

var recipeParser = {
	/**
	 * @type {AnyPick[]}
	 */
	recipe: [],
	clear: function () {
		this.recipe = [];
	},
	/**
	 * @param {string} line
	 */
	processLine: function (line) {
		line = line.toString().trim();

		if (line.startsWith("#") || line.startsWith("//") || !line.length)
			return;

		if (line.startsWith("ppv1")) {
			// ppv1 "<source>" ("<comments generator>") (filters...)
			// where filters is one of:
			// [minPP/maxPP/minLength/maxLength/minKeys/maxKeys/minBpm/maxBpm/minPassCount/maxPassCount/minUpdateHours/maxUpdateHours/langs/genres]=<int>
			// [minDiff/maxDiff/minOverweight/maxOverweight]=<float>
			// [hd/hr/fl]=yes/no/any
			// dt=yes/no/ht/any

			line = line.substring(4).trim(); // 4 is "ppv1".length

			/**
			 * @type {AutoPPPick}
			 */
			var pp = {};

			var tmp = recipeParser.parseString(line);
			pp.dataSource = tmp[0];
			if (tmp[0].startsWith("osu"))
				pp.mode = "osu";
			else if (tmp[0].startsWith("taiko"))
				pp.mode = "taiko";
			else if (tmp[0].startsWith("ctb"))
				pp.mode = "ctb";
			else if (tmp[0].startsWith("mania"))
				pp.mode = "mania";

			line = tmp[1];

			pp.comments = "";
			if (line.startsWith("'") || line.startsWith("\"")) {
				tmp = recipeParser.parseString(line);
				pp.comments = tmp[0];
				line = tmp[1];
			}

			while (line.length > 0) {
				var space = line.indexOf(" ");
				if (space == -1)
					space = line.length;
				var arg = line.substring(0, space);
				line = line.substring(space).trim();

				var eq = arg.indexOf("=");
				if (eq == -1)
					eq = arg.length;
				var varname = arg.substring(0, eq).toLowerCase();
				var value = eq == arg.length ? "yes" : arg.substring(eq + 1);

				if (varname == "hd") {
					pp.hd = parseTriState(value, "HD");
				} else if (varname == "hr") {
					pp.hr = parseTriState(value, "HR");
				} else if (varname == "fl") {
					pp.fl = parseTriState(value, "FL");
				} else if (varname == "dt") {
					pp.dt = parseDT(value);
				} else {
					setPPVariable(pp, varname, value);
				}
			}

			this.recipe.push({
				type: "autopp",
				value: pp
			});
		}
		else {
			// <beatmapID>(,<mode>)(+<mods>)(:<comment>)
			var end = countUntil(line, function (a) { return a < '0' || a > '9'; });
			if (end == -1)
				end = line.length;

			if (end == 0 || end >= 16)
				return;

			/** @type {string[]} */
			var mods = [];
			var comment = "";
			/** @type {GameMode} */
			var mode = "osu";

			var id = line.substring(0, end);
			line = line.substring(end);

			if (line.startsWith(",")) {
				line = line.substring(1).trim();
				var m = /^(osu|taiko|ctb|mania)/.exec(line);
				if (m) {
					line = line.substring(m[0].length).trim();
					// @ts-ignore
					mode = m[1];
				}
			}

			if (line.startsWith("+")) {
				line = line.substring(1).trim();
				var modsTmp = recipeParser.parseMods(line);
				mods = modsTmp[0];
				line = modsTmp[1];
			}

			var colon = line.indexOf(':');
			if (colon != -1)
				comment = line.substring(colon + 1).trim();

			this.recipe.push({
				type: "manual",
				value: {
					map: id,
					comment: comment,
					mods: mods,
					mode: mode
				}
			});
		}
	},
	/**
	 * @param {string} line
	 * @returns {[string[], string]} A mods[] and the modified line
	 */
	parseMods: function (line) {
		var found;
		var mods = [];
		while (line.length > 0) {
			found = false;
			for (var i = 0; i < allMods.length; i++) {
				var names = modNames(allMods[i]);
				for (var j = 0; j < names.length; j++) {
					var mod = names[j];
					if (line.toUpperCase().startsWith(mod)) {
						mods.push(mod);
						line = line.substring(mod.length).trim();
						found = true;
						if (mods.length >= 16)
							return [mods, line];
						break;
					}
				}
				if (found)
					break;
			}
			if (!found)
				break;
		}
		return [mods, line];
	},
	/**
	 * @param {string} line
	 * @returns {[string, string]} A interpreted string and the modified line
	 */
	parseString: function (line) {
		if (line.startsWith("'") || line.startsWith("\"")) {
			var quote = line[0];
			line = line.substring(1);
			var ret = "";
			var escape = false;
			var i = 0;
			for (; i < line.length; i++) {
				var c = line[i];
				if (escape)
					escape = false;
				else if (c == quote)
					break;
				else if (c == '\\') {
					escape = true;
					continue;
				}
				ret += c;
			}
			line = line.substring(i + 1).trim();
			return [ret, line];
		} else {
			var end = countUntil(line, function (a) {
				return " ,=;:+()".indexOf(a) != -1;
			});
			if (end == -1)
				end = line.length;
			var ret = line.substring(0, end);
			line = line.substring(end).trim();
			return [ret, line];
		}
	}
};

/**
 * @param {string} value
 * @param {string} name for error messages
 * @returns {TriState}
 */
function parseTriState(value, name) {
	value = value.toLowerCase();
	if (value == "yes" || value == "no" || value == "any")
		return value;
	else
		throw new Error("Invalid value " + value + " for variable " + name);
}

/**
 * @param {string} value
 * @returns {DT}
 */
function parseDT(value) {
	value = value.toLowerCase();
	if (value == "yes" || value == "no" || value == "any" || value == "ht")
		return value;
	else
		throw new Error("Invalid value " + value + " for variable DT");
}

/**
 * @param {AutoPPPick} pp
 * @param {string} name
 * @param {string} value
 */
function setPPVariable(pp, name, value) {
	for (var i = 0; i < parsable.length; i++) {
		var var_ = parsable[i];
		if (var_.toLowerCase() == name.toLowerCase()) {
			var isFloat = floats.indexOf(var_) != -1;
			var val = isFloat ? parseFloat(value) : parseInt(value);
			if (!isFinite(val) || (!isFloat && (value.length > 9 || !isStrUnsignedInt(value))))
				throw new Error("Malformed value " + value + " for variable " + name);
			pp[var_] = val;
			return true;
		}
	}
	return false;
}

/**
 * @param {AutoPPPick} pp
 * @returns {string}
 */
function serializePPFilters(pp) {
	var ret = "";

	for (var i = 0; i < parsable.length; i++) {
		var name = parsable[i];
		var val = pp[name];
		var defaultVal = name.startsWith("max")
			? (name == "maxDiff" ? 100 : name == "maxOverweight" ? 1e5 : 2147483647)
			: 0;
		if (val !== undefined && val !== defaultVal) {
			ret += " " + name + "=" + parseFloat(val.toFixed(2));
		}
	}

	if (pp.dt === "yes")
		ret += " dt";
	else if ((pp.dt || "any") !== "any")
		ret += " dt=" + pp.dt;

	if (pp.hd === "yes")
		ret += " hd";
	else if ((pp.hd || "any") !== "any")
		ret += " hd=" + pp.hd;

	if (pp.hr === "yes")
		ret += " hr";
	else if ((pp.hr || "any") !== "any")
		ret += " hr=" + pp.hr;

	if (pp.fl === "yes")
		ret += " fl";
	else if ((pp.fl || "any") !== "any")
		ret += " fl=" + pp.fl;

	return ret;
}

function countUntil(arr, fun) {
	for (var i = 0; i < arr.length; i++)
		if (fun(arr[i]))
			return i;
	return -1;
}

function isStrUnsignedInt(str) {
	for (var i = 0; i < str.length; i++)
		if (str[i] < '0' || str[i] > '9')
			return false;
	return true;
}

/**
 * @param {string} str
 */
function capitalize(str) {
	return str[0].toUpperCase() + str.substring(1);
}

function formatMode(str) {
	if (str == "ctb")
		return "CtB";
	else
		return capitalize(str);
}

/**
 * @param {string} str string to escape
 * @param {boolean} [force=false]
 */
function escapeString(str, force) {
	if (!str)
		str = "";

	if (force || !str || str.startsWith("'") || str.startsWith("\"") || str.indexOf(" ,=;:+()") != -1) {
		if (str.indexOf("'") == -1) {
			return "'" + str.replace(/\\/g, "\\\\") + "'";
		} else {
			return '"' + str.replace(/\\/g, "\\\\").replace(/"/g, "\\\"") + '"';
		}
	} else {
		return str;
	}
}

/**
 * Creates a slider that until the halfway point acts near linear and then rapidly grows until maximum.
 * The halfway value can not be larger than the maximum and will slowly deviate from 0.5 the larger it gets
 * in relation to max.
 * @param {number} min
 * @param {number} max
 * @param {number} halfway
 * @returns {InverseSlider}
 */
function createInverseIncreasingSlider(min, max, halfway) {
	max -= min;
	halfway -= min;
	var factor = max / (max + halfway);
	var transform = function (x) {
		return ((factor * halfway * x) / (1 - factor * x)) + min;
	};
	var untransform = function (x) {
		x -= min;
		return x / (factor * (halfway + x));
	}

	var slider = document.createElement("input");
	slider.setAttribute("type", "range");
	slider.min = "0";
	slider.max = "1";
	slider.step = "any";
	return {
		element: slider,
		getTransformedValue: function () {
			return transform(parseFloat(this.element.value));
		},
		setTransformedValue: function (value) {
			this.element.value = untransform(value).toString();
		}
	}
}

/**
 * @param {InverseSlider} slider
 * @param {string} [prefix]
 * @param {string} [suffix]
 * @returns {InverseSlider & {container: HTMLElement, update: Function}}
 */
function autolabeledSlider(slider, prefix, suffix) {
	var container = document.createElement("label");
	container.setAttribute("class", "slider");
	var label = document.createElement("span");
	container.appendChild(label);
	container.appendChild(slider.element);

	function update() {
		label.textContent = (prefix || "") + parseFloat(slider.getTransformedValue().toFixed(2)) + (suffix || "");
	}

	slider.element.oninput = update;
	slider.element.onchange = update;

	update();

	return {
		container: container,
		update: update,
		element: slider.element,
		getTransformedValue: slider.getTransformedValue,
		setTransformedValue: function (v) {
			slider.setTransformedValue(v);
			update();
		}
	};
}

/**
 * @param {number} min
 * @param {number} max
 * @param {number} step
 * @param {number} def default value
 * @returns {HTMLInputElement}
 */
function createMinMaxNumber(min, max, step, def) {
	var ret = document.createElement("input");
	ret.setAttribute("type", "number");
	ret.setAttribute("class", "lazerinput minmaxinput");
	ret.min = min.toString();
	ret.max = max.toString();
	ret.step = step.toString();
	if (def != 2147483647 && def !== undefined) {
		ret.placeholder = def.toString();
		ret.value = def.toString();
	}
	return ret;
}

/**
 * @param {string} label
 * @param {number} min
 * @param {number} max
 * @param {number} mid
 * @param {object} val
 * @param {string} minl
 * @param {string} maxl
 * @param {Function} change
 * @returns {HTMLElement}
 */
function rangePair(label, min, max, mid, val, minl, maxl, change) {
	var pair = document.createElement("div");
	pair.setAttribute("class", "rangepair");

	var minv = val[minl];
	var maxv = val[maxl];

	var mine = autolabeledSlider(createInverseIncreasingSlider(min, max, mid), "Minimum " + label + ": ");
	mine.setTransformedValue(minv === undefined ? 0 : minv);
	mine.element.onchange = function () {
		val[minl] = mine.getTransformedValue();
		change();
	};
	pair.appendChild(mine.container);

	var maxe = autolabeledSlider(createInverseIncreasingSlider(min, max, mid), "Maximum " + label + ": ");
	maxe.setTransformedValue(maxv === undefined ? 1e5 : maxv);
	maxe.element.onchange = function () {
		val[maxl] = maxe.getTransformedValue();
		change();
	};
	pair.appendChild(maxe.container);

	return pair;
}

/**
 * @param {string} labelText
 * @param {number} min
 * @param {number} max
 * @param {number} step
 * @param {object} val
 * @param {string} minl
 * @param {string} maxl
 * @param {Function} change
 * @returns {HTMLElement}
 */
function rangeTextPair(labelText, min, max, step, val, minl, maxl, change) {
	var pair = document.createElement("div");
	pair.setAttribute("class", "rangepair");

	var minv = val[minl] === undefined ? min : val[minl];
	var maxv = val[maxl] === undefined ? max : val[maxl];

	var mine = createMinMaxNumber(min, max, step, minv);
	var minlbl = label(mine, "Minimum " + labelText + ": ");
	mine.onchange = function () {
		if (mine.checkValidity && !mine.checkValidity())
			return;

		if (!mine.value)
			delete val[minl];
		else
			val[minl] = parseFloat(mine.value);
		change();
	};
	pair.appendChild(minlbl);

	var maxe = createMinMaxNumber(min, max, step, maxv);
	var maxlbl = label(maxe, "Maximum " + labelText + ": ");
	maxe.onchange = function () {
		if (maxe.checkValidity && !maxe.checkValidity())
			return;

		if (!maxe.value)
			delete val[maxl];
		else
			val[maxl] = parseFloat(maxe.value);
		change();
	};
	pair.appendChild(maxlbl);

	return pair;
}

function addBeatmap() {
	recipeParser.recipe.push({
		type: "manual",
		value: {
			map: "",
			comment: "",
			mods: [],
			mode: "osu"
		}
	});

	updateRecipe();
	updateRecipeText();
}
window.addBeatmap = addBeatmap;

function addPPMaps() {
	recipeParser.recipe.push({
		type: "autopp",
		value: {
			dataSource: getDefaultPPMode(),
			comments: "This map gives {{pp}}pp with {{hd}}{{hr}}{{dt}}{{ht}}{{fl}} good accuracy. Overweightedness = {{overweight}}"
		}
	});

	updateRecipe();
	updateRecipeText();
}
window.addPPMaps = addPPMaps;

/**
 * @param {AnyPick} value
 * @param {Function} updateElem
 * @returns {HTMLDivElement}
 */
function generateEditBody(value, updateElem) {
	var ret = document.createElement("div");
	ret.setAttribute("class", "body");

	if (value.type == "autopp") {
		var source = document.createElement("select");
		var regex = /^(ctb|mania|standard|taiko)-(\d{4}-\d{2}-\d{2})_(\d{2})-(\d{2})/;
		var sourceval = value.value.dataSource;
		for (var i = 0; i < ppSources.length; i++) {
			var id = ppSources[i];
			var m = regex.exec(id);
			var name = m ? formatMode(m[1]) + " pp maps from " + m[2] + " " + m[3] + ":" + m[4] + " via /u/grumd" : id;
			var option = document.createElement("option");
			if (id == sourceval) {
				option.setAttribute("selected", "selected");
			}
			option.value = id;
			option.textContent = name;
			source.appendChild(option);
		}
		source.onchange = function () {
			value.value.dataSource = source.value;
			updateElem();
		};
		ret.appendChild(label(source, "Maps source"));

		var comments = document.createElement("input");
		comments.setAttribute("type", "text");
		comments.setAttribute("class", "lazerinput");
		comments.setAttribute("placeholder", "This map gives {{pp}}pp with good accuracy.");
		comments.value = value.value.comments || "";
		comments.onchange = function () {
			value.value.comments = comments.value.trim();
			updateElem();
		};
		ret.appendChild(label(comments, "Comments", "A comments generator text which can contain variables which get replaced for each map. Variables are written inside double braces {{like so}}. "
			+ "Available variables: dt, ht, hd, hr, fl, beatmap, mapset, overweight, pp, artist, title, version, difficulty, length, bpm, passcount, lastupdate, genre, language"));

		ret.appendChild(rangeTextPair("pp", 0, 2147483647, 1, value.value, "minPP", "maxPP", updateElem));
		ret.appendChild(rangeTextPair("length in seconds", 0, 2147483647, 1, value.value, "minLength", "maxLength", updateElem));
		ret.appendChild(rangeTextPair("BPM", 0, 2147483647, 1, value.value, "minBpm", "maxBpm", updateElem));
		ret.appendChild(rangeTextPair("Difficulty", 0, 100, 0.01, value.value, "minDiff", "maxDiff", updateElem));
		ret.appendChild(rangePair("Overweight", 0, 1e5, 100, value.value, "minOverweight", "maxOverweight", updateElem));
		ret.appendChild(rangeTextPair("pass count", 0, 2147483647, 1, value.value, "minPassCount", "maxPassCount", updateElem));
		ret.appendChild(rangeTextPair("hours since last update", 0, 2147483647, 1, value.value, "minUpdateHours", "maxUpdateHours", updateElem));
	} else if (value.type == "manual") {
		var mapID = document.createElement("input");
		mapID.setAttribute("type", "text");
		mapID.setAttribute("class", "lazerinput");
		mapID.setAttribute("required", "required");
		mapID.setAttribute("pattern", "^\\d{1,9}$");
		mapID.setAttribute("placeholder", "983680");
		mapID.value = value.value.map;
		var mapLabel = label(mapID, "Beatmap ID");
		mapID.onchange = function () {
			if (mapID.checkValidity && !mapID.checkValidity()) {
				return;
			}

			value.value.map = mapID.value.trim();
			updateElem();
		};
		ret.appendChild(mapLabel);

		var comment = document.createElement("input");
		comment.setAttribute("type", "text");
		comment.setAttribute("class", "lazerinput");
		comment.setAttribute("placeholder", "This is best played with HR");
		comment.value = value.value.comment;
		comment.onchange = function () {
			value.value.comment = comment.value.trim();
			updateElem();
		};
		ret.appendChild(label(comment, "Comment"));

		var mods = document.createElement("details");
		mods.setAttribute("class", "mods");
		var summary = document.createElement("summary");
		summary.textContent = "Mods";
		mods.appendChild(summary);

		var container = document.createElement("div");
		container.setAttribute("class", "container");
		mods.appendChild(container);

		/**
		 * @type {Function[]}
		 */
		var modeListener = [];

		for (var i = 0; i < allMods.length; i++)
			container.appendChild(modCheckbox(value.value.mods, allMods[i], updateElem, modeListener, value.value.mode));

		ret.appendChild(createGamemodeSelector(value.value.mode, function () {
			// @ts-ignore
			value.value.mode = this.value;
			for (var i = 0; i < modeListener.length; i++)
				modeListener[i](value.value.mode);
			updateElem();
		}));

		ret.appendChild(mods);
	}

	return ret;
}

/**
 * @param {string[]} modsRef
 * @param {ModInfo} modRaw
 * @param {Function} updateElem
 * @param {Function[]} modeListener
 * @param {GameMode} mode
 * @returns {HTMLElement}
 */
function modCheckbox(modsRef, modRaw, updateElem, modeListener, mode) {
	var mods = normalizeMod(modRaw);
	if (!mods)
		return null;
	var element = document.createElement("div");
	element.classList.add("mod-wrapper");
	var btns = [];
	function updateMode(mode) {
		var hidden = mods[0].modes.indexOf(mode) == -1;
		element.style.display = hidden ? "none" : "";
		if (hidden) {
			for (var i = 0; i < btns.length; i++)
				btns[i].toggle(false);
		}
	}
	modeListener.push(updateMode);
	updateMode(mode);
	if (mods[0].mod == "break") {
		element.classList.add("break");
		return element;
	}

	for (var i = 0; i < mods.length; i++) {
		var btn = modToggleButton(modsRef, function (i, steps) {
			if (steps == 1) {
				if (i + 1 >= btns.length) {
					btns[i].style.display = "none";
					btns[0].style.display = "";
				}
				else {
					btns[i].style.display = "none";
					btns[i + 1].toggle(true);
					btns[i + 1].style.display = "";
				}
			} else if (steps == -1) {
				if (i == 0) {
					if (!btns[0].checked) {
						btns[0].style.display = "none";
						btns[btns.length - 1].toggle(true);
						btns[btns.length - 1].style.display = "";
					}
				} else {
					btns[i].style.display = "none";
					btns[i - 1].toggle(true);
					btns[i - 1].style.display = "";
				}
			}
			updateElem();
		}.bind(this, i), mods[i]);
		element.appendChild(btn);
		btns.push(btn);
	}
	if (btns.length > 1) {
		var found = 0;
		for (var i = 0; i < btns.length; i++) {
			var btn = btns[i];
			if (btn.checked) {
				found++;
			} else {
				btn.style.display = "none";
			}
		}
		if (found == 0)
			btns[0].style.display = "";
	}

	return element;
}

/**
 * @param {string[]} modsRef
 * @param {Function} next
 * @param {NormalizedModInfo} mod
 * @returns {HTMLElement & {checked: boolean, toggle: Function}}
 */
function modToggleButton(modsRef, next, mod) {
	/**
	 * @type {HTMLElement & {checked: boolean, toggle: Function}}
	 */
	//@ts-ignore
	var label = document.createElement("label");
	label.setAttribute("class", "mapmod checkbox mod_" + mod.mod.toLowerCase());
	label.checked = false;
	var cb = document.createElement("input");
	cb.setAttribute("type", "checkbox");
	function updateMods() {
		if (cb.checked) {
			if (modsRef.indexOf(mod.mod) == -1) {
				modsRef.push(mod.mod);
			}
		} else {
			var index = modsRef.indexOf(mod.mod);
			if (index != -1) {
				modsRef.splice(index, 1);
			}
		}
	}
	cb.onchange = function () {
		label.checked = cb.checked;
		var step = cb.checked ? 0 : 1;
		updateMods();
		next(step);
	};
	cb.checked = label.checked = modsRef.indexOf(mod.mod) != -1;

	label.oncontextmenu = function (e) {
		e.preventDefault();
		if (cb.checked)
			cb.checked = false;
		updateMods();
		next(-1);
		label.checked = cb.checked;
	};
	label.toggle = function (value) {
		cb.checked = label.checked = value === undefined ? !label.checked : value;
		updateMods();
	};

	label.appendChild(cb);
	var span = document.createElement("span");
	span.textContent = mod.mod;
	label.appendChild(span);
	return label;
}

/**
 * @param {HTMLElement} elem
 * @param {string} label
 * @param {string} [desc]
 * @returns {HTMLElement}
 */
function label(elem, label, desc) {
	var ret = document.createElement("div");
	ret.setAttribute("class", "labeled");
	var labelElem = document.createElement("label");
	var span = document.createElement("span");
	span.textContent = label;
	labelElem.appendChild(span);

	labelElem.appendChild(elem);

	if (desc) {
		var p = document.createElement("p");
		p.setAttribute("class", "description");
		p.textContent = desc;
		labelElem.appendChild(p);
	}
	ret.appendChild(labelElem);
	return ret;
}

/**
 * @param {string} value
 * @param {(this: GlobalEventHandlers, ev: Event) => any} [onChange]
 */
function createGamemodeSelector(value, onChange) {
	var mode = document.createElement("div");
	mode.setAttribute("class", "gamemoderows");
	var modesN = [["osu", "taiko"], ["ctb", "mania"]];
	var modeNames = [["Osu", "Taiko"], ["CtB", "Mania"]];
	var randomName = "__" + Math.random().toString(36);
	for (var j = 0; j < modesN.length; j++) {
		var modes = modesN[j];
		var row = document.createElement("div");
		row.className = "row";
		for (var i = 0; i < modesN.length; i++) {
			var text = document.createElement("label");
			var span = document.createElement("span");
			span.setAttribute("class", "mode-" + modes[i]);
			span.textContent = modeNames[j][i];

			var input = document.createElement("input");
			input.setAttribute("type", "radio");
			input.setAttribute("name", randomName);
			input.setAttribute("value", modes[i]);
			if (modes[i] == value)
				input.setAttribute("checked", "checked");
			input.onchange = onChange;
			text.appendChild(input);
			text.appendChild(span);
			row.appendChild(text);
		}
		mode.appendChild(row);
	}
	return mode;
}

function updateRecipe() {
	for (var i = 0; i < recipeParser.recipe.length; i++) {
		var value = recipeParser.recipe[i];
		var v = JSON.stringify(value);
		while (gui.children[i] && gui.children[i].getAttribute("data-recipe") != v)
			gui.removeChild(gui.children[i]);

		if (gui.children[i]) {
			gui.children[i].setAttribute("data-index", i.toString());

			if (gui.children[i].getAttribute("data-recipe") == v)
				continue;
		}

		var elem = document.createElement("div");
		elem.setAttribute("class", "recipe_gui_recipe " + value.type);
		elem.setAttribute("data-recipe", v);
		elem.setAttribute("data-index", i.toString());

		var wrap = document.createElement("div");
		wrap.setAttribute("class", "wrap");
		elem.appendChild(wrap);

		var header = document.createElement("div");
		header.setAttribute("class", "mapheader");
		wrap.appendChild(header);

		var handle = document.createElement("i");
		handle.setAttribute("class", "handle fas fa-grip-horizontal");
		header.appendChild(handle);

		var title = document.createElement("h3");
		title.textContent = value.type == "autopp" ? "PP Maps" : value.type == "manual" ? "Selected Beatmap" : "???";
		header.appendChild(title);

		var remove = document.createElement("i");
		remove.setAttribute("class", "remove fas fa-times");
		remove.onclick = (function (obj, elem) {
			if (!confirm("Really delete this map part?"))
				return;

			var index = recipeParser.recipe.indexOf(obj);
			recipeParser.recipe.splice(index, 1);

			elem.parentElement.removeChild(elem);
			updateRecipe();
			updateRecipeText();
		}).bind(this, value, elem);
		header.appendChild(remove);

		var body = generateEditBody(value, function (elem, value) {
			elem.setAttribute("data-recipe", JSON.stringify(value));
			updateRecipe();
			updateRecipeText();
		}.bind(this, elem, value));
		wrap.appendChild(body);

		gui.appendChild(elem);
	}

	while (gui.children[recipeParser.recipe.length])
		gui.removeChild(gui.children[recipeParser.recipe.length]);
}

function updateRecipeText() {
	/**
	 * @type {any}
	 */
	var textarea = document.getElementById("recipe");
	var ret = "";
	for (var i = 0; i < recipeParser.recipe.length; i++) {
		var value = recipeParser.recipe[i];
		if (value.type == "autopp") {
			ret += "ppv1 ";
			ret += escapeString(value.value.dataSource);
			if (value.value.comments)
				ret += " " + escapeString(value.value.comments, true);
			ret += serializePPFilters(value.value);
			ret += "\n";
		} else if (value.type == "manual") {
			ret += value.value.map;
			if (value.value.mode && value.value.mode != "osu")
				ret += "," + value.value.mode;

			if (typeof value.value.mods == "object" && value.value.mods.length > 0)
				ret += "+" + value.value.mods.join();

			if (value.value.comment.length)
				ret += ": " + value.value.comment;

			ret += "\n";
		}
	}
	textarea.value = ret;
}

function getDefaultPPMode() {
	for (var i = 0; i < ppSources.length; i++)
		if (ppSources[i].startsWith("standard"))
			return ppSources[i];
}

function parseRecipe(textarea) {
	if (!textarea)
		textarea = document.getElementById("recipe");

	var lines = textarea.value.split(/\r\n|\n|\r/g);
	recipeParser.clear();
	for (var i = 0; i < lines.length; i++)
		recipeParser.processLine(lines[i]);
	updateRecipe();
}

var gui;
var drake;

function startMapLists() {
	gui = document.getElementById("recipe_gui");
	drake = dragula({
		containers: [gui],
		moves: function (el, container, handle) {
			return handle.classList.contains("handle");
		}
	});

	drake.on("drop", function (el, target, source, sibling) {
		var oldIndex = parseInt(el.getAttribute("data-index"));
		var newIndex = sibling ? parseInt(sibling.getAttribute("data-index")) : recipeParser.recipe.length;
		var data = recipeParser.recipe.splice(oldIndex, 1)[0];

		if (newIndex > oldIndex) {
			recipeParser.recipe.splice(newIndex - 1, 0, data);
		} else {
			recipeParser.recipe.splice(newIndex, 0, data);
		}

		updateRecipe();
		updateRecipeText();
	});

	gui.parentElement.style.display = "block";

	parseRecipe();

	window.onbeforeunload = function () {
		backup();
		return "Any unsaved settings will be lost.";
	};
	setInterval(backup, 30000);
	setTimeout(function () {
		if (!document.getElementById("recipe").value) restore();
	}, 50);
}

window.startMapLists = startMapLists;

function backup() { window.localStorage.setItem("osuhost.recipe", document.getElementById("recipe").value); }
function restore() { document.getElementById("recipe").value = window.localStorage.getItem("osuhost.recipe"); parseRecipe(); }
