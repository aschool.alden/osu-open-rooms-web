function reload() {
	var place = document.getElementsByClassName("place")[0];

	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/create/queue/poll");
	xhr.onloadend = function () {
		if (xhr.status != 200) {
			setTimeout(function () {
				window.location.reload();
			}, 30000);
			return;
		}

		if (xhr.responseText == "0")
			window.location.reload();
		else
			place.textContent = xhr.responseText;
	};
	xhr.send();
}

window.reload = reload;
