var confirmButton, loadingIndicator;

function pollReady() {
	var xhr = new XMLHttpRequest();

	xhr.onerror = function () {
		confirmButton.style.display = "";
		loadingIndicator.style.display = "none";
	};

	xhr.onload = function () {
		var data = xhr.responseText;
		if (data == "-1")
			window.location.reload();
		else if (data == "0")
			pollReady();
		else if (data == "1")
			window.location.href = "/register/finish";
	};

	xhr.open("POST", "/register/check");
	xhr.send();
}

if (window.XMLHttpRequest) {
	var confirmField = document.getElementById("confirm");
	confirmButton = confirmField.firstElementChild;
	confirmButton.style.display = "none";

	loadingIndicator = document.createElement("div");
	loadingIndicator.setAttribute("class", "loading-indicator");

	var spinner = document.createElement("i");
	spinner.setAttribute("class", "fas fa-spinner fa-spin");
	loadingIndicator.appendChild(spinner);

	loadingIndicator.appendChild(document.createTextNode("Bot is waiting for the DM containing the code..."));
	confirmField.appendChild(loadingIndicator);
	pollReady();
}
