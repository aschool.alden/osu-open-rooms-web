///@ts-check

/**
 * @param {HTMLElement} container
 */
function setupMapSelect(container) {
	container.classList.add("js");

	var mapscontent = container.getElementsByClassName("mapscontent");
	if (mapscontent.length) {
		var button = mapscontent[0].nextElementSibling;
		if (button) {
			// @ts-ignore
			button.style.display = "";
		}
	}
}

/**
 * @param {PointerEvent} event
 * @param {HTMLElement} maplist
 * @param {string} setting
 */
function selectMap(event, maplist, setting) {
	var id = maplist.getAttribute("data-id");
	var mapselect = document.getElementById(setting + "_area");
	if (id && mapselect) {
		var elem = event.toElement || event.target;
		var input = mapselect.getElementsByClassName("identry")[0];
		// @ts-ignore
		if (input && (!elem || !elem.classList || elem.classList.contains("id"))) {
			// @ts-ignore
			input.value = id;
		}
	}
}

/**
 * @param {HTMLElement} button
 * @param {string} setting
 */
function showMoreMapscontent(button, setting) {
	var mapselect = document.getElementById(setting + "_area");
	if (mapselect) {
		button.previousElementSibling.classList.add("scroll");

		if (button)
			button.parentElement.removeChild(button);
	}
}
window.showMoreMapscontent = showMoreMapscontent;

(function () {
	var lists = document.getElementsByClassName("mapselect");
	for (var i = 0; i < lists.length; i++) {
		// @ts-ignore
		setupMapSelect(lists[i]);
	}
})();
