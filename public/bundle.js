// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"elements/CurveSlider.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SliderKnob = exports.CurveSlider = void 0;

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (b.hasOwnProperty(p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var CurveSlider =
/** @class */
function (_super) {
  __extends(CurveSlider, _super);

  function CurveSlider() {
    var _this = // Always call super first in constructor
    _super.call(this) || this;

    var shadow = _this.attachShadow({
      mode: "open"
    });

    _this.css = document.createElement("style");
    _this.css.textContent = "\n\t\t\t:host {\n\t\t\t\tposition: relative;\n\t\t\t\tdisplay: inline-block;\n\t\t\t\tmax-width: 100%;\n\t\t\t\twidth: 300px;\n\t\t\t\theight: 40px;\n\t\t\t\tpadding: 10px;\n\t\t\t\tbox-sizing: border-box;\n\t\t\t}\n\n\t\t\t:focus-within .bar {\n\t\t\t\tbox-shadow: 0 0 2px #ffcc22;\n\t\t\t}\n\n\t\t\tslider-knob:focus {\n\t\t\t\t--knob-box-shadow: 0 0 4px 1px #ffcc22;\n\t\t\t\t--value-opacity: 1;\n\t\t\t\tz-index: 4;\n\t\t\t}\n\n\t\t\t.container {\n\t\t\t\tposition: relative;\n\t\t\t\twidth: 100%;\n\t\t\t\theight: 100%;\n\t\t\t}\n\n\t\t\t.bar, .tick, slider-knob {\n\t\t\t\tposition: absolute;\n\t\t\t\ttop: 0;\n\t\t\t\tbottom: 0;\n\t\t\t\tmargin: auto;\n\t\t\t}\n\n\t\t\tslider-knob {\n\t\t\t\tz-index: 3;\n\t\t\t\tleft: calc(var(--normalized-value) * 100%);\n\t\t\t}\n\n\t\t\t.bar {\n\t\t\t\tborder-radius: var(--bar-border-radius, 0);\n\t\t\t\tborder: var(--bar-border, none);\n\t\t\t\tbackground: var(--bar-color, var(--slider-color, white));\n\t\t\t\theight: var(--bar-height, 1px);\n\t\t\t\tleft: 0;\n\t\t\t\tright: 0;\n\t\t\t\tz-index: 1;\n\t\t\t}\n\n\t\t\t.tick {\n\t\t\t\tpointer-events: none;\n\t\t\t\tbackground: var(--tick-color, var(--slider-color, white));\n\t\t\t\topacity: var(--tick-opacity, 1);\n\t\t\t\twidth: var(--tick-thickness, 1px);\n\t\t\t\theight: var(--tick-size, 5px);\n\t\t\t\tz-index: 2;\n\t\t\t\tleft: calc(var(--tick-value) * 100%);\n\t\t\t}\n\t\t"; // attach the created elements to the shadow dom

    shadow.appendChild(_this.css);

    _this.render(shadow);

    return _this;
  }

  CurveSlider.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
    switch (name) {
      case "value":
      case "step":
        this.knob.valueNum = parseFloat(newValue);
        this.knob.value = this.formatValue(parseFloat(newValue));
        this.recompute();
        break;

      case "min":
      case "max":
      case "slope":
        this.recompute();

      /* falls through */

      case "tick-count":
        this.regenerateTicks();
        break;

      case "max-infinity":
      case "min-infinity":
        this.knob.value = this.formatValue(this.knob.valueNum);
        break;

      default:
        console.warn("unknown property change", name, oldValue, newValue);
        break;
    }
  };

  CurveSlider.prototype.render = function (shadow) {
    var _this = this;

    this.container = document.createElement("div");
    this.container.addEventListener("pointerdown", function (e) {
      e.preventDefault();

      _this.handleBarDrag(_this.container, e);
    });
    this.container.className = "container";
    this.container.appendChild(this.bar = this.renderBar());
    this.container.appendChild(this.knob = this.renderKnob());
    this.regenerateTicks();
    shadow.appendChild(this.container);
  };

  CurveSlider.prototype.renderBar = function () {
    var bar = document.createElement("div");
    bar.className = "bar";
    return bar;
  };

  CurveSlider.prototype.regenerateTicks = function () {
    var _this = this;

    if (this.ticks) this.ticks.forEach(function (tick) {
      _this.container.removeChild(tick);
    });
    this.ticks = this.renderTicks();
    this.ticks.forEach(function (tick) {
      _this.container.appendChild(tick);
    });
    this.updateTicks();
  };

  CurveSlider.prototype.updateTicks = function () {
    var _this = this;

    this.ticks.forEach(function (tick) {
      if (_this.inSlider(parseFloat(tick.getAttribute("data-value")))) {
        if (!tick.classList.contains("between")) tick.classList.add("between");
      } else if (tick.classList.contains("between")) tick.classList.remove("between");
    });
  };
  /**
   * checks if a given normalized value is in the user selected value range of this slider.
   * @param v a normalized value in range 0-1
   */


  CurveSlider.prototype.inSlider = function (v) {
    return v < parseFloat(this.style.getPropertyValue("--normalized-value"));
  };

  CurveSlider.prototype.renderTicks = function () {
    var ret = [];
    var min = this.min;
    var max = this.max;
    var count = this.tickCount;

    for (var i = 0; i < count; i++) {
      var t = min + (max - min) / count * i;
      var tick = document.createElement("div");
      tick.className = "tick";
      var v = this.untransform(this.snapClamp(t));
      tick.setAttribute("data-value", v.toString());
      tick.style.setProperty("--tick-value", v.toString());
      ret.push(tick);
    }

    return ret;
  };

  CurveSlider.prototype.renderKnob = function (value, index) {
    var _this = this;

    if (value === void 0) {
      value = this.value;
    }

    if (index === void 0) {
      index = 1;
    }

    var knob = document.createElement("slider-knob");
    var ev = this.handleKnobDrag.bind(this, knob);
    knob.addEventListener("pointerdown", function (e) {
      e.preventDefault();
      knob.setPointerCapture(e.pointerId);
      knob.addEventListener("pointermove", ev);
    });

    var cancel = function cancel(e) {
      e.preventDefault();
      knob.releasePointerCapture(e.pointerId);
      knob.removeEventListener("pointermove", ev);

      _this.finishKnobDrag(knob, knob.valueNum);
    };

    knob.addEventListener("pointercancel", cancel);
    knob.addEventListener("pointerup", cancel);
    knob.addEventListener("keydown", function (e) {
      if (e.altKey || e.ctrlKey && e.shiftKey) return;

      if (e.key == "ArrowLeft") {
        e.preventDefault();

        _this.nudgeKnob(knob, (e.ctrlKey ? 10 : 1) * (e.shiftKey ? 0.1 : 1) * -1);
      } else if (e.key == "ArrowRight") {
        e.preventDefault();

        _this.nudgeKnob(knob, (e.ctrlKey ? 10 : 1) * (e.shiftKey ? 0.1 : 1) * 1);
      }
    }); // for some reason you can't directly assign value here (it breaks future assignments, only in the single slider)

    knob.setAttribute("value", this.formatValue(value));
    knob.setAttribute("tabindex", index.toString());
    return knob;
  };

  CurveSlider.prototype.handleKnobDrag = function (knob, e) {
    var rect = this.getBoundingClientRect();
    var min = this.min;
    var max = this.max;
    var v = (e.clientX - rect.left) / rect.width;
    if (v < 0) v = 0;
    if (v > 1) v = 1;
    var vt = this.snapClamp(this.transform(v));
    v = this.untransform(vt);
    knob.valueNum = vt;
    knob.value = this.formatValue(vt);
    this.style.setProperty("--input-value", vt.toString());
    this.style.setProperty("--normalized-value", v.toString());
    this.updateTicks();
  };

  CurveSlider.prototype.handleBarDrag = function (bar, e) {
    this.startKnobDrag(this.knob, e.pointerId);
  };

  CurveSlider.prototype.startKnobDrag = function (knob, pointer) {
    var _this = this;

    var ev = this.handleKnobDrag.bind(this, knob);

    var cancel = function cancel(e) {
      e.preventDefault();
      knob.releasePointerCapture(e.pointerId);
      knob.removeEventListener("pointermove", ev);

      _this.finishKnobDrag(knob, knob.valueNum);
    };

    knob.addEventListener("pointercancel", cancel);
    knob.addEventListener("pointerup", cancel);
    knob.setPointerCapture(pointer);
    knob.addEventListener("pointermove", ev);
    knob.focus();
  };

  CurveSlider.prototype.finishKnobDrag = function (knob, value) {
    if (value === undefined || value === null) return;
    this.value = value;
  };

  CurveSlider.prototype.calculateNudge = function (start, value) {
    var step = this.step;

    if (step > 0) {
      var a = Math.abs(value);
      if (a < 1) return Math.sign(value) * step;else return Math.round(value / step) * step;
    } else {
      var offset = value / 100;
      var dt = this.max - this.min;
      if (dt > 1000000) return offset * 1000000;else if (dt > 10000) return offset * 10000;else if (dt > 100) return offset * 100;

      if (this.hasNegative) {
        return this.slope * 2 * offset;
      } else {
        return (this.slope - this.min) * 2 * offset;
      }
    }
  };

  CurveSlider.prototype.nudgeKnob = function (knob, value) {
    this.value += this.calculateNudge(this.value, value);
  };

  Object.defineProperty(CurveSlider, "observedAttributes", {
    get: function get() {
      return ["min", "max", "slope", "step", "value", "tick-count", "max-infinity", "min-infinity"];
    },
    enumerable: true,
    configurable: true
  });

  CurveSlider.prototype.recompute = function () {
    var snapped = this.snapClamp(this.value);
    if (this.value != snapped) this.value = snapped;
    this.style.setProperty("--input-value", this.value.toString());
    this.style.setProperty("--normalized-value", this.untransform(this.value).toString());
    this.updateTicks();
  };
  /**
   * Converts a linear value into the slider value space.
   * @param x a linear value from 0-1
   * @returns a slider space value from min-max
   */


  CurveSlider.prototype.transform = function (x) {
    if (this.hasNegative) {
      if (x < 0.5) {
        x = 1 - x * 2;
        var factor = this.negFactor;
        return -(factor * this.slope * x / (1 - factor * x));
      } else {
        x = (x - 0.5) * 2;
        var factor = this.posFactor;
        return factor * this.slope * x / (1 - factor * x);
      }
    } else {
      var min = this.min;
      var factor = this.factor;
      return factor * (this.slope - min) * x / (1 - factor * x) + min;
    }
  };
  /**
   * Converts a slider space value into a linear value.
   * @param x a slider space value from min-max
   * @returns a linear value from 0-1
   */


  CurveSlider.prototype.untransform = function (x) {
    if (this.hasNegative) {
      if (x < 0) {
        x = -x;
        return 0.5 - x / (this.negFactor * (this.slope + x)) * 0.5;
      } else {
        return x / (this.posFactor * (this.slope + x)) * 0.5 + 0.5;
      }
    } else {
      var min = this.min;
      x -= min;
      return x / (this.factor * (this.slope - min + x));
    }
  };

  CurveSlider.prototype.formatValue = function (v) {
    if (v + 0.000001 >= this.max && this.maxInfinity) return "∞";else if (v - 0.000001 <= this.min && this.minInfinity) return "-∞";
    var a = Math.abs(v);
    if (a > 100000) return Math.round(v).toString();else if (a > 1000) return (Math.round(v * 10) / 10).toString();else if (a > 100) return (Math.round(v * 100) / 100).toString();else if (a > 1) return (Math.round(v * 1000) / 1000).toString();else return (Math.round(v * 10000) / 10000).toString();
  };

  CurveSlider.prototype.snapClamp = function (v) {
    var step = this.step;
    if (step > 0 && Math.round(v % step * 10000) / 10000 != 0) v = Math.round(v / step) * step;
    if (v > this.max) v = this.max;else if (v < this.min) v = this.min;
    return v;
  };

  Object.defineProperty(CurveSlider.prototype, "factor", {
    get: function get() {
      return (this.max - this.min) / (this.max + this.slope - 2 * this.min);
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "negFactor", {
    get: function get() {
      return -this.min / (-this.min + this.slope);
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "posFactor", {
    get: function get() {
      return this.max / (this.max + this.slope);
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "hasNegative", {
    get: function get() {
      return this.min < 0;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "min", {
    get: function get() {
      return parseFloat(this.getAttribute("min") || "0");
    },
    set: function set(min) {
      this.setAttribute("min", min.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "max", {
    get: function get() {
      return parseFloat(this.getAttribute("max") || "1");
    },
    set: function set(max) {
      this.setAttribute("max", max.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "slope", {
    get: function get() {
      return parseFloat(this.getAttribute("slope") || "0.5");
    },
    set: function set(slope) {
      this.setAttribute("slope", slope.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "step", {
    get: function get() {
      return parseFloat(this.getAttribute("step") || "0");
    },
    set: function set(step) {
      this.setAttribute("step", step.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "value", {
    get: function get() {
      return parseFloat(this.getAttribute("value") || "0");
    },
    set: function set(value) {
      this.setAttribute("value", value.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "tickCount", {
    get: function get() {
      return parseInt(this.getAttribute("tick-count") || "12");
    },
    set: function set(tickCount) {
      this.setAttribute("tick-count", tickCount.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "minInfinity", {
    get: function get() {
      return this.getAttribute("min-infinity") !== null;
    },
    set: function set(minInfinity) {
      if (minInfinity) this.setAttribute("min-infinity", "min-infinity");else this.removeAttribute("min-infinity");
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(CurveSlider.prototype, "maxInfinity", {
    get: function get() {
      return this.getAttribute("max-infinity") !== null;
    },
    set: function set(maxInfinity) {
      if (maxInfinity) this.setAttribute("max-infinity", "max-infinity");else this.removeAttribute("max-infinity");
    },
    enumerable: true,
    configurable: true
  });
  return CurveSlider;
}(HTMLElement);

exports.CurveSlider = CurveSlider;
window.customElements.define("curve-slider", CurveSlider);

var SliderKnob =
/** @class */
function (_super) {
  __extends(SliderKnob, _super);

  function SliderKnob() {
    var _this = // Always call super first in constructor
    _super.call(this) || this;

    var shadow = _this.attachShadow({
      mode: "open"
    });

    _this.css = document.createElement("style");
    _this.css.textContent = "\n\t\t\t:host {\n\t\t\t\twidth: 1px;\n\t\t\t\theight: 1px;\n\t\t\t\toutline: none;\n\t\t\t}\n\t\t\t.knob {\n\t\t\t\tdisplay: inline-block;\n\t\t\t\tposition: relative;\n\t\t\t\twidth: var(--knob-width, var(--diameter, 16px));\n\t\t\t\theight: var(--knob-height, var(--diameter, 16px));\n\t\t\t\tborder-top-left-radius: var(--roundness-top-left, var(--roundness, 100%));\n\t\t\t\tborder-bottom-left-radius: var(--roundness-bottom-left, var(--roundness, 100%));\n\t\t\t\tborder-top-right-radius: var(--roundness-top-right, var(--roundness, 100%));\n\t\t\t\tborder-bottom-right-radius: var(--roundness-bottom-right, var(--roundness, 100%));\n\t\t\t\ttransform: var(--knob-offset, translate(-50%, -50%));\n\t\t\t\tbackground: var(--knob-color, var(--slider-color, white));\n\t\t\t\tborder: var(--knob-border, none);\n\t\t\t\tbox-shadow: var(--knob-box-shadow, none);\n\t\t\t}\n\t\t\t.knob:focus {\n\t\t\t\tbackground-color: red;\n\t\t\t}\n\t\t\t.value {\n\t\t\t\topacity: var(--value-opacity, 0.5);\n\t\t\t\tpointer-events: none;\n\t\t\t\ttext-align: var(--value-align, center);\n\t\t\t\tposition: relative;\n\t\t\t\ttop: var(--value-offset, 100%);\n\t\t\t\tfont: inherit;\n\t\t\t\tcolor: inherit;\n\t\t\t\tmargin: var(--value-margin, 0 -100px);\n\t\t\t\t-webkit-user-select: none;\n\t\t\t\t-moz-user-select: none;\n\t\t\t\t-ms-user-select: none;\n\t\t\t\tuser-select: none;\n\t\t\t}\n\t\t"; // attach the created elements to the shadow dom

    shadow.appendChild(_this.css);
    shadow.appendChild(_this.knob = _this.renderKnob());
    return _this;
  }

  SliderKnob.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
    switch (name) {
      case "value":
        this.valueElem.textContent = newValue;
        break;

      case "min":
      case "max":
      case "slope":
        break;

      default:
        console.warn("unknown property change", name, oldValue, newValue);
        break;
    }
  };

  SliderKnob.prototype.renderKnob = function () {
    var knob = document.createElement("div");
    knob.className = "knob";
    knob.appendChild(this.valueElem = this.renderValue());
    return knob;
  };

  SliderKnob.prototype.renderValue = function () {
    var value = document.createElement("div");
    value.className = "value";
    value.textContent = this.value;
    return value;
  };

  Object.defineProperty(SliderKnob, "observedAttributes", {
    get: function get() {
      return ["axis", "value"];
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(SliderKnob.prototype, "axis", {
    get: function get() {
      var attr = this.getAttribute("axis");
      if (attr == "x" || attr == "y" || attr == "both") return attr;else return "x";
    },
    set: function set(value) {
      if (value == "x" || value == "y" || value == "both") this.setAttribute("axis", value);
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(SliderKnob.prototype, "valueNum", {
    get: function get() {
      return this._valueNum;
    },
    set: function set(value) {
      this._valueNum = value;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(SliderKnob.prototype, "value", {
    get: function get() {
      return this.getAttribute("value") || "0";
    },
    set: function set(value) {
      this.setAttribute("value", value);
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(SliderKnob.prototype, "moveX", {
    get: function get() {
      return this.axis == "x" || this.axis == "both";
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(SliderKnob.prototype, "moveY", {
    get: function get() {
      return this.axis == "y" || this.axis == "both";
    },
    enumerable: true,
    configurable: true
  });
  return SliderKnob;
}(HTMLElement);

exports.SliderKnob = SliderKnob;
window.customElements.define("slider-knob", SliderKnob);
},{}],"elements/RangeCurveSlider.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RangeCurveSlider = void 0;

var _CurveSlider = require("./CurveSlider");

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (b.hasOwnProperty(p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var RangeCurveSlider =
/** @class */
function (_super) {
  __extends(RangeCurveSlider, _super);

  function RangeCurveSlider() {
    var _this = // Always call super first in constructor
    _super.call(this) || this;

    _this.css.textContent += "\n\t\t\tslider-knob.min {\n\t\t\t\t--knob-width: 12px;\n\t\t\t\t--knob-height: 16px;\n\t\t\t\t--roundness-top-left: 16px;\n\t\t\t\t--roundness-bottom-left: 16px;\n\t\t\t\t--roundness-top-right: 4px;\n\t\t\t\t--roundness-bottom-right: 4px;\n\t\t\t\t--knob-offset: translate(-100%, -50%);\n\t\t\t\t--value-align: right;\n\t\t\t\t--value-margin: 0 2px 0 -100px;\n\n\t\t\t\tleft: calc(var(--min-normalized-value) * 100%);\n\t\t\t}\n\n\t\t\tslider-knob.max {\n\t\t\t\t--knob-width: 12px;\n\t\t\t\t--knob-height: 16px;\n\t\t\t\t--roundness-top-left: 4px;\n\t\t\t\t--roundness-bottom-left: 4px;\n\t\t\t\t--roundness-top-right: 16px;\n\t\t\t\t--roundness-bottom-right: 16px;\n\t\t\t\t--knob-offset: translate(0, -50%);\n\t\t\t\t--value-align: left;\n\t\t\t\t--value-margin: 0 -100px 0 2px;\n\n\t\t\t\tleft: calc(var(--max-normalized-value) * 100%);\n\t\t\t}\n\t\t";
    return _this;
  }

  RangeCurveSlider.prototype.render = function (shadow) {
    var _this = this;

    this.container = document.createElement("div");
    this.container.addEventListener("pointerdown", function (e) {
      e.preventDefault();

      _this.handleBarDrag(_this.container, e);
    });
    this.container.className = "container";
    this.container.appendChild(this.bar = this.renderBar());
    this.knobs = this.renderKnobs();
    this.container.appendChild(this.knobs[0]);
    this.container.appendChild(this.knobs[1]);
    this.regenerateTicks();
    shadow.appendChild(this.container);
  };

  RangeCurveSlider.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
    switch (name) {
      case "min-value":
        this.knobs[0].valueNum = parseFloat(newValue);
        this.knobs[0].value = this.formatValue(parseFloat(newValue));
        this.recompute();
        break;

      case "max-value":
        this.knobs[1].valueNum = parseFloat(newValue);
        this.knobs[1].value = this.formatValue(parseFloat(newValue));
        this.recompute();
        break;

      case "min":
      case "max":
      case "slope":
        this.recompute();

      /* falls through */

      case "tick-count":
        this.regenerateTicks();
        break;

      case "max-infinity":
      case "min-infinity":
        this.knobs[0].value = this.formatValue(this.knobs[0].valueNum);
        this.knobs[1].value = this.formatValue(this.knobs[1].valueNum);
        break;

      default:
        console.warn("unknown property change", name, oldValue, newValue);
        break;
    }
  };

  RangeCurveSlider.prototype.renderKnobs = function () {
    var ret = [this.renderKnob(this.minValue, 1), this.renderKnob(this.maxValue, 2)];
    ret[0].classList.add("min");
    ret[1].classList.add("max");
    return ret;
  };

  RangeCurveSlider.prototype.handleKnobDrag = function (knob, e) {
    var rect = this.getBoundingClientRect();
    var min = this.min;
    var max = this.max;
    var v = (e.clientX - rect.left) / rect.width;
    if (v < 0) v = 0;
    if (v > 1) v = 1;
    var t = "";

    if (knob.classList.contains("min")) {
      var localMax = this.untransform(this.maxValue);
      if (v > localMax) v = localMax;
      t = "min";
    } else if (knob.classList.contains("max")) {
      var localMin = this.untransform(this.minValue);
      if (v < localMin) v = localMin;
      t = "max";
    } else console.error("don't know what i'm dragging!!");

    var vt = this.snapClamp(this.transform(v));
    v = this.untransform(vt);
    knob.valueNum = vt;
    knob.value = this.formatValue(vt);
    this.style.setProperty("--" + t + "-input-value", vt.toString());
    this.style.setProperty("--" + t + "-normalized-value", v.toString());
    this.updateTicks();
  };

  RangeCurveSlider.prototype.handleBarDrag = function (bar, e) {
    var rect = this.getBoundingClientRect();
    var v = (e.clientX - rect.left) / rect.width;
    var mid = (this.untransform(this.minValue) + this.untransform(this.maxValue)) * 0.5;
    var knob = v < mid ? this.knobs[0] : this.knobs[1];
    this.startKnobDrag(knob, e.pointerId);
  };

  RangeCurveSlider.prototype.finishKnobDrag = function (knob, value) {
    if (value === undefined || value === null) return;

    if (knob.classList.contains("min")) {
      this.minValue = value;
    } else if (knob.classList.contains("max")) {
      this.maxValue = value;
    } else console.error("don't know what i'm dragging!!");
  };

  RangeCurveSlider.prototype.nudgeKnob = function (knob, value) {
    if (knob.classList.contains("min")) {
      var t = this.minValue + this.calculateNudge(this.minValue, value);
      if (t < this.maxValue) this.minValue = t;else this.minValue = this.maxValue;
    } else if (knob.classList.contains("max")) {
      var t = this.maxValue + this.calculateNudge(this.maxValue, value);
      if (t > this.minValue) this.maxValue = t;else this.maxValue = this.minValue;
    } else console.error("don't know what kind of knob this is!!");
  };

  Object.defineProperty(RangeCurveSlider, "observedAttributes", {
    get: function get() {
      return ["min", "max", "slope", "min-value", "max-value", "tick-count", "max-infinity", "min-infinity"];
    },
    enumerable: true,
    configurable: true
  });

  RangeCurveSlider.prototype.recompute = function () {
    if (this.minValue > this.max) this.minValue = this.max;else if (this.minValue < this.min) this.minValue = this.min;
    if (this.maxValue > this.max) this.maxValue = this.max;else if (this.maxValue < this.min) this.maxValue = this.min;
    if (this.minValue > this.maxValue) this.minValue = this.maxValue;
    this.style.setProperty("--min-input-value", this.minValue.toString());
    this.style.setProperty("--min-normalized-value", this.untransform(this.minValue).toString());
    this.style.setProperty("--max-input-value", this.maxValue.toString());
    this.style.setProperty("--max-normalized-value", this.untransform(this.maxValue).toString());
    this.updateTicks();
  };
  /**
   * checks if a given normalized value is in the user selected value range of this slider.
   * @param v a normalized value in range 0-1
   */


  RangeCurveSlider.prototype.inSlider = function (v) {
    return v >= parseFloat(this.style.getPropertyValue("--min-normalized-value")) && v <= parseFloat(this.style.getPropertyValue("--max-normalized-value"));
  };

  RangeCurveSlider.prototype.formatValue = function (v) {
    if (v + 0.000001 >= this.max && this.maxInfinity) return "∞";else if (v - 0.000001 <= this.min && this.minInfinity) return "-∞";
    var a = Math.abs(v);
    if (a > 1000) return Math.round(v).toString();else if (a > 10) return (Math.round(v * 10) / 10).toString();else return (Math.round(v * 100) / 100).toString();
  };

  Object.defineProperty(RangeCurveSlider.prototype, "minValue", {
    get: function get() {
      return parseFloat(this.getAttribute("min-value") || "0");
    },
    set: function set(minValue) {
      this.setAttribute("min-value", minValue.toString());
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(RangeCurveSlider.prototype, "maxValue", {
    get: function get() {
      return parseFloat(this.getAttribute("max-value") || "1");
    },
    set: function set(maxValue) {
      this.setAttribute("max-value", maxValue.toString());
    },
    enumerable: true,
    configurable: true
  });
  return RangeCurveSlider;
}(_CurveSlider.CurveSlider);

exports.RangeCurveSlider = RangeCurveSlider;
window.customElements.define('range-curve-slider', RangeCurveSlider);
},{"./CurveSlider":"elements/CurveSlider.js"}],"elements/OsuCurveSlider.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OsuRangeCurveSlider = exports.OsuCurveSlider = void 0;

var _CurveSlider = require("./CurveSlider");

var _RangeCurveSlider = require("./RangeCurveSlider");

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (b.hasOwnProperty(p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var OsuCurveSlider =
/** @class */
function (_super) {
  __extends(OsuCurveSlider, _super);

  function OsuCurveSlider() {
    var _this = // Always call super first in constructor
    _super.call(this) || this;

    _this.css.textContent += "\n\t\t\t:host {\n\t\t\t\t--bar-height: 3px;\n\t\t\t\t--slider-color: #ff66aa;\n\t\t\t\t--knob-width: 30px;\n\t\t\t\t--gradstart: calc(var(--normalized-value) * 100% - var(--knob-width) / 2);\n\t\t\t\t--gradend: calc(var(--normalized-value) * 100% + var(--knob-width) / 2);\n\t\t\t\t--bar-color: linear-gradient(90deg,\n\t\t\t\t\t#ff66aa var(--gradstart),\n\t\t\t\t\ttransparent var(--gradstart),\n\t\t\t\t\ttransparent var(--gradend),\n\t\t\t\t\trgba(255, 102, 170, 0.6) var(--gradend));\n\t\t\t\t--tick-color: #3e3a44;\n\t\t\t\t--tick-opacity: 1;\n\t\t\t\t--tick-size: 3px;\n\t\t\t}\n\n\t\t\t.tick.between {\n\t\t\t\t--tick-opacity: 0;\n\t\t\t}\n\n\t\t\t:focus-within .bar {\n\t\t\t\tbox-shadow: none;\n\t\t\t}\n\n\t\t\tslider-knob {\n\t\t\t\t--knob-height: 9px;\n\t\t\t\t--roundness: 9px;\n\t\t\t\t--knob-border-color: #ff66aa;\n\t\t\t\t--knob-border: 3px solid var(--knob-border-color);\n\t\t\t\t--knob-color: transparent;\n\t\t\t}\n\n\t\t\t:hover slider-knob {\n\t\t\t\t--knob-color: #bf187f;\n\t\t\t}\n\n\t\t\tslider-knob:focus {\n\t\t\t\t--knob-box-shadow: 0 0 5px 3px #bf187f;\n\t\t\t\t--knob-color: var(--knob-border-color);\n\t\t\t}\n\n\t\t\t:hover slider-knob {\n\t\t\t\t--knob-box-shadow: 0 0 5px 3px #bf187f;\n\t\t\t\t--knob-border-color: #ffddee;\n\t\t\t}\n\t\t";
    return _this;
  }

  return OsuCurveSlider;
}(_CurveSlider.CurveSlider);

exports.OsuCurveSlider = OsuCurveSlider;
window.customElements.define("osu-curve-slider", OsuCurveSlider);

var OsuRangeCurveSlider =
/** @class */
function (_super) {
  __extends(OsuRangeCurveSlider, _super);

  function OsuRangeCurveSlider() {
    var _this = // Always call super first in constructor
    _super.call(this) || this;

    _this.css.textContent += "\n\t\t\t:host {\n\t\t\t\t--bar-height: 3px;\n\t\t\t\t--slider-color: #ff66aa;\n\t\t\t\t--gradminstart: calc(var(--min-normalized-value) * 100% - 20px);\n\t\t\t\t--gradminend: calc(var(--min-normalized-value) * 100%);\n\t\t\t\t--gradmaxstart: calc(var(--max-normalized-value) * 100%);\n\t\t\t\t--gradmaxend: calc(var(--max-normalized-value) * 100% + 20px);\n\t\t\t\t--bar-color: linear-gradient(90deg,\n\t\t\t\t\trgba(255, 102, 170, 0.6) var(--gradminstart),\n\t\t\t\t\ttransparent var(--gradminstart),\n\t\t\t\t\ttransparent var(--gradminend),\n\t\t\t\t\t#ff66aa var(--gradminend),\n\t\t\t\t\t#ff66aa var(--gradmaxstart),\n\t\t\t\t\ttransparent var(--gradmaxstart),\n\t\t\t\t\ttransparent var(--gradmaxend),\n\t\t\t\t\trgba(255, 102, 170, 0.6) var(--gradmaxend))\n\t\t\t\t;\n\t\t\t\t--tick-color: #3e3a44;\n\t\t\t\t--tick-opacity: 1;\n\t\t\t\t--tick-size: 3px;\n\t\t\t}\n\n\t\t\t.tick.between {\n\t\t\t\t--tick-opacity: 0;\n\t\t\t}\n\n\t\t\t:focus-within .bar {\n\t\t\t\tbox-shadow: none;\n\t\t\t}\n\n\t\t\tslider-knob.min, slider-knob.max {\n\t\t\t\t--knob-width: 15px;\n\t\t\t\t--knob-height: 9px;\n\t\t\t\t--roundness: 9px;\n\t\t\t\t--knob-border-color: #ff66aa;\n\t\t\t\t--knob-border: 3px solid var(--knob-border-color);\n\t\t\t\t--knob-color: transparent;\n\t\t\t}\n\n\t\t\t:hover slider-knob {\n\t\t\t\t--knob-color: #bf187f;\n\t\t\t}\n\n\t\t\tslider-knob:focus {\n\t\t\t\t--knob-box-shadow: 0 0 5px 3px #bf187f;\n\t\t\t\t--knob-color: var(--knob-border-color);\n\t\t\t}\n\n\t\t\t:hover slider-knob {\n\t\t\t\t--knob-box-shadow: 0 0 5px 3px #bf187f;\n\t\t\t\t--knob-border-color: #ffddee;\n\t\t\t}\n\t\t";
    return _this;
  }

  return OsuRangeCurveSlider;
}(_RangeCurveSlider.RangeCurveSlider);

exports.OsuRangeCurveSlider = OsuRangeCurveSlider;
window.customElements.define("osu-range-curve-slider", OsuRangeCurveSlider);
},{"./CurveSlider":"elements/CurveSlider.js","./RangeCurveSlider":"elements/RangeCurveSlider.js"}],"js/locale.js":[function(require,module,exports) {
function convertTimezones() {
  var locals = document.getElementsByClassName("utc");

  for (var i = 0; i < locals.length; i++) {
    var t = locals[i];
    var time = new Date(t.getAttribute("data-time"));
    if (!(time instanceof Date) || isNaN(time)) continue;

    if (t.classList.contains("time")) {
      t.textContent = time.toLocaleTimeString();
    }
  }
}

function convertValues() {
  var locals = document.getElementsByClassName("localize");

  for (var i = 0; i < locals.length; i++) {
    var v = locals[i];

    if (v.classList.contains("number")) {
      v.textContent = parseFloat(v.textContent).toLocaleString();
    }
  }
}

convertTimezones();
convertValues();
},{}],"js/lobby_admin.js":[function(require,module,exports) {
function loadTabs() {
  var tabcontrol = document.querySelector(".admin .tabcontrol");
  var tabs = document.querySelector(".admin .tabs");
  tabs.classList.add("js");
  tabcontrol.style.display = "block";
  var links = tabcontrol.querySelectorAll("a");
  var activeTab = links[0];
  activeTab.classList.add("active");
  tabs.getElementsByClassName(activeTab.getAttribute("data-tab"))[0].style.display = "block";

  for (var i = 0; i < links.length; i++) {
    var link = links[i];

    link.onclick = function () {
      tabs.getElementsByClassName(activeTab.getAttribute("data-tab"))[0].style.display = "";
      activeTab.classList.remove("active");
      activeTab = this;
      activeTab.classList.add("active");
      tabs.getElementsByClassName(activeTab.getAttribute("data-tab"))[0].style.display = "block";
    };
  }
}

function connectAdmin() {
  var chatMessages = document.querySelector(".chat .messages");
  var mapPreview = document.querySelector(".admin .info span.map");
  chatMessages.scrollTop = chatMessages.scrollHeight;

  if (window.WebSocket) {
    var onChatMessage = function onChatMessage(msg) {
      var wasDown = chatMessages.scrollTop > chatMessages.scrollHeight - chatMessages.clientHeight - 10;
      var elem = document.createElement("div");
      elem.className = "message" + (botAccounts.indexOf(msg.sender) == -1 ? "" : " bot");
      var time = document.createElement("div");
      time.className = "time utc";
      time.setAttribute("data-time", msg.time);
      time.textContent = new Date(msg.time).toLocaleTimeString();
      var sender = document.createElement("div");
      sender.className = "sender";
      sender.textContent = msg.sender;
      var content = document.createElement("div");
      content.className = "content";
      content.textContent = msg.message;
      elem.appendChild(time);
      elem.appendChild(sender);
      elem.appendChild(content);
      chatMessages.appendChild(elem);
      if (wasDown) chatMessages.scrollTop = chatMessages.scrollHeight;
    };

    var onMapChange = function onMapChange(map) {
      if (map) {
        while (mapPreview.lastChild) {
          mapPreview.removeChild(mapPreview.lastChild);
        }

        var a = document.createElement("a");
        a.href = "https://osu.ppy.sh/b/" + map.beatmapID;
        a.textContent = map.artist + " - " + map.title + " [" + map.difficultyName + "]";
        mapPreview.appendChild(a);
      }
    };

    var connectChat = function connectChat(url) {
      var chatSocket = new WebSocket(url);

      chatSocket.onmessage = function (packet) {
        empty.style.display = "none";
        var msg = packet.data;
        if (typeof msg == "string") msg = JSON.parse(msg);
        console.log(msg);

        switch (msg.type) {
          case "chat":
            onChatMessage(msg.data);
            break;

          case "map":
            onMapChange(msg.data);
            break;
        }
      };

      chatSocket.onclose = function () {
        setTimeout(function () {
          console.error("Reconnecting websocket");
          connectChat(url);
        }, 1000);
      };
    };

    var url = window.location.origin + "/lobby/" + lobbyId + "/chat";
    if (url.startsWith("https:") || url.startsWith("http:")) url = "ws" + url.substr(4);
    var empty = chatMessages.querySelector(".empty");
    connectChat(url);
  }
}

window.loadTabs = loadTabs;
window.connectAdmin = connectAdmin;
},{}],"js/mapselect.js":[function(require,module,exports) {
///@ts-check

/**
 * @param {HTMLElement} container
 */
function setupMapSelect(container) {
  container.classList.add("js");
  var mapscontent = container.getElementsByClassName("mapscontent");

  if (mapscontent.length) {
    var button = mapscontent[0].nextElementSibling;

    if (button) {
      // @ts-ignore
      button.style.display = "";
    }
  }
}
/**
 * @param {PointerEvent} event
 * @param {HTMLElement} maplist
 * @param {string} setting
 */


function selectMap(event, maplist, setting) {
  var id = maplist.getAttribute("data-id");
  var mapselect = document.getElementById(setting + "_area");

  if (id && mapselect) {
    var elem = event.toElement || event.target;
    var input = mapselect.getElementsByClassName("identry")[0]; // @ts-ignore

    if (input && (!elem || !elem.classList || elem.classList.contains("id"))) {
      // @ts-ignore
      input.value = id;
    }
  }
}
/**
 * @param {HTMLElement} button
 * @param {string} setting
 */


function showMoreMapscontent(button, setting) {
  var mapselect = document.getElementById(setting + "_area");

  if (mapselect) {
    button.previousElementSibling.classList.add("scroll");
    if (button) button.parentElement.removeChild(button);
  }
}

window.showMoreMapscontent = showMoreMapscontent;

(function () {
  var lists = document.getElementsByClassName("mapselect");

  for (var i = 0; i < lists.length; i++) {
    // @ts-ignore
    setupMapSelect(lists[i]);
  }
})();
},{}],"js/maplists_view.js":[function(require,module,exports) {
//@ts-check

/**
 * @param {HTMLAnchorElement} a
 */
function handleLike(a) {
  var like = a.getAttribute("data-like") == "true";
  var id = a.getAttribute("data-maplist");
  var count = a.previousElementSibling;
  var oldContent = count.textContent.trim();
  var icon = a.children[0];
  count.textContent = (parseInt(oldContent) + (like ? -1 : 1)).toString();
  icon.classList.toggle("fas");
  icon.classList.toggle("far");
  var xhr = new XMLHttpRequest();

  xhr.onloadend = function () {
    if (xhr.status >= 400) {
      icon.classList.toggle("fas");
      icon.classList.toggle("far");
      count.textContent = oldContent;
    } else {
      var likeCheck = new XMLHttpRequest();
      likeCheck.open("GET", "/api/maplists/" + id + "/likes");

      likeCheck.onload = function () {
        count.textContent = parseInt(likeCheck.responseText).toString();
      };

      likeCheck.send();
    }
  };

  xhr.open("GET", a.href);
  xhr.setRequestHeader("X-Refresh-Token", "false");
  xhr.send();
  if (a.href.indexOf("/unlike") != -1) a.href = a.href.replace("/unlike", "/like");else a.href = a.href.replace("/like", "/unlike");
  a.setAttribute("data-like", like ? "false" : "true");
}
},{}],"../node_modules/dragula/dist/dragula.js":[function(require,module,exports) {
var define;
var global = arguments[3];
(function (f) { if (typeof exports === "object" && typeof module !== "undefined") {
    module.exports = f();
}
else if (typeof define === "function" && define.amd) {
    define([], f);
}
else {
    var g;
    if (typeof window !== "undefined") {
        g = window;
    }
    else if (typeof global !== "undefined") {
        g = global;
    }
    else if (typeof self !== "undefined") {
        g = self;
    }
    else {
        g = this;
    }
    g.dragula = f();
} })(function () {
    var define, module, exports;
    return (function e(t, n, r) { function s(o, u) { if (!n[o]) {
        if (!t[o]) {
            var a = typeof require == "function" && require;
            if (!u && a)
                return a(o, !0);
            if (i)
                return i(o, !0);
            var f = new Error("Cannot find module '" + o + "'");
            throw f.code = "MODULE_NOT_FOUND", f;
        }
        var l = n[o] = { exports: {} };
        t[o][0].call(l.exports, function (e) { var n = t[o][1][e]; return s(n ? n : e); }, l, l.exports, e, t, n, r);
    } return n[o].exports; } var i = typeof require == "function" && require; for (var o = 0; o < r.length; o++)
        s(r[o]); return s; })({ 1: [function (require, module, exports) {
                'use strict';
                var cache = {};
                var start = '(?:^|\\s)';
                var end = '(?:\\s|$)';
                function lookupClass(className) {
                    var cached = cache[className];
                    if (cached) {
                        cached.lastIndex = 0;
                    }
                    else {
                        cache[className] = cached = new RegExp(start + className + end, 'g');
                    }
                    return cached;
                }
                function addClass(el, className) {
                    var current = el.className;
                    if (!current.length) {
                        el.className = className;
                    }
                    else if (!lookupClass(className).test(current)) {
                        el.className += ' ' + className;
                    }
                }
                function rmClass(el, className) {
                    el.className = el.className.replace(lookupClass(className), ' ').trim();
                }
                module.exports = {
                    add: addClass,
                    rm: rmClass
                };
            }, {}], 2: [function (require, module, exports) {
                (function (global) {
                    'use strict';
                    var emitter = require('contra/emitter');
                    var crossvent = require('crossvent');
                    var classes = require('./classes');
                    var doc = document;
                    var documentElement = doc.documentElement;
                    function dragula(initialContainers, options) {
                        var len = arguments.length;
                        if (len === 1 && Array.isArray(initialContainers) === false) {
                            options = initialContainers;
                            initialContainers = [];
                        }
                        var _mirror; // mirror image
                        var _source; // source container
                        var _item; // item being dragged
                        var _offsetX; // reference x
                        var _offsetY; // reference y
                        var _moveX; // reference move x
                        var _moveY; // reference move y
                        var _initialSibling; // reference sibling when grabbed
                        var _currentSibling; // reference sibling now
                        var _copy; // item used for copying
                        var _renderTimer; // timer for setTimeout renderMirrorImage
                        var _lastDropTarget = null; // last container item was over
                        var _grabbed; // holds mousedown context until first mousemove
                        var o = options || {};
                        if (o.moves === void 0) {
                            o.moves = always;
                        }
                        if (o.accepts === void 0) {
                            o.accepts = always;
                        }
                        if (o.invalid === void 0) {
                            o.invalid = invalidTarget;
                        }
                        if (o.containers === void 0) {
                            o.containers = initialContainers || [];
                        }
                        if (o.isContainer === void 0) {
                            o.isContainer = never;
                        }
                        if (o.copy === void 0) {
                            o.copy = false;
                        }
                        if (o.copySortSource === void 0) {
                            o.copySortSource = false;
                        }
                        if (o.revertOnSpill === void 0) {
                            o.revertOnSpill = false;
                        }
                        if (o.removeOnSpill === void 0) {
                            o.removeOnSpill = false;
                        }
                        if (o.direction === void 0) {
                            o.direction = 'vertical';
                        }
                        if (o.ignoreInputTextSelection === void 0) {
                            o.ignoreInputTextSelection = true;
                        }
                        if (o.mirrorContainer === void 0) {
                            o.mirrorContainer = doc.body;
                        }
                        var drake = emitter({
                            containers: o.containers,
                            start: manualStart,
                            end: end,
                            cancel: cancel,
                            remove: remove,
                            destroy: destroy,
                            canMove: canMove,
                            dragging: false
                        });
                        if (o.removeOnSpill === true) {
                            drake.on('over', spillOver).on('out', spillOut);
                        }
                        events();
                        return drake;
                        function isContainer(el) {
                            return drake.containers.indexOf(el) !== -1 || o.isContainer(el);
                        }
                        function events(remove) {
                            var op = remove ? 'remove' : 'add';
                            touchy(documentElement, op, 'mousedown', grab);
                            touchy(documentElement, op, 'mouseup', release);
                        }
                        function eventualMovements(remove) {
                            var op = remove ? 'remove' : 'add';
                            touchy(documentElement, op, 'mousemove', startBecauseMouseMoved);
                        }
                        function movements(remove) {
                            var op = remove ? 'remove' : 'add';
                            crossvent[op](documentElement, 'selectstart', preventGrabbed); // IE8
                            crossvent[op](documentElement, 'click', preventGrabbed);
                        }
                        function destroy() {
                            events(true);
                            release({});
                        }
                        function preventGrabbed(e) {
                            if (_grabbed) {
                                e.preventDefault();
                            }
                        }
                        function grab(e) {
                            _moveX = e.clientX;
                            _moveY = e.clientY;
                            var ignore = whichMouseButton(e) !== 1 || e.metaKey || e.ctrlKey;
                            if (ignore) {
                                return; // we only care about honest-to-god left clicks and touch events
                            }
                            var item = e.target;
                            var context = canStart(item);
                            if (!context) {
                                return;
                            }
                            _grabbed = context;
                            eventualMovements();
                            if (e.type === 'mousedown') {
                                if (isInput(item)) { // see also: https://github.com/bevacqua/dragula/issues/208
                                    item.focus(); // fixes https://github.com/bevacqua/dragula/issues/176
                                }
                                else {
                                    e.preventDefault(); // fixes https://github.com/bevacqua/dragula/issues/155
                                }
                            }
                        }
                        function startBecauseMouseMoved(e) {
                            if (!_grabbed) {
                                return;
                            }
                            if (whichMouseButton(e) === 0) {
                                release({});
                                return; // when text is selected on an input and then dragged, mouseup doesn't fire. this is our only hope
                            }
                            // truthy check fixes #239, equality fixes #207
                            if (e.clientX !== void 0 && e.clientX === _moveX && e.clientY !== void 0 && e.clientY === _moveY) {
                                return;
                            }
                            if (o.ignoreInputTextSelection) {
                                var clientX = getCoord('clientX', e);
                                var clientY = getCoord('clientY', e);
                                var elementBehindCursor = doc.elementFromPoint(clientX, clientY);
                                if (isInput(elementBehindCursor)) {
                                    return;
                                }
                            }
                            var grabbed = _grabbed; // call to end() unsets _grabbed
                            eventualMovements(true);
                            movements();
                            end();
                            start(grabbed);
                            var offset = getOffset(_item);
                            _offsetX = getCoord('pageX', e) - offset.left;
                            _offsetY = getCoord('pageY', e) - offset.top;
                            classes.add(_copy || _item, 'gu-transit');
                            renderMirrorImage();
                            drag(e);
                        }
                        function canStart(item) {
                            if (drake.dragging && _mirror) {
                                return;
                            }
                            if (isContainer(item)) {
                                return; // don't drag container itself
                            }
                            var handle = item;
                            while (getParent(item) && isContainer(getParent(item)) === false) {
                                if (o.invalid(item, handle)) {
                                    return;
                                }
                                item = getParent(item); // drag target should be a top element
                                if (!item) {
                                    return;
                                }
                            }
                            var source = getParent(item);
                            if (!source) {
                                return;
                            }
                            if (o.invalid(item, handle)) {
                                return;
                            }
                            var movable = o.moves(item, source, handle, nextEl(item));
                            if (!movable) {
                                return;
                            }
                            return {
                                item: item,
                                source: source
                            };
                        }
                        function canMove(item) {
                            return !!canStart(item);
                        }
                        function manualStart(item) {
                            var context = canStart(item);
                            if (context) {
                                start(context);
                            }
                        }
                        function start(context) {
                            if (isCopy(context.item, context.source)) {
                                _copy = context.item.cloneNode(true);
                                drake.emit('cloned', _copy, context.item, 'copy');
                            }
                            _source = context.source;
                            _item = context.item;
                            _initialSibling = _currentSibling = nextEl(context.item);
                            drake.dragging = true;
                            drake.emit('drag', _item, _source);
                        }
                        function invalidTarget() {
                            return false;
                        }
                        function end() {
                            if (!drake.dragging) {
                                return;
                            }
                            var item = _copy || _item;
                            drop(item, getParent(item));
                        }
                        function ungrab() {
                            _grabbed = false;
                            eventualMovements(true);
                            movements(true);
                        }
                        function release(e) {
                            ungrab();
                            if (!drake.dragging) {
                                return;
                            }
                            var item = _copy || _item;
                            var clientX = getCoord('clientX', e);
                            var clientY = getCoord('clientY', e);
                            var elementBehindCursor = getElementBehindPoint(_mirror, clientX, clientY);
                            var dropTarget = findDropTarget(elementBehindCursor, clientX, clientY);
                            if (dropTarget && ((_copy && o.copySortSource) || (!_copy || dropTarget !== _source))) {
                                drop(item, dropTarget);
                            }
                            else if (o.removeOnSpill) {
                                remove();
                            }
                            else {
                                cancel();
                            }
                        }
                        function drop(item, target) {
                            var parent = getParent(item);
                            if (_copy && o.copySortSource && target === _source) {
                                parent.removeChild(_item);
                            }
                            if (isInitialPlacement(target)) {
                                drake.emit('cancel', item, _source, _source);
                            }
                            else {
                                drake.emit('drop', item, target, _source, _currentSibling);
                            }
                            cleanup();
                        }
                        function remove() {
                            if (!drake.dragging) {
                                return;
                            }
                            var item = _copy || _item;
                            var parent = getParent(item);
                            if (parent) {
                                parent.removeChild(item);
                            }
                            drake.emit(_copy ? 'cancel' : 'remove', item, parent, _source);
                            cleanup();
                        }
                        function cancel(revert) {
                            if (!drake.dragging) {
                                return;
                            }
                            var reverts = arguments.length > 0 ? revert : o.revertOnSpill;
                            var item = _copy || _item;
                            var parent = getParent(item);
                            var initial = isInitialPlacement(parent);
                            if (initial === false && reverts) {
                                if (_copy) {
                                    if (parent) {
                                        parent.removeChild(_copy);
                                    }
                                }
                                else {
                                    _source.insertBefore(item, _initialSibling);
                                }
                            }
                            if (initial || reverts) {
                                drake.emit('cancel', item, _source, _source);
                            }
                            else {
                                drake.emit('drop', item, parent, _source, _currentSibling);
                            }
                            cleanup();
                        }
                        function cleanup() {
                            var item = _copy || _item;
                            ungrab();
                            removeMirrorImage();
                            if (item) {
                                classes.rm(item, 'gu-transit');
                            }
                            if (_renderTimer) {
                                clearTimeout(_renderTimer);
                            }
                            drake.dragging = false;
                            if (_lastDropTarget) {
                                drake.emit('out', item, _lastDropTarget, _source);
                            }
                            drake.emit('dragend', item);
                            _source = _item = _copy = _initialSibling = _currentSibling = _renderTimer = _lastDropTarget = null;
                        }
                        function isInitialPlacement(target, s) {
                            var sibling;
                            if (s !== void 0) {
                                sibling = s;
                            }
                            else if (_mirror) {
                                sibling = _currentSibling;
                            }
                            else {
                                sibling = nextEl(_copy || _item);
                            }
                            return target === _source && sibling === _initialSibling;
                        }
                        function findDropTarget(elementBehindCursor, clientX, clientY) {
                            var target = elementBehindCursor;
                            while (target && !accepted()) {
                                target = getParent(target);
                            }
                            return target;
                            function accepted() {
                                var droppable = isContainer(target);
                                if (droppable === false) {
                                    return false;
                                }
                                var immediate = getImmediateChild(target, elementBehindCursor);
                                var reference = getReference(target, immediate, clientX, clientY);
                                var initial = isInitialPlacement(target, reference);
                                if (initial) {
                                    return true; // should always be able to drop it right back where it was
                                }
                                return o.accepts(_item, target, _source, reference);
                            }
                        }
                        function drag(e) {
                            if (!_mirror) {
                                return;
                            }
                            e.preventDefault();
                            var clientX = getCoord('clientX', e);
                            var clientY = getCoord('clientY', e);
                            var x = clientX - _offsetX;
                            var y = clientY - _offsetY;
                            _mirror.style.left = x + 'px';
                            _mirror.style.top = y + 'px';
                            var item = _copy || _item;
                            var elementBehindCursor = getElementBehindPoint(_mirror, clientX, clientY);
                            var dropTarget = findDropTarget(elementBehindCursor, clientX, clientY);
                            var changed = dropTarget !== null && dropTarget !== _lastDropTarget;
                            if (changed || dropTarget === null) {
                                out();
                                _lastDropTarget = dropTarget;
                                over();
                            }
                            var parent = getParent(item);
                            if (dropTarget === _source && _copy && !o.copySortSource) {
                                if (parent) {
                                    parent.removeChild(item);
                                }
                                return;
                            }
                            var reference;
                            var immediate = getImmediateChild(dropTarget, elementBehindCursor);
                            if (immediate !== null) {
                                reference = getReference(dropTarget, immediate, clientX, clientY);
                            }
                            else if (o.revertOnSpill === true && !_copy) {
                                reference = _initialSibling;
                                dropTarget = _source;
                            }
                            else {
                                if (_copy && parent) {
                                    parent.removeChild(item);
                                }
                                return;
                            }
                            if ((reference === null && changed) ||
                                reference !== item &&
                                    reference !== nextEl(item)) {
                                _currentSibling = reference;
                                dropTarget.insertBefore(item, reference);
                                drake.emit('shadow', item, dropTarget, _source);
                            }
                            function moved(type) { drake.emit(type, item, _lastDropTarget, _source); }
                            function over() { if (changed) {
                                moved('over');
                            } }
                            function out() { if (_lastDropTarget) {
                                moved('out');
                            } }
                        }
                        function spillOver(el) {
                            classes.rm(el, 'gu-hide');
                        }
                        function spillOut(el) {
                            if (drake.dragging) {
                                classes.add(el, 'gu-hide');
                            }
                        }
                        function renderMirrorImage() {
                            if (_mirror) {
                                return;
                            }
                            var rect = _item.getBoundingClientRect();
                            _mirror = _item.cloneNode(true);
                            _mirror.style.width = getRectWidth(rect) + 'px';
                            _mirror.style.height = getRectHeight(rect) + 'px';
                            classes.rm(_mirror, 'gu-transit');
                            classes.add(_mirror, 'gu-mirror');
                            o.mirrorContainer.appendChild(_mirror);
                            touchy(documentElement, 'add', 'mousemove', drag);
                            classes.add(o.mirrorContainer, 'gu-unselectable');
                            drake.emit('cloned', _mirror, _item, 'mirror');
                        }
                        function removeMirrorImage() {
                            if (_mirror) {
                                classes.rm(o.mirrorContainer, 'gu-unselectable');
                                touchy(documentElement, 'remove', 'mousemove', drag);
                                getParent(_mirror).removeChild(_mirror);
                                _mirror = null;
                            }
                        }
                        function getImmediateChild(dropTarget, target) {
                            var immediate = target;
                            while (immediate !== dropTarget && getParent(immediate) !== dropTarget) {
                                immediate = getParent(immediate);
                            }
                            if (immediate === documentElement) {
                                return null;
                            }
                            return immediate;
                        }
                        function getReference(dropTarget, target, x, y) {
                            var horizontal = o.direction === 'horizontal';
                            var reference = target !== dropTarget ? inside() : outside();
                            return reference;
                            function outside() {
                                var len = dropTarget.children.length;
                                var i;
                                var el;
                                var rect;
                                for (i = 0; i < len; i++) {
                                    el = dropTarget.children[i];
                                    rect = el.getBoundingClientRect();
                                    if (horizontal && (rect.left + rect.width / 2) > x) {
                                        return el;
                                    }
                                    if (!horizontal && (rect.top + rect.height / 2) > y) {
                                        return el;
                                    }
                                }
                                return null;
                            }
                            function inside() {
                                var rect = target.getBoundingClientRect();
                                if (horizontal) {
                                    return resolve(x > rect.left + getRectWidth(rect) / 2);
                                }
                                return resolve(y > rect.top + getRectHeight(rect) / 2);
                            }
                            function resolve(after) {
                                return after ? nextEl(target) : target;
                            }
                        }
                        function isCopy(item, container) {
                            return typeof o.copy === 'boolean' ? o.copy : o.copy(item, container);
                        }
                    }
                    function touchy(el, op, type, fn) {
                        var touch = {
                            mouseup: 'touchend',
                            mousedown: 'touchstart',
                            mousemove: 'touchmove'
                        };
                        var pointers = {
                            mouseup: 'pointerup',
                            mousedown: 'pointerdown',
                            mousemove: 'pointermove'
                        };
                        var microsoft = {
                            mouseup: 'MSPointerUp',
                            mousedown: 'MSPointerDown',
                            mousemove: 'MSPointerMove'
                        };
                        if (global.navigator.pointerEnabled) {
                            crossvent[op](el, pointers[type], fn);
                        }
                        else if (global.navigator.msPointerEnabled) {
                            crossvent[op](el, microsoft[type], fn);
                        }
                        else {
                            crossvent[op](el, touch[type], fn);
                            crossvent[op](el, type, fn);
                        }
                    }
                    function whichMouseButton(e) {
                        if (e.touches !== void 0) {
                            return e.touches.length;
                        }
                        if (e.which !== void 0 && e.which !== 0) {
                            return e.which;
                        } // see https://github.com/bevacqua/dragula/issues/261
                        if (e.buttons !== void 0) {
                            return e.buttons;
                        }
                        var button = e.button;
                        if (button !== void 0) { // see https://github.com/jquery/jquery/blob/99e8ff1baa7ae341e94bb89c3e84570c7c3ad9ea/src/event.js#L573-L575
                            return button & 1 ? 1 : button & 2 ? 3 : (button & 4 ? 2 : 0);
                        }
                    }
                    function getOffset(el) {
                        var rect = el.getBoundingClientRect();
                        return {
                            left: rect.left + getScroll('scrollLeft', 'pageXOffset'),
                            top: rect.top + getScroll('scrollTop', 'pageYOffset')
                        };
                    }
                    function getScroll(scrollProp, offsetProp) {
                        if (typeof global[offsetProp] !== 'undefined') {
                            return global[offsetProp];
                        }
                        if (documentElement.clientHeight) {
                            return documentElement[scrollProp];
                        }
                        return doc.body[scrollProp];
                    }
                    function getElementBehindPoint(point, x, y) {
                        var p = point || {};
                        var state = p.className;
                        var el;
                        p.className += ' gu-hide';
                        el = doc.elementFromPoint(x, y);
                        p.className = state;
                        return el;
                    }
                    function never() { return false; }
                    function always() { return true; }
                    function getRectWidth(rect) { return rect.width || (rect.right - rect.left); }
                    function getRectHeight(rect) { return rect.height || (rect.bottom - rect.top); }
                    function getParent(el) { return el.parentNode === doc ? null : el.parentNode; }
                    function isInput(el) { return el.tagName === 'INPUT' || el.tagName === 'TEXTAREA' || el.tagName === 'SELECT' || isEditable(el); }
                    function isEditable(el) {
                        if (!el) {
                            return false;
                        } // no parents were editable
                        if (el.contentEditable === 'false') {
                            return false;
                        } // stop the lookup
                        if (el.contentEditable === 'true') {
                            return true;
                        } // found a contentEditable element in the chain
                        return isEditable(getParent(el)); // contentEditable is set to 'inherit'
                    }
                    function nextEl(el) {
                        return el.nextElementSibling || manually();
                        function manually() {
                            var sibling = el;
                            do {
                                sibling = sibling.nextSibling;
                            } while (sibling && sibling.nodeType !== 1);
                            return sibling;
                        }
                    }
                    function getEventHost(e) {
                        // on touchend event, we have to use `e.changedTouches`
                        // see http://stackoverflow.com/questions/7192563/touchend-event-properties
                        // see https://github.com/bevacqua/dragula/issues/34
                        if (e.targetTouches && e.targetTouches.length) {
                            return e.targetTouches[0];
                        }
                        if (e.changedTouches && e.changedTouches.length) {
                            return e.changedTouches[0];
                        }
                        return e;
                    }
                    function getCoord(coord, e) {
                        var host = getEventHost(e);
                        var missMap = {
                            pageX: 'clientX',
                            pageY: 'clientY' // IE8
                        };
                        if (coord in missMap && !(coord in host) && missMap[coord] in host) {
                            coord = missMap[coord];
                        }
                        return host[coord];
                    }
                    module.exports = dragula;
                }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
            }, { "./classes": 1, "contra/emitter": 5, "crossvent": 6 }], 3: [function (require, module, exports) {
                module.exports = function atoa(a, n) { return Array.prototype.slice.call(a, n); };
            }, {}], 4: [function (require, module, exports) {
                'use strict';
                var ticky = require('ticky');
                module.exports = function debounce(fn, args, ctx) {
                    if (!fn) {
                        return;
                    }
                    ticky(function run() {
                        fn.apply(ctx || null, args || []);
                    });
                };
            }, { "ticky": 9 }], 5: [function (require, module, exports) {
                'use strict';
                var atoa = require('atoa');
                var debounce = require('./debounce');
                module.exports = function emitter(thing, options) {
                    var opts = options || {};
                    var evt = {};
                    if (thing === undefined) {
                        thing = {};
                    }
                    thing.on = function (type, fn) {
                        if (!evt[type]) {
                            evt[type] = [fn];
                        }
                        else {
                            evt[type].push(fn);
                        }
                        return thing;
                    };
                    thing.once = function (type, fn) {
                        fn._once = true; // thing.off(fn) still works!
                        thing.on(type, fn);
                        return thing;
                    };
                    thing.off = function (type, fn) {
                        var c = arguments.length;
                        if (c === 1) {
                            delete evt[type];
                        }
                        else if (c === 0) {
                            evt = {};
                        }
                        else {
                            var et = evt[type];
                            if (!et) {
                                return thing;
                            }
                            et.splice(et.indexOf(fn), 1);
                        }
                        return thing;
                    };
                    thing.emit = function () {
                        var args = atoa(arguments);
                        return thing.emitterSnapshot(args.shift()).apply(this, args);
                    };
                    thing.emitterSnapshot = function (type) {
                        var et = (evt[type] || []).slice(0);
                        return function () {
                            var args = atoa(arguments);
                            var ctx = this || thing;
                            if (type === 'error' && opts.throws !== false && !et.length) {
                                throw args.length === 1 ? args[0] : args;
                            }
                            et.forEach(function emitter(listen) {
                                if (opts.async) {
                                    debounce(listen, args, ctx);
                                }
                                else {
                                    listen.apply(ctx, args);
                                }
                                if (listen._once) {
                                    thing.off(type, listen);
                                }
                            });
                            return thing;
                        };
                    };
                    return thing;
                };
            }, { "./debounce": 4, "atoa": 3 }], 6: [function (require, module, exports) {
                (function (global) {
                    'use strict';
                    var customEvent = require('custom-event');
                    var eventmap = require('./eventmap');
                    var doc = global.document;
                    var addEvent = addEventEasy;
                    var removeEvent = removeEventEasy;
                    var hardCache = [];
                    if (!global.addEventListener) {
                        addEvent = addEventHard;
                        removeEvent = removeEventHard;
                    }
                    module.exports = {
                        add: addEvent,
                        remove: removeEvent,
                        fabricate: fabricateEvent
                    };
                    function addEventEasy(el, type, fn, capturing) {
                        return el.addEventListener(type, fn, capturing);
                    }
                    function addEventHard(el, type, fn) {
                        return el.attachEvent('on' + type, wrap(el, type, fn));
                    }
                    function removeEventEasy(el, type, fn, capturing) {
                        return el.removeEventListener(type, fn, capturing);
                    }
                    function removeEventHard(el, type, fn) {
                        var listener = unwrap(el, type, fn);
                        if (listener) {
                            return el.detachEvent('on' + type, listener);
                        }
                    }
                    function fabricateEvent(el, type, model) {
                        var e = eventmap.indexOf(type) === -1 ? makeCustomEvent() : makeClassicEvent();
                        if (el.dispatchEvent) {
                            el.dispatchEvent(e);
                        }
                        else {
                            el.fireEvent('on' + type, e);
                        }
                        function makeClassicEvent() {
                            var e;
                            if (doc.createEvent) {
                                e = doc.createEvent('Event');
                                e.initEvent(type, true, true);
                            }
                            else if (doc.createEventObject) {
                                e = doc.createEventObject();
                            }
                            return e;
                        }
                        function makeCustomEvent() {
                            return new customEvent(type, { detail: model });
                        }
                    }
                    function wrapperFactory(el, type, fn) {
                        return function wrapper(originalEvent) {
                            var e = originalEvent || global.event;
                            e.target = e.target || e.srcElement;
                            e.preventDefault = e.preventDefault || function preventDefault() { e.returnValue = false; };
                            e.stopPropagation = e.stopPropagation || function stopPropagation() { e.cancelBubble = true; };
                            e.which = e.which || e.keyCode;
                            fn.call(el, e);
                        };
                    }
                    function wrap(el, type, fn) {
                        var wrapper = unwrap(el, type, fn) || wrapperFactory(el, type, fn);
                        hardCache.push({
                            wrapper: wrapper,
                            element: el,
                            type: type,
                            fn: fn
                        });
                        return wrapper;
                    }
                    function unwrap(el, type, fn) {
                        var i = find(el, type, fn);
                        if (i) {
                            var wrapper = hardCache[i].wrapper;
                            hardCache.splice(i, 1); // free up a tad of memory
                            return wrapper;
                        }
                    }
                    function find(el, type, fn) {
                        var i, item;
                        for (i = 0; i < hardCache.length; i++) {
                            item = hardCache[i];
                            if (item.element === el && item.type === type && item.fn === fn) {
                                return i;
                            }
                        }
                    }
                }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
            }, { "./eventmap": 7, "custom-event": 8 }], 7: [function (require, module, exports) {
                (function (global) {
                    'use strict';
                    var eventmap = [];
                    var eventname = '';
                    var ron = /^on/;
                    for (eventname in global) {
                        if (ron.test(eventname)) {
                            eventmap.push(eventname.slice(2));
                        }
                    }
                    module.exports = eventmap;
                }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
            }, {}], 8: [function (require, module, exports) {
                (function (global) {
                    var NativeCustomEvent = global.CustomEvent;
                    function useNative() {
                        try {
                            var p = new NativeCustomEvent('cat', { detail: { foo: 'bar' } });
                            return 'cat' === p.type && 'bar' === p.detail.foo;
                        }
                        catch (e) {
                        }
                        return false;
                    }
                    /**
                     * Cross-browser `CustomEvent` constructor.
                     *
                     * https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent.CustomEvent
                     *
                     * @public
                     */
                    module.exports = useNative() ? NativeCustomEvent :
                        // IE >= 9
                        'function' === typeof document.createEvent ? function CustomEvent(type, params) {
                            var e = document.createEvent('CustomEvent');
                            if (params) {
                                e.initCustomEvent(type, params.bubbles, params.cancelable, params.detail);
                            }
                            else {
                                e.initCustomEvent(type, false, false, void 0);
                            }
                            return e;
                        } :
                            // IE <= 8
                            function CustomEvent(type, params) {
                                var e = document.createEventObject();
                                e.type = type;
                                if (params) {
                                    e.bubbles = Boolean(params.bubbles);
                                    e.cancelable = Boolean(params.cancelable);
                                    e.detail = params.detail;
                                }
                                else {
                                    e.bubbles = false;
                                    e.cancelable = false;
                                    e.detail = void 0;
                                }
                                return e;
                            };
                }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
            }, {}], 9: [function (require, module, exports) {
                var si = typeof setImmediate === 'function', tick;
                if (si) {
                    tick = function (fn) { setImmediate(fn); };
                }
                else {
                    tick = function (fn) { setTimeout(fn, 0); };
                }
                module.exports = tick;
            }, {}] }, {}, [2])(2);
});

//# sourceMappingURL=dragula.js.map
},{}],"js/maplists.js":[function(require,module,exports) {
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

///@ts-check
var dragula = require("../../node_modules/dragula/dist/dragula"); // maplists_create

/**
 * @typedef {"osu" | "taiko" | "ctb" | "mania"} GameMode
 */

/**
 * @typedef {"yes" | "no" | "any"} TriState
 */

/**
 * @typedef {"yes" | "no" | "any" | "ht"} DT
 */

/**
 * @typedef {{element: HTMLInputElement, getTransformedValue: () => number, setTransformedValue: (v: number) => void}} InverseSlider
 */

/**
 * @typedef {object} AutoPPPick
 * @property {string} dataSource
 * @property {GameMode} [mode]
 * @property {string} [comments]
 * @property {number} [minPP]
 * @property {number} [maxPP]
 * @property {number} [minLength]
 * @property {number} [maxLength]
 * @property {number} [minKeys]
 * @property {number} [maxKeys]
 * @property {number} [minBpm]
 * @property {number} [maxBpm]
 * @property {number} [minDiff]
 * @property {number} [maxDiff]
 * @property {number} [minOverweight]
 * @property {number} [maxOverweight]
 * @property {number} [minPassCount]
 * @property {number} [maxPassCount]
 * @property {number} [minUpdateHours]
 * @property {number} [maxUpdateHours]
 * @property {number} [langs]
 * @property {number} [genres]
 * @property {DT} [dt]
 * @property {TriState} [hd]
 * @property {TriState} [hr]
 * @property {TriState} [fl]
 */

/**
 * @typedef {object} ManualPick
 * @property {string} map
 * @property {string} comment
 * @property {string[]} mods
 * @property {GameMode} mode
 */

/**
 * @typedef {DetailedModInfo | string | string[] | "break"} ModInfo
 */

/**
 * @typedef {object} DetailedModInfo
 * @property {ModInfo} mod
 * @property {GameMode[]} [modes]
 */

/**
 * @typedef {object} NormalizedModInfo
 * @property {string | "break"} mod
 * @property {GameMode[]} modes
 */

/**
 * @typedef {({"type":"autopp", "value":AutoPPPick}|{"type":"manual", "value":ManualPick})} AnyPick
 */


var parsable = ["minPP", "maxPP", "minLength", "maxLength", "minKeys", "maxKeys", "minBpm", "maxBpm", "minPassCount", "maxPassCount", "minUpdateHours", "maxUpdateHours", "langs", "genres", "minDiff", "maxDiff", "minOverweight", "maxOverweight"];
var floats = ["minDiff", "maxDiff", "minOverweight", "maxOverweight"];
/**
 * @type {ModInfo[]}
 */

var allMods = ["EZ", "NF", "HT", {
  mod: "SO",
  modes: ["osu"]
}, "break", "HR", "SD", ["DT", "NC"], "HD", {
  mod: ["FI"],
  modes: ["mania"]
}, "FL", "break", {
  mod: "RX",
  modes: ["osu", "taiko", "ctb"]
}, "AP", {
  mod: "break",
  modes: ["mania"]
}, {
  mod: ["K4", "K5", "K6", "K7", "K8", "K9", "K1", "K2", "K3"],
  modes: ["mania"]
}, {
  mod: "COOP",
  modes: ["mania"]
}, {
  mod: "RN",
  modes: ["mania"]
}];
/**
 * @param {ModInfo} mod
 * @param {GameMode[]} [allModes]
 * @returns {NormalizedModInfo[]}
 */

function normalizeMod(mod, allModes) {
  if (!allModes) allModes = ["ctb", "mania", "osu", "taiko"];
  /**
   * @type {NormalizedModInfo[]}
   */

  var ret = [];
  if (typeof mod == "string") ret.push({
    mod: mod,
    modes: allModes
  });else if (Array.isArray(mod)) {
    for (var j = 0; j < mod.length; j++) {
      var modname = mod[j];
      ret.push({
        mod: modname,
        modes: allModes
      });
    }
  } else if (_typeof(mod) == "object") {
    if (!mod.modes) mod.modes = allModes;

    if (Array.isArray(mod.mod)) {
      for (var j = 0; j < mod.mod.length; j++) {
        ret.push.apply(ret, normalizeMod(mod.mod[j], mod.modes));
      }
    } else {
      ret = normalizeMod(mod.mod, mod.modes);
    }
  }
  return ret;
}
/**
 * @param {ModInfo} mod
 * @returns {string[]}
 */


function modNames(mod) {
  /**
   * @type {string[]}
   */
  var ret = [];

  if (mod == "break") {} else if (typeof mod == "string") ret.push(mod);else if (Array.isArray(mod)) {
    for (var index = 0; index < mod.length; index++) {
      var element = mod[index];
      ret.push.apply(ret, modNames(element));
    }
  } else if (_typeof(mod) == "object") {
    ret.push.apply(ret, modNames(mod.mod));
  } else console.error("Unknown mod name for", mod);

  return ret;
}

var recipeParser = {
  /**
   * @type {AnyPick[]}
   */
  recipe: [],
  clear: function clear() {
    this.recipe = [];
  },

  /**
   * @param {string} line
   */
  processLine: function processLine(line) {
    line = line.toString().trim();
    if (line.startsWith("#") || line.startsWith("//") || !line.length) return;

    if (line.startsWith("ppv1")) {
      // ppv1 "<source>" ("<comments generator>") (filters...)
      // where filters is one of:
      // [minPP/maxPP/minLength/maxLength/minKeys/maxKeys/minBpm/maxBpm/minPassCount/maxPassCount/minUpdateHours/maxUpdateHours/langs/genres]=<int>
      // [minDiff/maxDiff/minOverweight/maxOverweight]=<float>
      // [hd/hr/fl]=yes/no/any
      // dt=yes/no/ht/any
      line = line.substring(4).trim(); // 4 is "ppv1".length

      /**
       * @type {AutoPPPick}
       */

      var pp = {};
      var tmp = recipeParser.parseString(line);
      pp.dataSource = tmp[0];
      if (tmp[0].startsWith("osu")) pp.mode = "osu";else if (tmp[0].startsWith("taiko")) pp.mode = "taiko";else if (tmp[0].startsWith("ctb")) pp.mode = "ctb";else if (tmp[0].startsWith("mania")) pp.mode = "mania";
      line = tmp[1];
      pp.comments = "";

      if (line.startsWith("'") || line.startsWith("\"")) {
        tmp = recipeParser.parseString(line);
        pp.comments = tmp[0];
        line = tmp[1];
      }

      while (line.length > 0) {
        var space = line.indexOf(" ");
        if (space == -1) space = line.length;
        var arg = line.substring(0, space);
        line = line.substring(space).trim();
        var eq = arg.indexOf("=");
        if (eq == -1) eq = arg.length;
        var varname = arg.substring(0, eq).toLowerCase();
        var value = eq == arg.length ? "yes" : arg.substring(eq + 1);

        if (varname == "hd") {
          pp.hd = parseTriState(value, "HD");
        } else if (varname == "hr") {
          pp.hr = parseTriState(value, "HR");
        } else if (varname == "fl") {
          pp.fl = parseTriState(value, "FL");
        } else if (varname == "dt") {
          pp.dt = parseDT(value);
        } else {
          setPPVariable(pp, varname, value);
        }
      }

      this.recipe.push({
        type: "autopp",
        value: pp
      });
    } else {
      // <beatmapID>(,<mode>)(+<mods>)(:<comment>)
      var end = countUntil(line, function (a) {
        return a < '0' || a > '9';
      });
      if (end == -1) end = line.length;
      if (end == 0 || end >= 16) return;
      /** @type {string[]} */

      var mods = [];
      var comment = "";
      /** @type {GameMode} */

      var mode = "osu";
      var id = line.substring(0, end);
      line = line.substring(end);

      if (line.startsWith(",")) {
        line = line.substring(1).trim();
        var m = /^(osu|taiko|ctb|mania)/.exec(line);

        if (m) {
          line = line.substring(m[0].length).trim(); // @ts-ignore

          mode = m[1];
        }
      }

      if (line.startsWith("+")) {
        line = line.substring(1).trim();
        var modsTmp = recipeParser.parseMods(line);
        mods = modsTmp[0];
        line = modsTmp[1];
      }

      var colon = line.indexOf(':');
      if (colon != -1) comment = line.substring(colon + 1).trim();
      this.recipe.push({
        type: "manual",
        value: {
          map: id,
          comment: comment,
          mods: mods,
          mode: mode
        }
      });
    }
  },

  /**
   * @param {string} line
   * @returns {[string[], string]} A mods[] and the modified line
   */
  parseMods: function parseMods(line) {
    var found;
    var mods = [];

    while (line.length > 0) {
      found = false;

      for (var i = 0; i < allMods.length; i++) {
        var names = modNames(allMods[i]);

        for (var j = 0; j < names.length; j++) {
          var mod = names[j];

          if (line.toUpperCase().startsWith(mod)) {
            mods.push(mod);
            line = line.substring(mod.length).trim();
            found = true;
            if (mods.length >= 16) return [mods, line];
            break;
          }
        }

        if (found) break;
      }

      if (!found) break;
    }

    return [mods, line];
  },

  /**
   * @param {string} line
   * @returns {[string, string]} A interpreted string and the modified line
   */
  parseString: function parseString(line) {
    if (line.startsWith("'") || line.startsWith("\"")) {
      var quote = line[0];
      line = line.substring(1);
      var ret = "";
      var escape = false;
      var i = 0;

      for (; i < line.length; i++) {
        var c = line[i];
        if (escape) escape = false;else if (c == quote) break;else if (c == '\\') {
          escape = true;
          continue;
        }
        ret += c;
      }

      line = line.substring(i + 1).trim();
      return [ret, line];
    } else {
      var end = countUntil(line, function (a) {
        return " ,=;:+()".indexOf(a) != -1;
      });
      if (end == -1) end = line.length;
      var ret = line.substring(0, end);
      line = line.substring(end).trim();
      return [ret, line];
    }
  }
};
/**
 * @param {string} value
 * @param {string} name for error messages
 * @returns {TriState}
 */

function parseTriState(value, name) {
  value = value.toLowerCase();
  if (value == "yes" || value == "no" || value == "any") return value;else throw new Error("Invalid value " + value + " for variable " + name);
}
/**
 * @param {string} value
 * @returns {DT}
 */


function parseDT(value) {
  value = value.toLowerCase();
  if (value == "yes" || value == "no" || value == "any" || value == "ht") return value;else throw new Error("Invalid value " + value + " for variable DT");
}
/**
 * @param {AutoPPPick} pp
 * @param {string} name
 * @param {string} value
 */


function setPPVariable(pp, name, value) {
  for (var i = 0; i < parsable.length; i++) {
    var var_ = parsable[i];

    if (var_.toLowerCase() == name.toLowerCase()) {
      var isFloat = floats.indexOf(var_) != -1;
      var val = isFloat ? parseFloat(value) : parseInt(value);
      if (!isFinite(val) || !isFloat && (value.length > 9 || !isStrUnsignedInt(value))) throw new Error("Malformed value " + value + " for variable " + name);
      pp[var_] = val;
      return true;
    }
  }

  return false;
}
/**
 * @param {AutoPPPick} pp
 * @returns {string}
 */


function serializePPFilters(pp) {
  var ret = "";

  for (var i = 0; i < parsable.length; i++) {
    var name = parsable[i];
    var val = pp[name];
    var defaultVal = name.startsWith("max") ? name == "maxDiff" ? 100 : name == "maxOverweight" ? 1e5 : 2147483647 : 0;

    if (val !== undefined && val !== defaultVal) {
      ret += " " + name + "=" + parseFloat(val.toFixed(2));
    }
  }

  if (pp.dt === "yes") ret += " dt";else if ((pp.dt || "any") !== "any") ret += " dt=" + pp.dt;
  if (pp.hd === "yes") ret += " hd";else if ((pp.hd || "any") !== "any") ret += " hd=" + pp.hd;
  if (pp.hr === "yes") ret += " hr";else if ((pp.hr || "any") !== "any") ret += " hr=" + pp.hr;
  if (pp.fl === "yes") ret += " fl";else if ((pp.fl || "any") !== "any") ret += " fl=" + pp.fl;
  return ret;
}

function countUntil(arr, fun) {
  for (var i = 0; i < arr.length; i++) {
    if (fun(arr[i])) return i;
  }

  return -1;
}

function isStrUnsignedInt(str) {
  for (var i = 0; i < str.length; i++) {
    if (str[i] < '0' || str[i] > '9') return false;
  }

  return true;
}
/**
 * @param {string} str
 */


function capitalize(str) {
  return str[0].toUpperCase() + str.substring(1);
}

function formatMode(str) {
  if (str == "ctb") return "CtB";else return capitalize(str);
}
/**
 * @param {string} str string to escape
 * @param {boolean} [force=false]
 */


function escapeString(str, force) {
  if (!str) str = "";

  if (force || !str || str.startsWith("'") || str.startsWith("\"") || str.indexOf(" ,=;:+()") != -1) {
    if (str.indexOf("'") == -1) {
      return "'" + str.replace(/\\/g, "\\\\") + "'";
    } else {
      return '"' + str.replace(/\\/g, "\\\\").replace(/"/g, "\\\"") + '"';
    }
  } else {
    return str;
  }
}
/**
 * Creates a slider that until the halfway point acts near linear and then rapidly grows until maximum.
 * The halfway value can not be larger than the maximum and will slowly deviate from 0.5 the larger it gets
 * in relation to max.
 * @param {number} min
 * @param {number} max
 * @param {number} halfway
 * @returns {InverseSlider}
 */


function createInverseIncreasingSlider(min, max, halfway) {
  max -= min;
  halfway -= min;
  var factor = max / (max + halfway);

  var transform = function transform(x) {
    return factor * halfway * x / (1 - factor * x) + min;
  };

  var untransform = function untransform(x) {
    x -= min;
    return x / (factor * (halfway + x));
  };

  var slider = document.createElement("input");
  slider.setAttribute("type", "range");
  slider.min = "0";
  slider.max = "1";
  slider.step = "any";
  return {
    element: slider,
    getTransformedValue: function getTransformedValue() {
      return transform(parseFloat(this.element.value));
    },
    setTransformedValue: function setTransformedValue(value) {
      this.element.value = untransform(value).toString();
    }
  };
}
/**
 * @param {InverseSlider} slider
 * @param {string} [prefix]
 * @param {string} [suffix]
 * @returns {InverseSlider & {container: HTMLElement, update: Function}}
 */


function autolabeledSlider(slider, prefix, suffix) {
  var container = document.createElement("label");
  container.setAttribute("class", "slider");
  var label = document.createElement("span");
  container.appendChild(label);
  container.appendChild(slider.element);

  function update() {
    label.textContent = (prefix || "") + parseFloat(slider.getTransformedValue().toFixed(2)) + (suffix || "");
  }

  slider.element.oninput = update;
  slider.element.onchange = update;
  update();
  return {
    container: container,
    update: update,
    element: slider.element,
    getTransformedValue: slider.getTransformedValue,
    setTransformedValue: function setTransformedValue(v) {
      slider.setTransformedValue(v);
      update();
    }
  };
}
/**
 * @param {number} min
 * @param {number} max
 * @param {number} step
 * @param {number} def default value
 * @returns {HTMLInputElement}
 */


function createMinMaxNumber(min, max, step, def) {
  var ret = document.createElement("input");
  ret.setAttribute("type", "number");
  ret.setAttribute("class", "lazerinput minmaxinput");
  ret.min = min.toString();
  ret.max = max.toString();
  ret.step = step.toString();

  if (def != 2147483647 && def !== undefined) {
    ret.placeholder = def.toString();
    ret.value = def.toString();
  }

  return ret;
}
/**
 * @param {string} label
 * @param {number} min
 * @param {number} max
 * @param {number} mid
 * @param {object} val
 * @param {string} minl
 * @param {string} maxl
 * @param {Function} change
 * @returns {HTMLElement}
 */


function rangePair(label, min, max, mid, val, minl, maxl, change) {
  var pair = document.createElement("div");
  pair.setAttribute("class", "rangepair");
  var minv = val[minl];
  var maxv = val[maxl];
  var mine = autolabeledSlider(createInverseIncreasingSlider(min, max, mid), "Minimum " + label + ": ");
  mine.setTransformedValue(minv === undefined ? 0 : minv);

  mine.element.onchange = function () {
    val[minl] = mine.getTransformedValue();
    change();
  };

  pair.appendChild(mine.container);
  var maxe = autolabeledSlider(createInverseIncreasingSlider(min, max, mid), "Maximum " + label + ": ");
  maxe.setTransformedValue(maxv === undefined ? 1e5 : maxv);

  maxe.element.onchange = function () {
    val[maxl] = maxe.getTransformedValue();
    change();
  };

  pair.appendChild(maxe.container);
  return pair;
}
/**
 * @param {string} labelText
 * @param {number} min
 * @param {number} max
 * @param {number} step
 * @param {object} val
 * @param {string} minl
 * @param {string} maxl
 * @param {Function} change
 * @returns {HTMLElement}
 */


function rangeTextPair(labelText, min, max, step, val, minl, maxl, change) {
  var pair = document.createElement("div");
  pair.setAttribute("class", "rangepair");
  var minv = val[minl] === undefined ? min : val[minl];
  var maxv = val[maxl] === undefined ? max : val[maxl];
  var mine = createMinMaxNumber(min, max, step, minv);
  var minlbl = label(mine, "Minimum " + labelText + ": ");

  mine.onchange = function () {
    if (mine.checkValidity && !mine.checkValidity()) return;
    if (!mine.value) delete val[minl];else val[minl] = parseFloat(mine.value);
    change();
  };

  pair.appendChild(minlbl);
  var maxe = createMinMaxNumber(min, max, step, maxv);
  var maxlbl = label(maxe, "Maximum " + labelText + ": ");

  maxe.onchange = function () {
    if (maxe.checkValidity && !maxe.checkValidity()) return;
    if (!maxe.value) delete val[maxl];else val[maxl] = parseFloat(maxe.value);
    change();
  };

  pair.appendChild(maxlbl);
  return pair;
}

function addBeatmap() {
  recipeParser.recipe.push({
    type: "manual",
    value: {
      map: "",
      comment: "",
      mods: [],
      mode: "osu"
    }
  });
  updateRecipe();
  updateRecipeText();
}

window.addBeatmap = addBeatmap;

function addPPMaps() {
  recipeParser.recipe.push({
    type: "autopp",
    value: {
      dataSource: getDefaultPPMode(),
      comments: "This map gives {{pp}}pp with {{hd}}{{hr}}{{dt}}{{ht}}{{fl}} good accuracy. Overweightedness = {{overweight}}"
    }
  });
  updateRecipe();
  updateRecipeText();
}

window.addPPMaps = addPPMaps;
/**
 * @param {AnyPick} value
 * @param {Function} updateElem
 * @returns {HTMLDivElement}
 */

function generateEditBody(value, updateElem) {
  var ret = document.createElement("div");
  ret.setAttribute("class", "body");

  if (value.type == "autopp") {
    var source = document.createElement("select");
    var regex = /^(ctb|mania|standard|taiko)-(\d{4}-\d{2}-\d{2})_(\d{2})-(\d{2})/;
    var sourceval = value.value.dataSource;

    for (var i = 0; i < ppSources.length; i++) {
      var id = ppSources[i];
      var m = regex.exec(id);
      var name = m ? formatMode(m[1]) + " pp maps from " + m[2] + " " + m[3] + ":" + m[4] + " via /u/grumd" : id;
      var option = document.createElement("option");

      if (id == sourceval) {
        option.setAttribute("selected", "selected");
      }

      option.value = id;
      option.textContent = name;
      source.appendChild(option);
    }

    source.onchange = function () {
      value.value.dataSource = source.value;
      updateElem();
    };

    ret.appendChild(label(source, "Maps source"));
    var comments = document.createElement("input");
    comments.setAttribute("type", "text");
    comments.setAttribute("class", "lazerinput");
    comments.setAttribute("placeholder", "This map gives {{pp}}pp with good accuracy.");
    comments.value = value.value.comments || "";

    comments.onchange = function () {
      value.value.comments = comments.value.trim();
      updateElem();
    };

    ret.appendChild(label(comments, "Comments", "A comments generator text which can contain variables which get replaced for each map. Variables are written inside double braces {{like so}}. " + "Available variables: dt, ht, hd, hr, fl, beatmap, mapset, overweight, pp, artist, title, version, difficulty, length, bpm, passcount, lastupdate, genre, language"));
    ret.appendChild(rangeTextPair("pp", 0, 2147483647, 1, value.value, "minPP", "maxPP", updateElem));
    ret.appendChild(rangeTextPair("length in seconds", 0, 2147483647, 1, value.value, "minLength", "maxLength", updateElem));
    ret.appendChild(rangeTextPair("BPM", 0, 2147483647, 1, value.value, "minBpm", "maxBpm", updateElem));
    ret.appendChild(rangeTextPair("Difficulty", 0, 100, 0.01, value.value, "minDiff", "maxDiff", updateElem));
    ret.appendChild(rangePair("Overweight", 0, 1e5, 100, value.value, "minOverweight", "maxOverweight", updateElem));
    ret.appendChild(rangeTextPair("pass count", 0, 2147483647, 1, value.value, "minPassCount", "maxPassCount", updateElem));
    ret.appendChild(rangeTextPair("hours since last update", 0, 2147483647, 1, value.value, "minUpdateHours", "maxUpdateHours", updateElem));
  } else if (value.type == "manual") {
    var mapID = document.createElement("input");
    mapID.setAttribute("type", "text");
    mapID.setAttribute("class", "lazerinput");
    mapID.setAttribute("required", "required");
    mapID.setAttribute("pattern", "^\\d{1,9}$");
    mapID.setAttribute("placeholder", "983680");
    mapID.value = value.value.map;
    var mapLabel = label(mapID, "Beatmap ID");

    mapID.onchange = function () {
      if (mapID.checkValidity && !mapID.checkValidity()) {
        return;
      }

      value.value.map = mapID.value.trim();
      updateElem();
    };

    ret.appendChild(mapLabel);
    var comment = document.createElement("input");
    comment.setAttribute("type", "text");
    comment.setAttribute("class", "lazerinput");
    comment.setAttribute("placeholder", "This is best played with HR");
    comment.value = value.value.comment;

    comment.onchange = function () {
      value.value.comment = comment.value.trim();
      updateElem();
    };

    ret.appendChild(label(comment, "Comment"));
    var mods = document.createElement("details");
    mods.setAttribute("class", "mods");
    var summary = document.createElement("summary");
    summary.textContent = "Mods";
    mods.appendChild(summary);
    var container = document.createElement("div");
    container.setAttribute("class", "container");
    mods.appendChild(container);
    /**
     * @type {Function[]}
     */

    var modeListener = [];

    for (var i = 0; i < allMods.length; i++) {
      container.appendChild(modCheckbox(value.value.mods, allMods[i], updateElem, modeListener, value.value.mode));
    }

    ret.appendChild(createGamemodeSelector(value.value.mode, function () {
      // @ts-ignore
      value.value.mode = this.value;

      for (var i = 0; i < modeListener.length; i++) {
        modeListener[i](value.value.mode);
      }

      updateElem();
    }));
    ret.appendChild(mods);
  }

  return ret;
}
/**
 * @param {string[]} modsRef
 * @param {ModInfo} modRaw
 * @param {Function} updateElem
 * @param {Function[]} modeListener
 * @param {GameMode} mode
 * @returns {HTMLElement}
 */


function modCheckbox(modsRef, modRaw, updateElem, modeListener, mode) {
  var mods = normalizeMod(modRaw);
  if (!mods) return null;
  var element = document.createElement("div");
  element.classList.add("mod-wrapper");
  var btns = [];

  function updateMode(mode) {
    var hidden = mods[0].modes.indexOf(mode) == -1;
    element.style.display = hidden ? "none" : "";

    if (hidden) {
      for (var i = 0; i < btns.length; i++) {
        btns[i].toggle(false);
      }
    }
  }

  modeListener.push(updateMode);
  updateMode(mode);

  if (mods[0].mod == "break") {
    element.classList.add("break");
    return element;
  }

  for (var i = 0; i < mods.length; i++) {
    var btn = modToggleButton(modsRef, function (i, steps) {
      if (steps == 1) {
        if (i + 1 >= btns.length) {
          btns[i].style.display = "none";
          btns[0].style.display = "";
        } else {
          btns[i].style.display = "none";
          btns[i + 1].toggle(true);
          btns[i + 1].style.display = "";
        }
      } else if (steps == -1) {
        if (i == 0) {
          if (!btns[0].checked) {
            btns[0].style.display = "none";
            btns[btns.length - 1].toggle(true);
            btns[btns.length - 1].style.display = "";
          }
        } else {
          btns[i].style.display = "none";
          btns[i - 1].toggle(true);
          btns[i - 1].style.display = "";
        }
      }

      updateElem();
    }.bind(this, i), mods[i]);
    element.appendChild(btn);
    btns.push(btn);
  }

  if (btns.length > 1) {
    var found = 0;

    for (var i = 0; i < btns.length; i++) {
      var btn = btns[i];

      if (btn.checked) {
        found++;
      } else {
        btn.style.display = "none";
      }
    }

    if (found == 0) btns[0].style.display = "";
  }

  return element;
}
/**
 * @param {string[]} modsRef
 * @param {Function} next
 * @param {NormalizedModInfo} mod
 * @returns {HTMLElement & {checked: boolean, toggle: Function}}
 */


function modToggleButton(modsRef, next, mod) {
  /**
   * @type {HTMLElement & {checked: boolean, toggle: Function}}
   */
  //@ts-ignore
  var label = document.createElement("label");
  label.setAttribute("class", "mapmod checkbox mod_" + mod.mod.toLowerCase());
  label.checked = false;
  var cb = document.createElement("input");
  cb.setAttribute("type", "checkbox");

  function updateMods() {
    if (cb.checked) {
      if (modsRef.indexOf(mod.mod) == -1) {
        modsRef.push(mod.mod);
      }
    } else {
      var index = modsRef.indexOf(mod.mod);

      if (index != -1) {
        modsRef.splice(index, 1);
      }
    }
  }

  cb.onchange = function () {
    label.checked = cb.checked;
    var step = cb.checked ? 0 : 1;
    updateMods();
    next(step);
  };

  cb.checked = label.checked = modsRef.indexOf(mod.mod) != -1;

  label.oncontextmenu = function (e) {
    e.preventDefault();
    if (cb.checked) cb.checked = false;
    updateMods();
    next(-1);
    label.checked = cb.checked;
  };

  label.toggle = function (value) {
    cb.checked = label.checked = value === undefined ? !label.checked : value;
    updateMods();
  };

  label.appendChild(cb);
  var span = document.createElement("span");
  span.textContent = mod.mod;
  label.appendChild(span);
  return label;
}
/**
 * @param {HTMLElement} elem
 * @param {string} label
 * @param {string} [desc]
 * @returns {HTMLElement}
 */


function label(elem, label, desc) {
  var ret = document.createElement("div");
  ret.setAttribute("class", "labeled");
  var labelElem = document.createElement("label");
  var span = document.createElement("span");
  span.textContent = label;
  labelElem.appendChild(span);
  labelElem.appendChild(elem);

  if (desc) {
    var p = document.createElement("p");
    p.setAttribute("class", "description");
    p.textContent = desc;
    labelElem.appendChild(p);
  }

  ret.appendChild(labelElem);
  return ret;
}
/**
 * @param {string} value
 * @param {(this: GlobalEventHandlers, ev: Event) => any} [onChange]
 */


function createGamemodeSelector(value, onChange) {
  var mode = document.createElement("div");
  mode.setAttribute("class", "gamemoderows");
  var modesN = [["osu", "taiko"], ["ctb", "mania"]];
  var modeNames = [["Osu", "Taiko"], ["CtB", "Mania"]];
  var randomName = "__" + Math.random().toString(36);

  for (var j = 0; j < modesN.length; j++) {
    var modes = modesN[j];
    var row = document.createElement("div");
    row.className = "row";

    for (var i = 0; i < modesN.length; i++) {
      var text = document.createElement("label");
      var span = document.createElement("span");
      span.setAttribute("class", "mode-" + modes[i]);
      span.textContent = modeNames[j][i];
      var input = document.createElement("input");
      input.setAttribute("type", "radio");
      input.setAttribute("name", randomName);
      input.setAttribute("value", modes[i]);
      if (modes[i] == value) input.setAttribute("checked", "checked");
      input.onchange = onChange;
      text.appendChild(input);
      text.appendChild(span);
      row.appendChild(text);
    }

    mode.appendChild(row);
  }

  return mode;
}

function updateRecipe() {
  for (var i = 0; i < recipeParser.recipe.length; i++) {
    var value = recipeParser.recipe[i];
    var v = JSON.stringify(value);

    while (gui.children[i] && gui.children[i].getAttribute("data-recipe") != v) {
      gui.removeChild(gui.children[i]);
    }

    if (gui.children[i]) {
      gui.children[i].setAttribute("data-index", i.toString());
      if (gui.children[i].getAttribute("data-recipe") == v) continue;
    }

    var elem = document.createElement("div");
    elem.setAttribute("class", "recipe_gui_recipe " + value.type);
    elem.setAttribute("data-recipe", v);
    elem.setAttribute("data-index", i.toString());
    var wrap = document.createElement("div");
    wrap.setAttribute("class", "wrap");
    elem.appendChild(wrap);
    var header = document.createElement("div");
    header.setAttribute("class", "mapheader");
    wrap.appendChild(header);
    var handle = document.createElement("i");
    handle.setAttribute("class", "handle fas fa-grip-horizontal");
    header.appendChild(handle);
    var title = document.createElement("h3");
    title.textContent = value.type == "autopp" ? "PP Maps" : value.type == "manual" ? "Selected Beatmap" : "???";
    header.appendChild(title);
    var remove = document.createElement("i");
    remove.setAttribute("class", "remove fas fa-times");

    remove.onclick = function (obj, elem) {
      if (!confirm("Really delete this map part?")) return;
      var index = recipeParser.recipe.indexOf(obj);
      recipeParser.recipe.splice(index, 1);
      elem.parentElement.removeChild(elem);
      updateRecipe();
      updateRecipeText();
    }.bind(this, value, elem);

    header.appendChild(remove);
    var body = generateEditBody(value, function (elem, value) {
      elem.setAttribute("data-recipe", JSON.stringify(value));
      updateRecipe();
      updateRecipeText();
    }.bind(this, elem, value));
    wrap.appendChild(body);
    gui.appendChild(elem);
  }

  while (gui.children[recipeParser.recipe.length]) {
    gui.removeChild(gui.children[recipeParser.recipe.length]);
  }
}

function updateRecipeText() {
  /**
   * @type {any}
   */
  var textarea = document.getElementById("recipe");
  var ret = "";

  for (var i = 0; i < recipeParser.recipe.length; i++) {
    var value = recipeParser.recipe[i];

    if (value.type == "autopp") {
      ret += "ppv1 ";
      ret += escapeString(value.value.dataSource);
      if (value.value.comments) ret += " " + escapeString(value.value.comments, true);
      ret += serializePPFilters(value.value);
      ret += "\n";
    } else if (value.type == "manual") {
      ret += value.value.map;
      if (value.value.mode && value.value.mode != "osu") ret += "," + value.value.mode;
      if (_typeof(value.value.mods) == "object" && value.value.mods.length > 0) ret += "+" + value.value.mods.join();
      if (value.value.comment.length) ret += ": " + value.value.comment;
      ret += "\n";
    }
  }

  textarea.value = ret;
}

function getDefaultPPMode() {
  for (var i = 0; i < ppSources.length; i++) {
    if (ppSources[i].startsWith("standard")) return ppSources[i];
  }
}

function parseRecipe(textarea) {
  if (!textarea) textarea = document.getElementById("recipe");
  var lines = textarea.value.split(/\r\n|\n|\r/g);
  recipeParser.clear();

  for (var i = 0; i < lines.length; i++) {
    recipeParser.processLine(lines[i]);
  }

  updateRecipe();
}

var gui;
var drake;

function startMapLists() {
  gui = document.getElementById("recipe_gui");
  drake = dragula({
    containers: [gui],
    moves: function moves(el, container, handle) {
      return handle.classList.contains("handle");
    }
  });
  drake.on("drop", function (el, target, source, sibling) {
    var oldIndex = parseInt(el.getAttribute("data-index"));
    var newIndex = sibling ? parseInt(sibling.getAttribute("data-index")) : recipeParser.recipe.length;
    var data = recipeParser.recipe.splice(oldIndex, 1)[0];

    if (newIndex > oldIndex) {
      recipeParser.recipe.splice(newIndex - 1, 0, data);
    } else {
      recipeParser.recipe.splice(newIndex, 0, data);
    }

    updateRecipe();
    updateRecipeText();
  });
  gui.parentElement.style.display = "block";
  parseRecipe();

  window.onbeforeunload = function () {
    backup();
    return "Any unsaved settings will be lost.";
  };

  setInterval(backup, 30000);
  setTimeout(function () {
    if (!document.getElementById("recipe").value) restore();
  }, 50);
}

window.startMapLists = startMapLists;

function backup() {
  window.localStorage.setItem("osuhost.recipe", document.getElementById("recipe").value);
}

function restore() {
  document.getElementById("recipe").value = window.localStorage.getItem("osuhost.recipe");
  parseRecipe();
}
},{"../../node_modules/dragula/dist/dragula":"../node_modules/dragula/dist/dragula.js"}],"js/queue.js":[function(require,module,exports) {
function reload() {
  var place = document.getElementsByClassName("place")[0];
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "/create/queue/poll");

  xhr.onloadend = function () {
    if (xhr.status != 200) {
      setTimeout(function () {
        window.location.reload();
      }, 30000);
      return;
    }

    if (xhr.responseText == "0") window.location.reload();else place.textContent = xhr.responseText;
  };

  xhr.send();
}

window.reload = reload;
},{}],"index.js":[function(require,module,exports) {
"use strict";

require("./elements/CurveSlider");

require("./elements/RangeCurveSlider");

require("./elements/OsuCurveSlider");

require("./js/locale");

require("./js/lobby_admin");

require("./js/mapselect");

require("./js/maplists_view");

require("./js/maplists");

require("./js/queue");
},{"./elements/CurveSlider":"elements/CurveSlider.js","./elements/RangeCurveSlider":"elements/RangeCurveSlider.js","./elements/OsuCurveSlider":"elements/OsuCurveSlider.js","./js/locale":"js/locale.js","./js/lobby_admin":"js/lobby_admin.js","./js/mapselect":"js/mapselect.js","./js/maplists_view":"js/maplists_view.js","./js/maplists":"js/maplists.js","./js/queue":"js/queue.js"}],"../../../node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "35585" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.js"], null)
//# sourceMappingURL=/bundle.js.map